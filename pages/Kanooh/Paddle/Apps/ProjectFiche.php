<?php
/**
 * @file
 * Contains \Kanooh\Paddle\Apps\ProjectFiche.
 */

namespace Kanooh\Paddle\Apps;

/**
 * The Publication app.
 */
class ProjectFiche implements AppInterface
{

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return 'paddle-project-fiche';
    }

    /**
     * {@inheritdoc}
     */
    public function getModuleName()
    {
        return 'paddle_project_fiche';
    }

    /**
     * {@inheritdoc}
     */
    public function isConfigurable()
    {
        return false;
    }
}
