<?php
/**
 * @file
 * Contains \Kanooh\Paddle\Apps\EducationPage.
 */

namespace Kanooh\Paddle\Apps;

/**
 * The Publication app.
 */
class EducationPage implements AppInterface
{

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return 'education-page';
    }

    /**
     * {@inheritdoc}
     */
    public function getModuleName()
    {
        return 'paddle_education_page';
    }

    /**
     * {@inheritdoc}
     */
    public function isConfigurable()
    {
        return false;
    }
}
