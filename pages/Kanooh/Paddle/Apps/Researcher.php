<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Apps\Researcher.
 */

namespace Kanooh\Paddle\Apps;

/**
 * The Researcher app.
 */
class Researcher implements AppInterface
{

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return 'researcher';
    }

    /**
     * {@inheritdoc}
     */
    public function getModuleName()
    {
        return 'paddle_vub_researcher';
    }

    /**
     * {@inheritdoc}
     */
    public function isConfigurable()
    {
        return false;
    }
}
