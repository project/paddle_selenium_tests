<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Apps\Wegwijs.
 */

namespace Kanooh\Paddle\Apps;

/**
 * The Wegwijs app.
 */
class Wegwijs implements AppInterface
{

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return 'wegwijs';
    }

    /**
     * {@inheritdoc}
     */
    public function getModuleName()
    {
        return 'paddle_wegwijs';
    }

    /**
     * {@inheritdoc}
     */
    public function isConfigurable()
    {
        return false;
    }
}
