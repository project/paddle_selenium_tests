<?php
/**
 * @file
 * Contains \Kanooh\Paddle\Apps\PasswordPolicy.
 */

namespace Kanooh\Paddle\Apps;

/**
 * The Password Policy app.
 */
class PasswordPolicy implements AppInterface
{
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return 'paddle-password-policy';
    }

    /**
     * {@inheritdoc}
     */
    public function getModuleName()
    {
        return 'paddle_password_policy';
    }

    /**
     * {@inheritdoc}
     */
    public function isConfigurable()
    {
        return true;
    }
}
