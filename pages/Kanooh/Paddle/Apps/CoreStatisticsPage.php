<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Apps\CoreStatisticsPage.
 */

namespace Kanooh\Paddle\Apps;

/**
 * The Publication app.
 */
class CoreStatisticsPage implements AppInterface
{
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return 'paddle-core-statistics';
    }

    /**
     * {@inheritdoc}
     */
    public function getModuleName()
    {
        return 'paddle_core_statistics';
    }

    /**
     * {@inheritdoc}
     */
    public function isConfigurable()
    {
        return false;
    }
}
