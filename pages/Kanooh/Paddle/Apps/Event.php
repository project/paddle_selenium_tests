<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Apps\Event.
 */

namespace Kanooh\Paddle\Apps;

/**
 * The Event app.
 */
class Event implements AppInterface
{
  /**
   * {@inheritdoc}
   */
  public function getId()
  {
    return 'paddle-event';
  }

  /**
   * {@inheritdoc}
   */
  public function getModuleName()
  {
    return 'paddle_event';
  }

  /**
   * {@inheritdoc}
   */
  public function isConfigurable()
  {
    return false;
  }
}
