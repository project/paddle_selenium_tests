<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Admin\Apps\PaddleApps\PaddleProtectedContent.
 */

namespace Kanooh\Paddle\Pages\Admin\Apps\PaddleApps\PaddleProtectedContent;

use Kanooh\Paddle\Pages\PaddlePage;

/**
 * The forgot password page.
 */
class ForgotPasswordPage extends PaddlePage
{
    /**
     * {@inheritdoc}
     */
    protected $path = 'user/password';
}
