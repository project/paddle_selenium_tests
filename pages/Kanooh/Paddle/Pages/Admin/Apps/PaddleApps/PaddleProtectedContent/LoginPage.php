<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Admin\Apps\PaddleApps\PaddleProtectedContent.
 */

namespace Kanooh\Paddle\Pages\Admin\Apps\PaddleApps\PaddleProtectedContent;

use Kanooh\Paddle\Pages\PaddlePage;

/**
 * The login page.
 */
class LoginPage extends PaddlePage
{
    /**
     * {@inheritdoc}
     */
    protected $path = 'user';
}
