<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Admin\Apps\PaddleApps\PaddleProtectedContent\ConfigurePage\ConfigureForm.
 */

namespace Kanooh\Paddle\Pages\Admin\Apps\PaddleApps\PaddleProtectedContent\ConfigurePage;

use Kanooh\Paddle\Pages\Element\Form\Form;
use Kanooh\Paddle\Pages\Element\Form\FormFieldNotDefinedException;
use Kanooh\Paddle\Pages\Element\Form\Text;
use Kanooh\Paddle\Pages\Element\Form\FileField;

/**
 * Class representing the Paddle Protected Content configuration form.
 *
 * @property FileField $backgroundImage
 * @property Text $loginTitle
 * @property Text $contactEmail
 */
class ConfigureForm extends Form
{
    /**
     * {@inheritdoc}
     */
    public function __get($name)
    {
        switch ($name) {
            case 'backgroundImage':
                return new FileField(
                    $this->webdriver,
                    '//input[@name="files[paddle_protected_content_login_background_image]"]',
                    '//input[@name="paddle_protected_content_login_background_image_upload_button"]',
                    '//input[@name="paddle_protected_content_login_background_image_remove_button"]'
                );
                break;
            case 'loginTitle':
                return new Text($this->webdriver, $this->webdriver->byName('paddle_protected_content_login_title'));
                break;
            case 'contactEmail':
                return new Text($this->webdriver, $this->webdriver->byName('paddle_protected_content_login_contact_email'));
                break;
        }
        throw new FormFieldNotDefinedException($name);
    }
}
