<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Admin\Apps\PaddleApps\PaddleEducationPage\ConfigurePage\ConfigureForm.
 */

namespace Kanooh\Paddle\Pages\Admin\Apps\PaddleApps\PaddleEducationPage\ConfigurePage;

use Kanooh\Paddle\Pages\Element\Form\Form;
use Kanooh\Paddle\Pages\Element\Form\FormFieldNotDefinedException;
use Kanooh\Paddle\Pages\Element\Form\Text;
use Kanooh\Paddle\Pages\Element\LanguageSwitcher\LanguageSwitcher;

/**
 * Class representing the Paddle ReCaptcha configuration form.
 *
 * @property Text $hoursPerWeek
 * @property Text $studyArea
 * @property Text $grade
 * @property Text $whatWillYouLearn
 * @property Text $entryRequirements
 * @property Text $whatCanIBecome
 * @property Text $whereFollowEducation
 * @property Text $moreInfo
 * @property Text $links
 * @property LanguageSwitcher|null $languageSwitcher
 *   The language switcher block on the page. Null is absent.
 */
class ConfigureForm extends Form
{
    /**
     * {@inheritdoc}
     */
    public function __get($name)
    {
        switch ($name) {
            case 'hoursPerWeek':
                return new Text($this->webdriver, $this->webdriver->byName('field_paddle_ep_hours_per_week'));
                break;
            case 'studyArea':
                return new Text($this->webdriver, $this->webdriver->byName('field_paddle_ep_study_area'));
                break;
            case 'grade':
                return new Text($this->webdriver, $this->webdriver->byName('field_paddle_ep_grade'));
                break;
            case 'whatWillYouLearn':
                return new Text($this->webdriver, $this->webdriver->byName('field_paddle_ep_what_will_learn'));
                break;
            case 'entryRequirements':
                return new Text($this->webdriver, $this->webdriver->byName('field_paddle_ep_entry_requirment'));
                break;
            case 'whatCanIBecome':
                return new Text($this->webdriver, $this->webdriver->byName('field_paddle_ep_what_can_i_becom'));
                break;
            case 'whereFollowEducation':
                return new Text($this->webdriver, $this->webdriver->byName('field_paddle_ep_where_follow_edu'));
                break;
            case 'moreInfo':
                return new Text($this->webdriver, $this->webdriver->byName('field_paddle_ep_more_info'));
                break;
            case 'links':
                return new Text($this->webdriver, $this->webdriver->byName('field_paddle_ep_links'));
                break;
            case 'languageSwitcher':
                try {
                    // Pass the container as it is uncertain if the language
                    // switcher is an <ul> or <select>.
                    $element = $this->webdriver->byClassName('language-switcher-locale-url');

                    return new LanguageSwitcher($this->webdriver, $element, false);
                } catch (\Exception $e) {
                    return null;
                }
                break;
        }
        throw new FormFieldNotDefinedException($name);
    }
}
