<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Admin\Apps\PaddleApps\PaddlePasswordPolicy\ConfigurePage\ConfigureForm.
 */

namespace Kanooh\Paddle\Pages\Admin\Apps\PaddleApps\PaddlePasswordPolicy\ConfigurePage;

use Kanooh\Paddle\Pages\Element\Form\Checkbox;
use Kanooh\Paddle\Pages\Element\Form\Form;
use Kanooh\Paddle\Pages\Element\Form\FormFieldNotDefinedException;
use Kanooh\Paddle\Pages\Element\Form\FileField;
use Kanooh\Paddle\Pages\Element\Form\Text;

/**
 * Class representing the Paddle Password Policy configuration form.
 *
 * @property Checkbox $useNormalPolicy
 */
class ConfigureForm extends Form
{
    /**
     * {@inheritdoc}
     */
    public function __get($name)
    {
        switch ($name) {
            case 'useNormalPolicy':
                return new Checkbox($this->webdriver, $this->webdriver->byId('edit-paddle-password-policy-enabled'));
        }
        throw new FormFieldNotDefinedException($name);
    }
}
