<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Admin\Apps\PaddleApps\PaddleProjectFiche\ConfigurePage\ConfigureForm.
 */

namespace Kanooh\Paddle\Pages\Admin\Apps\PaddleApps\PaddleProjectFiche\ConfigurePage;

use Kanooh\Paddle\Pages\Element\Form\Form;
use Kanooh\Paddle\Pages\Element\Form\FormFieldNotDefinedException;
use Kanooh\Paddle\Pages\Element\Form\Text;

/**
 * Class representing the Paddle ReCaptcha configuration form.
 *
 * @property Text $bannerImage
 * @property Text $currentStage
 * @property Text $date
 * @property Text $downloads
 * @property Text $financeChannels
 * @property Text $partners
 * @property Text $researcher
 * @property Text $website
 */
class ConfigureForm extends Form
{
    /**
     * {@inheritdoc}
     */
    public function __get($name)
    {
        switch ($name) {
            case 'bannerImage':
                return new Text($this->webdriver, $this->webdriver->byName('field_paddle_pf_banner_image'));
                break;
            case 'currentStage':
                return new Text($this->webdriver, $this->webdriver->byName('field_paddle_pf_current_stage'));
                break;
            case 'date':
                return new Text($this->webdriver, $this->webdriver->byName('field_paddle_pf_date'));
                break;
            case 'downloads':
                return new Text($this->webdriver, $this->webdriver->byName('field_paddle_pf_downloads'));
                break;
            case 'financeChannels':
                return new Text($this->webdriver, $this->webdriver->byName('field_paddle_pf_finance_channels'));
                break;
            case 'partners':
                return new Text($this->webdriver, $this->webdriver->byName('field_paddle_pf_partners'));
                break;
            case 'researcher':
                return new Text($this->webdriver, $this->webdriver->byName('field_paddle_pf_proj_researcher'));
                break;
            case 'website':
                return new Text($this->webdriver, $this->webdriver->byName('field_paddle_pf_project_website'));
                break;
        }
        throw new FormFieldNotDefinedException($name);
    }
}
