<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Admin\ContentManager\Node\LayoutPage\WegwijsLayoutPage.
 */

namespace Kanooh\Paddle\Pages\Admin\ContentManager\Node\LayoutPage;

use Kanooh\Paddle\Pages\Element\Display\WegwijsDisplay;

/**
 * The Panels display for wegwijs content.
 *
 * @property WegwijsDisplay $display
 */
class WegwijsLayoutPage extends LayoutPage
{
    /**
     * {@inheritdoc}
     */
    public function __get($property)
    {
        switch ($property) {
            case 'display':
                return new WegwijsDisplay($this->webdriver);
        }
        return parent::__get($property);
    }
}
