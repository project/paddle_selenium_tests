<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Admin\ContentManager\Node\LayoutPage\ResearcherLayoutPage.
 */

namespace Kanooh\Paddle\Pages\Admin\ContentManager\Node\LayoutPage;

use Kanooh\Paddle\Pages\Element\Display\ResearcherDisplay;

/**
 * The Panels display editor for researcher content.
 *
 * @property ResearcherDisplay $display
 *   The Panels researcher display.
 */
class ResearcherLayoutPage extends LayoutPage
{
    /**
     * {@inheritdoc}
     */
    public function __get($property)
    {
        switch ($property) {
            case 'display':
                return new ResearcherDisplay($this->webdriver);
        }
        return parent::__get($property);
    }
}
