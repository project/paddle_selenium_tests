<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Admin\ContentManager\Node\LayoutPage\CoreStatisticsPageLayoutPage.
 */

namespace Kanooh\Paddle\Pages\Admin\ContentManager\Node\LayoutPage;

use Kanooh\Paddle\Pages\Element\Display\CoreStatisticsPageDisplay;

/**
 * The Panels display editor for newsletter content.
 *
 * @property CoreStatisticsPageDisplay $display
 *   The Panels newsletter display.
 */
class CoreStatisticsPageLayoutPage extends LayoutPage
{
    /**
     * {@inheritdoc}
     */
    public function __get($property)
    {
        switch ($property) {
            case 'display':
                return new CoreStatisticsPageDisplay($this->webdriver);
        }
        return parent::__get($property);
    }
}
