<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Admin\ContentManager\Node\LayoutPage\ProjectFicheLayoutPage.
 */

namespace Kanooh\Paddle\Pages\Admin\ContentManager\Node\LayoutPage;

use Kanooh\Paddle\Pages\Element\Display\ProjecFicheDisplay;
use Kanooh\Paddle\Pages\Element\NodeMetadataSummary\NodeMetadataSummary;

/**
 * The Panels display editor for newsletter content.
 *
 * @property \Kanooh\Paddle\Pages\Element\Display\ProjectFicheDisplay $display
 *   The Panels project fiche display.
 */
class ProjectFicheLayoutPage extends LayoutPage
{
    /**
     * {@inheritdoc}
     */
    public function __get($property)
    {
        switch ($property) {
            case 'display':
                return new ProjecFicheDisplay($this->webdriver);
        }
        return parent::__get($property);
    }
}
