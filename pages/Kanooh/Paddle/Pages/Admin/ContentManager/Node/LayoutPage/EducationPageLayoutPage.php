<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Admin\ContentManager\Node\LayoutPage\EducationPageLayoutPage.
 */

namespace Kanooh\Paddle\Pages\Admin\ContentManager\Node\LayoutPage;

use Kanooh\Paddle\Pages\Element\Display\EducationPageDisplay;

/**
 * The Panels display editor for newsletter content.
 *
 * @property EducationPageDisplay $display
 *   The Panels newsletter display.
 */
class EducationPageLayoutPage extends LayoutPage
{
    /**
     * {@inheritdoc}
     */
    public function __get($property)
    {
        switch ($property) {
            case 'display':
                return new EducationPageDisplay($this->webdriver);
        }
        return parent::__get($property);
    }
}
