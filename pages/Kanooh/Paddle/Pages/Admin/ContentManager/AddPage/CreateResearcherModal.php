<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Admin\ContentManager\AddPage\CreateResearcherModal.
 */

namespace Kanooh\Paddle\Pages\Admin\ContentManager\AddPage;

use Kanooh\Paddle\Pages\Element\Form\Text;

/**
 * Class representing the modal dialog for creating new researchers.
 *
 * @package Kanooh\Paddle\Pages\Admin\ContentManager\AddPage
 *
 * @property Text $firstName
 *   The first name text field.
 * @property Text $lastName
 *   The last name text field.
 */
class CreateResearcherModal extends CreateNodeModal
{
    /**
     * Magic getter.
     */
    public function __get($property)
    {
        switch ($property) {
            case 'firstName':
                return new Text($this->webdriver, $this->webdriver->byName('field_paddle_researcher_firstnam[und][0][value]'));
            case 'lastName':
                return new Text($this->webdriver, $this->webdriver->byName('field_paddle_researcher_last_nam[und][0][value]'));
        }

        return parent::__get($property);
    }
}
