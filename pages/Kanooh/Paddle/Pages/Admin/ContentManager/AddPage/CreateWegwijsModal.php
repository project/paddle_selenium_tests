<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Admin\ContentManager\AddPage\CreateWegwijsModal.
 */

namespace Kanooh\Paddle\Pages\Admin\ContentManager\AddPage;

use Kanooh\Paddle\Pages\Element\Form\Text;

/**
 * Class representing the modal dialog for creating new wegwijs.
 *
 * @package Kanooh\Paddle\Pages\Admin\ContentManager\AddPage
 *
 * @property Text $title
 *   The title text field.
 * @property Text $ovoNumber
 *   The ovonumber text field.
 */
class CreateWegwijsModal extends CreateNodeModal
{
    /**
     * Magic getter.
     */
    public function __get($property)
    {
        switch ($property) {
            case 'title':
                return new Text($this->webdriver, $this->webdriver->byName('title'));
            case 'ovoNumber':
                return new Text($this->webdriver, $this->webdriver->byName('field_paddle_wegwijs_ovonumber[und][0][value]'));
        }

        return parent::__get($property);
    }
}
