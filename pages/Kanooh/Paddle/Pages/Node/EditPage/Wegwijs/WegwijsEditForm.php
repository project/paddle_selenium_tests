<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Node\EditPage\Wegwijs\WegwijsEditForm.
 */

namespace Kanooh\Paddle\Pages\Node\EditPage\Wegwijs;

use Kanooh\Paddle\Pages\Element\Form\Form;
use Kanooh\Paddle\Pages\Element\Wysiwyg\Wysiwyg;
use Kanooh\Paddle\Pages\Element\Form\FormFieldNotDefinedException;
use Kanooh\Paddle\Pages\Element\Form\Text;

/**
 * Class representing the wegwijs edit form.
 *
 * @property Text $ovonumber
 * @property Text $website
 * @property Text $contactPerson
 * @property Text $emailAddress
 * @property Text $telephone
 * @property Text $thoroughfare
 * @property Text $premise
 * @property Text $postCode
 * @property Text $city
 * @property Wysiwyg $body
 * @property \PHPUnit_Extensions_Selenium2TestCase_Element $rechtsvorm
 * @property \PHPUnit_Extensions_Selenium2TestCase_Element $rechtsvormAddButton
 * @property \PHPUnit_Extensions_Selenium2TestCase_Element $rechtsvormSelected
 */
class WegwijsEditForm extends Form
{

    /**
     * {@inheritdoc}
     */
    public function __get($name)
    {
        switch ($name) {
            case 'ovonumber':
                return new Text($this->webdriver, $this->element->byName('field_paddle_wegwijs_ovonumber[und][0][value]'));
                break;
            case 'contactPerson':
                return new Text($this->webdriver, $this->element->byName('field_paddle_wegwijs_contact_per[und][0][value]'));
                break;
            case 'emailAddress':
                return new Text($this->webdriver, $this->element->byName('field_paddle_wegwijs_email[und][0][email]'));
                break;
            case 'telephone':
                return new Text($this->webdriver, $this->element->byName('field_paddle_wegwijs_telephone[und][0][value]'));
                break;
            case 'website':
                return new Text($this->webdriver, $this->element->byName('field_paddle_wegwijs_website[und][0][url]'));
                break;
            case 'thoroughfare':
                return new Text($this->webdriver, $this->element->byName('field_paddle_wegwijs_location[und][0][thoroughfare]'));
                break;
            case 'premise':
                return new Text($this->webdriver, $this->element->byName('field_paddle_wegwijs_location[und][0][premise]'));
                break;
            case 'postCode':
                return new Text($this->webdriver, $this->element->byName('field_paddle_wegwijs_location[und][0][postal_code]'));
                break;
            case 'city':
                return new Text($this->webdriver, $this->element->byName('field_paddle_wegwijs_location[und][0][locality]'));
                break;
            case 'body':
                return new Wysiwyg($this->webdriver, 'edit-body-und-0-value');
                break;
            case 'rechtsvorm':
                return $this->webdriver->byName('field_paddle_wegwijs_rechtsvorm[und][term_entry]');
                break;
            case 'rechtsvormAddButton':
                return $this->webdriver->byXPath('//div[contains(@class, "field-name-field-paddle-wegwijs-rechtsvorm")]//input[@type = "submit" and @name = "op"]');
                break;
            case 'rechtsvormSelected':
                return $this->webdriver->byXPath('//div[contains(@class, "field-name-field-paddle-wegwijs-rechtsvorm")]//div[contains(@class, "at-term")]//span[contains(@class, "at-term-text")]');
                break;
        }
        throw new FormFieldNotDefinedException($name);
    }
}
