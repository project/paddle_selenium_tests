<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Node\EditPage\Wegwijs\WegwijsEditPage.
 */

namespace Kanooh\Paddle\Pages\Node\EditPage\Wegwijs;

use Kanooh\Paddle\Pages\Node\EditPage\EditPage;
use Kanooh\Paddle\Pages\Node\EditPage\Wegwijs\WegwijsEditForm;

/**
 * Page to edit a Wegwijs.
 *
 * @property WegwijsEditForm $wegwijsEditForm
 */
class WegwijsEditPage extends EditPage
{

    /**
     * {@inheritdoc}
     */
    public function __get($property)
    {
        switch ($property) {
            case 'wegwijsEditForm':
                return new WegwijsEditForm($this->webdriver, $this->webdriver->byId('wegwijs-node-form'));
        }
        return parent::__get($property);
    }

    /**
     * {@inheritdoc}
     */
    public function waitUntilTagIsDisplayed($tag)
    {
        $xpath = '//div[contains(@class, "field-name-field-paddle-wegwijs-rechtsvorm")]//div[contains(@class, "at-term-list")]//span[text() = "' . $tag . '"]';
        $this->webdriver->waitUntilElementIsDisplayed($xpath);
    }
}
