<?php

namespace Kanooh\Paddle\Pages\Node\EditPage\Researcher;

use Kanooh\Paddle\Pages\Element\TermReferenceTree\TermReferenceTree;

class ResearcherStaffTypeVocabularyTermReference extends TermReferenceTree
{
    /**
     * {@inheritdoc}
     */
    protected $xpathSelector = '//div[@id="edit-field-paddle-researcher-staff"]';
}
