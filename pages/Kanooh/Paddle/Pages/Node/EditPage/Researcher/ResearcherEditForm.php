<?php

/**
 * @file
 * Contains
 *   \Kanooh\Paddle\Pages\Node\EditPage\Researcher\ResearcherEditForm.
 */

namespace Kanooh\Paddle\Pages\Node\EditPage\Researcher;

use Kanooh\Paddle\Pages\Element\Form\Form;
use Kanooh\Paddle\Pages\Element\Wysiwyg\Wysiwyg;
use Kanooh\Paddle\Pages\Element\Form\FormFieldNotDefinedException;
use Kanooh\Paddle\Pages\Element\Researcher\ExtraLinksTable;
use Kanooh\Paddle\Pages\Element\Form\Text;
use Kanooh\Paddle\Pages\Element\Scald\ImageAtomField;

/**
 * Class representing the researcher edit form.
 *
 * @property ExtraLinksTable $extraLinks
 * @property Text $firstName
 * @property Text $lastName
 * @property Text $position
 * @property Text $email
 * @property Text $telephone
 * @property Text $linkedin
 * @property Text $crisLink
 * @property Text $projectsLink
 * @property Text $publicationsLink
 * @property Text $campus
 * @property Text $office
 * @property Text $address
 * @property Text $postCode
 * @property Text $city
 * @property Wysiwyg $body
 * @property ResearcherStaffTypeVocabularyTermReference $staffType
 * @property ImageAtomField $photo1
 * @property ImageAtomField $photo2
 */
class ResearcherEditForm extends Form
{

    /**
     * {@inheritdoc}
     */
    public function __get($name)
    {
        switch ($name) {
            case 'extraLinks':
                return new ExtraLinksTable($this->webdriver, '//table[contains(@id, "field-paddle-researcher-extralnk-values")]');
                break;
            case 'firstName':
                return new Text($this->webdriver, $this->element->byName('field_paddle_researcher_firstnam[und][0][value]'));
                break;
            case 'lastName':
                return new Text($this->webdriver, $this->element->byName('field_paddle_researcher_last_nam[und][0][value]'));
                break;
            case 'position':
                return new Text($this->webdriver, $this->element->byName('field_paddle_researcher_position[und][0][value]'));
                break;
            case 'email':
                return new Text($this->webdriver, $this->element->byName('field_paddle_researcher_email[und][0][email]'));
                break;
            case 'telephone':
                return new Text($this->webdriver, $this->element->byName('field_paddle_researcher_telefone[und][0][value]'));
                break;
            case 'campus':
                return new Text($this->webdriver, $this->element->byName('field_paddle_researcher_address[und][0][organisation_name]'));
                break;
            case 'office':
                return new Text($this->webdriver, $this->element->byName('field_paddle_researcher_address[und][0][thoroughfare]'));
                break;
            case 'address':
                return new Text($this->webdriver, $this->element->byName('field_paddle_researcher_address[und][0][premise]'));
                break;
            case 'postCode':
                return new Text($this->webdriver, $this->element->byName('field_paddle_researcher_address[und][0][postal_code]'));
                break;
            case 'city':
                return new Text($this->webdriver, $this->element->byName('field_paddle_researcher_address[und][0][locality]'));
                break;
            case 'body':
                return new Wysiwyg($this->webdriver, 'edit-body-und-0-value');
                break;
            case 'staffType':
                return new ResearcherStaffTypeVocabularyTermReference($this->webdriver);
                break;
            case 'linkedin':
                return new Text($this->webdriver, $this->element->byName('field_paddle_researcher_linkedin[und][0][url]'));
                break;
            case 'crisLink':
                return new Text($this->webdriver, $this->element->byName('field_paddle_researcher_cris_lnk[und][0][url]'));
                break;
            case 'projectsLink':
                return new Text($this->webdriver, $this->element->byName('field_paddle_researcher_projects[und][0][url]'));
                break;
            case 'publicationsLink':
                return new Text($this->webdriver, $this->element->byName('field_paddle_researcher_pub_link[und][0][url]'));
                break;
            case 'photo1':
                $element = $this->element->byXPath('.//div/input[@name="field_paddle_researcher_photo1[und][0][sid]"]/..');
                return new ImageAtomField($this->webdriver, $element);
                break;
            case 'photo2':
                $element = $this->element->byXPath('.//div/input[@name="field_paddle_researcher_photo2[und][0][sid]"]/..');
                return new ImageAtomField($this->webdriver, $element);
                break;
        }
        throw new FormFieldNotDefinedException($name);
    }
}
