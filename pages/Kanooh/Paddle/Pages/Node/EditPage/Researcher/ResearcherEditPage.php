<?php

/**
 * @file
 * Contains
 *   \Kanooh\Paddle\Pages\Node\EditPage\Researcher\ResearcherEditPage.
 */

namespace Kanooh\Paddle\Pages\Node\EditPage\Researcher;

use Kanooh\Paddle\Pages\Node\EditPage\EditPage;

/**
 * Page to edit a researcher.
 *
 * @property ResearcherEditForm $researcherEditForm
 */
class ResearcherEditPage extends EditPage
{

    /**
     * {@inheritdoc}
     */
    public function __get($property)
    {
        switch ($property) {
            case 'researcherEditForm':
                return new ResearcherEditForm($this->webdriver, $this->webdriver->byId('researcher-node-form'));
        }
        return parent::__get($property);
    }
}
