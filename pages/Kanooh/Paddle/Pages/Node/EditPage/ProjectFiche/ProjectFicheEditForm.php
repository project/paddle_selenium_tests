<?php

/**
 * @file
 * Contains
 *   \Kanooh\Paddle\Pages\Node\EditPage\ProjectFiche\ProjectFicheEditForm.
 */

namespace Kanooh\Paddle\Pages\Node\EditPage\ProjectFiche;

use Kanooh\Paddle\Pages\Element\Form\AutoCompletedText;
use Kanooh\Paddle\Pages\Element\Form\Form;
use Kanooh\Paddle\Pages\Element\Form\FormFieldNotDefinedException;
use Kanooh\Paddle\Pages\Element\Form\Text;
use Kanooh\Paddle\Pages\Element\Wysiwyg\Wysiwyg;
use Kanooh\Paddle\Pages\Element\Scald\ImageAtomField;
use Kanooh\Paddle\Pages\Element\ProjectFiche\DownloadsTable;
use Kanooh\Paddle\Pages\Element\Form\Checkbox;
use Kanooh\Paddle\Pages\Element\ProjectFiche\ProjectResearcherTable;

/**
 * Class representing the publication edit form.
 *
 * @property Text $date
 * @property Text $currentStage
 * @property Wysiwyg $partners
 * @property Text projectWebsite
 * @property Wysiwyg $financeChannels
 * @property ProjectResearcherTable $projectResearcher
 * @property DownloadsTable $downloads
 * @property ImageAtomField $bannerImage
 * @property Checkbox $showEndDate
 * @property Text $endDate
 * @property Checkbox $hideDay
 * @property Wysiwyg $body
 */
class ProjectFicheEditForm extends Form
{

    /**
     * {@inheritdoc}
     */
    public function __get($name)
    {
        switch ($name) {
            case 'date':
                return new Text($this->webdriver, $this->element->byName('field_paddle_pf_date[und][0][value][date]'));
                break;
            case 'showEndDate':
                return new Checkbox($this->webdriver, $this->element->byName('field_paddle_pf_date[und][0][show_todate]'));
                break;
            case 'endDate':
                return new Text($this->webdriver, $this->element->byName('field_paddle_pf_date[und][0][value2][date]'));
                break;
            case 'hideDay':
                return new Checkbox($this->webdriver, $this->element->byName('field_paddle_pf_hide_day[und]'));
                break;
            case 'currentStage':
                return new Text($this->webdriver, $this->element->byName('field_paddle_pf_current_stage[und][0][value]'));
                break;
            case 'partners':
                return new Wysiwyg($this->webdriver, 'edit-field-paddle-pf-partners-und-0-value');
                break;
            case 'projectWebsite':
                return new Text($this->webdriver, $this->element->byName('field_paddle_pf_project_website[und][0][url]'));
                break;
            case 'financeChannels':
                return new Wysiwyg($this->webdriver, 'edit-field-paddle-pf-finance-channels-und-0-value');
                break;
            case 'projectResearcher':
                return new ProjectResearcherTable($this->webdriver, '//table[contains(@id, "field-paddle-pf-proj-researcher-values")]');
                break;
            case 'downloads':
                return new DownloadsTable($this->webdriver, '//table[contains(@id, "field-paddle-pf-downloads-values")]');
                break;
            case 'bannerImage':
                $element = $this->element->byXPath('.//div/input[@name="field_paddle_pf_banner_image[und][0][sid]"]/..');
                return new ImageAtomField($this->webdriver, $element);
                break;
            case 'body':
                return new Wysiwyg($this->webdriver, 'edit-body-und-0-value');
                break;
        }
        throw new FormFieldNotDefinedException($name);
    }
}
