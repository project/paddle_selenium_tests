<?php

/**
 * @file
 * Contains
 *   \Kanooh\Paddle\Pages\Node\EditPage\ProjectFiche\ProjectFicheEditPage.
 */

namespace Kanooh\Paddle\Pages\Node\EditPage\ProjectFiche;

use Kanooh\Paddle\App\ProjectFiche\ProjectFicheTest;
use Kanooh\Paddle\Pages\Node\EditPage\EditPage;

/**
 * Page to edit a publication.
 *
 * @property ProjectFicheEditForm $projectFicheEditForm
 */
class ProjectFicheEditPage extends EditPage
{

    /**
     * {@inheritdoc}
     */
    public function __get($property)
    {
        switch ($property) {
            case 'projectFicheEditForm':
                return new ProjectFicheEditForm($this->webdriver, $this->webdriver->byId('project-fiche-node-form'));
        }
        return parent::__get($property);
    }
}
