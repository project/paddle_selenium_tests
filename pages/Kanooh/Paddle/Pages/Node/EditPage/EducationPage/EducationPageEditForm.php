<?php

/**
 * @file
 * Contains
 *   \Kanooh\Paddle\Pages\Node\EditPage\EducationPage\EducationPageEditForm.
 */

namespace Kanooh\Paddle\Pages\Node\EditPage\EducationPage;

use Kanooh\Paddle\Pages\Element\Form\Form;
use Kanooh\Paddle\Pages\Element\Wysiwyg\Wysiwyg;
use Kanooh\Paddle\Pages\Element\Form\Checkbox;
use Kanooh\Paddle\Pages\Element\Form\FormFieldNotDefinedException;
use Kanooh\Paddle\Pages\Element\EducationPage\WhereFollowEducationTable;
use Kanooh\Paddle\Pages\Element\Form\Text;
use Kanooh\Paddle\Pages\Element\Scald\ImageAtomField;


/**
 * Class representing the publication edit form.
 *
 * @property WhereFollowEducationTable $whereFollowEducationTable
 * @property Text $hoursPerWeek
 * @property Wysiwyg $body
 * @property Wysiwyg $whatWillYouLearn
 * @property Wysiwyg $entryRequirements
 * @property Wysiwyg $whatCanIBecome
 * @property Wysiwyg $moreInfo
 * @property Wysiwyg $links
 * @property EducationPageStudyVocabularyTermReference $studyArea
 * @property Checkbox $grade
 * @property Text $moreInfoLink
 * @property Text $spotify
 * @property ImageAtomField $featureImage
 */
class EducationPageEditForm extends Form
{

    /**
     * {@inheritdoc}
     */
    public function __get($name)
    {
        switch ($name) {
            case 'whereFollowEducationTable':
                return new WhereFollowEducationTable($this->webdriver, '//table[contains(@id, "field-paddle-ep-where-follow-edu-values")]');
                break;
            case 'hoursPerWeek':
                return new Text($this->webdriver, $this->element->byName('field_paddle_ep_hours_per_week[und][0][value]'));
                break;
            case 'whatWillYouLearn':
                return new Wysiwyg($this->webdriver, 'edit-field-paddle-ep-what-will-learn-und-0-value');
                break;
            case 'entryRequirements':
                return new Wysiwyg($this->webdriver, 'edit-field-paddle-ep-entry-requirment-und-0-value');
                break;
            case 'whatCanIBecome':
                return new Wysiwyg($this->webdriver, 'edit-field-paddle-ep-what-can-i-becom-und-0-value');
                break;
            case 'moreInfo':
                return new Wysiwyg($this->webdriver, 'edit-field-paddle-ep-more-info-und-0-value');
                break;
            case 'links':
                return new Wysiwyg($this->webdriver, 'edit-field-paddle-ep-links-und-0-value');
                break;
            case 'studyArea':
                return new EducationPageStudyVocabularyTermReference($this->webdriver);
                break;
            case 'grade':
                return new EducationPageGradeVocabularyTermReference($this->webdriver);
                break;
            case 'moreInfoLink':
                return new Text($this->webdriver, $this->element->byName('field_paddle_ep_more_info_link[und][0][url]'));
                break;
            case 'spotify':
                return new Text($this->webdriver, $this->element->byName('field_paddle_ep_spotify_link[und][0][uri]'));
                break;
            case 'featureImage':
                $element = $this->element->byXPath('.//div/input[@name="field_paddle_featured_image[und][0][sid]"]/..');
                return new ImageAtomField($this->webdriver, $element);
                break;
        }
        throw new FormFieldNotDefinedException($name);
    }
}
