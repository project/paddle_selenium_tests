<?php

/**
 * @file
 * Contains
 *   \Kanooh\Paddle\Pages\Node\EditPage\EducationPage\EducationPageEditPage.
 */

namespace Kanooh\Paddle\Pages\Node\EditPage\EducationPage;

use Kanooh\Paddle\Pages\Node\EditPage\EditPage;

/**
 * Page to edit a education page.
 *
 * @property EducationPageEditForm $educationPageEditForm
 */
class EducationPageEditPage extends EditPage
{

    /**
     * {@inheritdoc}
     */
    public function __get($property)
    {
        switch ($property) {
            case 'educationPageEditForm':
                return new EducationPageEditForm($this->webdriver, $this->webdriver->byId('education-page-node-form'));
        }
        return parent::__get($property);
    }
}
