<?php

namespace Kanooh\Paddle\Pages\Node\EditPage\EducationPage;

use Kanooh\Paddle\Pages\Element\TermReferenceTree\TermReferenceTree;

class EducationPageStudyVocabularyTermReference extends TermReferenceTree
{
    /**
     * {@inheritdoc}
     */
    protected $xpathSelector = '//div[@id="edit-field-paddle-ep-study-area"]';
}
