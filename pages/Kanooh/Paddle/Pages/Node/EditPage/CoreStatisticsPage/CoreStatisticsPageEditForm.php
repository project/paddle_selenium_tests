<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Node\EditPage\CoreStatisticsPage\CoreStatisticsPageEditForm.
 */

namespace Kanooh\Paddle\Pages\Node\EditPage\CoreStatisticsPage;

use Kanooh\Paddle\Pages\Element\Form\Form;
use Kanooh\Paddle\Pages\Element\Form\FormFieldNotDefinedException;
use Kanooh\Paddle\Pages\Element\Scald\ImageAtomField;
use Kanooh\Paddle\Pages\Element\Wysiwyg\Wysiwyg;
use Kanooh\Paddle\Pages\Element\Form\Text;
use Kanooh\Paddle\Pages\Element\Form\Checkbox;

/**
 * Class representing the core statistics page edit form.
 *
 * @property ParagraphsTable $paragraphs
 * @property ImageAtomField $featureImage
 * @property Wysiwyg $sources
 * @property Wysiwyg $relevantLinks
 * @property Wysiwyg $definitions
 * @property Text $publicationDate
 * @property Text $nextUpdate
 * @property Text $metaData
 * @property Checkbox $displayJustMonthNextUpdate
 */
class CoreStatisticsPageEditForm extends Form
{
    /**
     * {@inheritdoc}
     */
    public function __get($name)
    {
        switch ($name) {
            case 'paragraphs':
                return new ParagraphsTable($this->webdriver, '//table[contains(@id, "field-core-statistics-paragraphs-values")]');
                break;
            case 'featureImage':
                $element = $this->element->byXPath('.//div/input[@name="field_paddle_featured_image[und][0][sid]"]/..');
                return new ImageAtomField($this->webdriver, $element);
                break;
            case 'sources':
                return new Wysiwyg($this->webdriver, 'edit-field-paddle-cs-sources-und-0-value');
                break;
            case 'relevantLinks':
                return new Wysiwyg($this->webdriver, 'edit-field-paddle-cs-relevant-links-und-0-value');
                break;
            case 'definitions':
                return new Wysiwyg($this->webdriver, 'edit-field-paddle-cs-definitions-und-0-value');
                break;
            case 'publicationDate':
                return new Text($this->webdriver, $this->element->byName('field_paddle_cs_publication_date[und][0][value][date]'));
                break;
            case 'nextUpdate':
                return new Text($this->webdriver, $this->element->byName('field_paddle_cs_next_update[und][0][value][date]'));
                break;
            case 'metaData':
                return new Text($this->webdriver, $this->element->byName('field_paddle_cs_metadata[und][0][url]'));
                break;
            case 'displayJustMonthNextUpdate':
                return new Checkbox($this->webdriver, $this->element->byName('field_paddle_cs_next_display_mon[und]'));
                break;
        }
        throw new FormFieldNotDefinedException($name);
    }
}
