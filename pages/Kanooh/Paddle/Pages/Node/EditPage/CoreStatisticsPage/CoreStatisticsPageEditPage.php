<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Node\EditPage\CoreStatisticsPage\CoreStatisticsPageEditPage.
 */

namespace Kanooh\Paddle\Pages\Node\EditPage\CoreStatisticsPage;

use Kanooh\Paddle\Pages\Node\EditPage\EditPage;

/**
 * Page to edit a core statistics page.
 *
 * @property CoreStatisticsPageEditForm $coreStatisticsPageEditForm
 */
class CoreStatisticsPageEditPage extends EditPage
{
    /**
     * {@inheritdoc}
     */
    public function __get($property)
    {
        switch ($property) {
            case 'coreStatisticsPageEditForm':
                return new CoreStatisticsPageEditForm($this->webdriver, $this->webdriver->byId('core-statistics-page-node-form'));
        }
        return parent::__get($property);
    }
}
