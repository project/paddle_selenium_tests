<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Node\EditPage\CoreStatisticsPage\ParagraphsTable.
 */

namespace Kanooh\Paddle\Pages\Node\EditPage\CoreStatisticsPage;

use Kanooh\Paddle\Pages\Element\Table\Table;
use Kanooh\WebDriver\WebDriverTestCase;

/**
 * Table containing the paragraphs on the node edit page.
 *
 * @property ParagraphsTableRow[] $rows
 * @property \PHPUnit_Extensions_Selenium2TestCase_Element $addAnotherItem
 */
class ParagraphsTable extends Table
{
    /**
     * {@inheritdoc}
     */
    public function __construct(WebDriverTestCase $webdriver, $xpath)
    {
        parent::__construct($webdriver);
        $this->xpathSelector = $xpath;
    }

    /**
     * {@inheritdoc}
     */
    public function __get($name)
    {
        switch ($name) {
            case 'rows':
                $rows = array();
                $xpath = $this->xpathSelector . '/tbody/tr[contains(@class, "draggable")]';
                $elements = $this->webdriver->elements($this->webdriver->using('xpath')->value($xpath));
                foreach ($elements as $element) {
                    $rows[] = new ParagraphsTableRow($this->webdriver, $element);
                }
                return $rows;
                break;
            case 'addAnotherItem':
                return $this->webdriver->byName('field_core_statistics_paragraphs_add_more');
                break;
        }
        throw new \Exception("The property with the name $name is not defined.");
    }
}
