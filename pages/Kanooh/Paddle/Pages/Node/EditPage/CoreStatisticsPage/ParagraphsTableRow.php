<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Node\EditPage\CoreStatisticsPage\ParagraphsTableRow.
 */

namespace Kanooh\Paddle\Pages\Node\EditPage\CoreStatisticsPage;

use Kanooh\Paddle\Pages\Element\Wysiwyg\Wysiwyg;
use Kanooh\Paddle\Pages\Element\Form\Text;
use Kanooh\Paddle\Pages\Element\Table\Row;
use Kanooh\WebDriver\WebDriverTestCase;

/**
 * Class CompanyInformationTableRow
 *
 * @property Text $title
 * @property Wysiwyg $body
 * @property Text $dataWrapper
 */
class ParagraphsTableRow extends Row
{
    /**
     * {@inheritdoc}
     */
    public function __construct(WebDriverTestCase $webdriver, \PHPUnit_Extensions_Selenium2TestCase_Element $element)
    {
        parent::__construct($webdriver);
        $this->element = $element;
    }

    /**
     * {@inheritdoc}
     */
    public function __get($name)
    {
        switch ($name) {
            case 'title':
                $element = $this->element->byCssSelector('div.field-name-field-paddle-cs-paragraph-title input');
                return new Text($this->webdriver, $element);
            case 'body':
                $element = $this->element->byCssSelector('div.field-name-field-paddle-cs-paragraph-body textarea');
                return new Wysiwyg($this->webdriver, $element->attribute('id'));
            case 'dataWrapper':
                $element = $this->element->byCssSelector('div.field-name-field-paddle-cs-paragraph-datawr input');
                return new Text($this->webdriver, $element);
        }
        throw new \Exception("The property with the name $name is not defined.");
    }
}
