<?php

/**
 * @file
 * Contains
 *   \Kanooh\Paddle\Pages\Node\ViewPage\EducationPage\EducationPageViewPage.
 */

namespace Kanooh\Paddle\Pages\Node\ViewPage\EducationPage;

use Kanooh\Paddle\Pages\Element\Pane\EducationPage\EducationPageFeatureImagePane;
use Kanooh\Paddle\Pages\Node\ViewPage\ViewPage;

/**
 * Class representing a Education page in the frontend view.
 *
 * @property EducationPageFeatureImagePane $featureImagePane
 */
class EducationPageViewPage extends ViewPage
{

    /**
     * {@inheritdoc}
     */
    public function __get($property)
    {
        switch ($property) {
            case 'featureImagePane':
                $element = $this->webdriver->byCssSelector('.pane-featured-image');
                return new EducationPageFeatureImagePane($this->webdriver, $element->attribute('data-pane-uuid'));
        }
        return parent::__get($property);
    }
}
