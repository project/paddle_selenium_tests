<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Node\ViewPage\Researcher\ResearcherViewPage.
 */

namespace Kanooh\Paddle\Pages\Node\ViewPage\Researcher;

use Kanooh\Paddle\Pages\Element\Pane\Researcher\ResearcherFeatureImagePane;
use Kanooh\Paddle\Pages\Node\ViewPage\ViewPage;

/**
 * Class representing a Researcher in the frontend view.
 *
 * @property ResearcherFeatureImagePane $featureImagePane
 */
class ResearcherViewPage extends ViewPage
{

    /**
     * {@inheritdoc}
     */
    public function __get($property)
    {
        switch ($property) {
            case 'researcherPhoto1':
                $element = $this->webdriver->byCssSelector('.pane-researcher-info');
                return new ResearcherFeatureImagePane($this->webdriver, $element->attribute('data-pane-uuid'));
        }
        return parent::__get($property);
    }
}
