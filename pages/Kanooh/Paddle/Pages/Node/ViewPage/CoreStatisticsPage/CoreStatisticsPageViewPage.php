<?php

/**
 * @file
 * Contains
 *   \Kanooh\Paddle\Pages\Node\ViewPage\CoreStatisticsPage\CoreStatisticsPageViewPage.
 */

namespace Kanooh\Paddle\Pages\Node\ViewPage\CoreStatisticsPage;

use Kanooh\Paddle\Pages\Element\Pane\CoreStatisticsPage\CoreStatisticsPageFeatureImagePane;
use Kanooh\Paddle\Pages\Node\ViewPage\ViewPage;

/**
 * Class representing a Education page in the frontend view.
 *
 * @property CoreStatisticsPageFeatureImagePane $featureImagePane
 */
class CoreStatisticsPageViewPage extends ViewPage
{

    /**
     * {@inheritdoc}
     */
    public function __get($property)
    {
        switch ($property) {
            case 'featureImagePane':
                $element = $this->webdriver->byCssSelector('.pane-feature-image');
                return new CoreStatisticsPageFeatureImagePane($this->webdriver, $element->attribute('data-pane-uuid'));
        }
        return parent::__get($property);
    }
}
