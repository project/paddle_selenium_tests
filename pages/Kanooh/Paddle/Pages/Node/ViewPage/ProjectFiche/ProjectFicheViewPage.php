<?php

/**
 * @file
 * Contains
 *   \Kanooh\Paddle\Pages\Node\ViewPage\ProjectFiche\ProjectFicheViewPage.
 */

namespace Kanooh\Paddle\Pages\Node\ViewPage\ProjectFiche;

use Kanooh\Paddle\Pages\Element\Pane\ProjectFiche\ProjectFicheBannerImagePane;
use Kanooh\Paddle\Pages\Node\ViewPage\ViewPage;

/**
 * Class representing a Project Fiche page in the frontend view.
 *
 * @property ProjectFicheBannerImagePane $bannerImagePane
 */
class ProjectFicheViewPage extends ViewPage
{

    /**
     * {@inheritdoc}
     */
    public function __get($property)
    {
        switch ($property) {
            case 'bannerImagePane':
                $element = $this->webdriver->byCssSelector('.pane-banner-image');
                return new ProjectFicheBannerImagePane($this->webdriver, $element->attribute('data-pane-uuid'));
        }
        return parent::__get($property);
    }
}
