<?php

/**
 * @file
 * Contains
 *   \Kanooh\Paddle\Pages\Node\ViewPage\Wegwijs\WegwijsViewPage.
 */

namespace Kanooh\Paddle\Pages\Node\ViewPage\Wegwijs;

use Kanooh\Paddle\Pages\Node\ViewPage\ViewPage;

/**
 * Class representing a Wegwijs in the frontend view.
 */
class WegwijsViewPage extends ViewPage
{

    /**
     * {@inheritdoc}
     */
    public function __get($property)
    {
        return parent::__get($property);
    }
}
