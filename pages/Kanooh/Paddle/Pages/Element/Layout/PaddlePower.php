<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Element\Layout\PaddlePower.
 */

namespace Kanooh\Paddle\Pages\Element\Layout;

/**
 * Power layout.
 */
class PaddlePower extends Layout
{

    const REGIONA = '1_a';
    const REGIONB = '2_b';
    const REGIONC = '2_c';
    const REGIOND = '2_d';
    const REGIONE = '3_e';
    const REGIONF = '4_f';
    const REGIONG = '4_g';
    const REGIONH = '5_h';
    const REGIONI = '6_i';
    const REGIONJ = '6_j';
    const REGIONK = '6_k';
    const REGIONL = '7_l';
    const REGIONM = '8_m';
    const REGIONN = '8_n';
    const REGIONO = '9_o';

    /**
     * {@inheritdoc}
     */
    protected $info = array(
      'id' => 'paddle_power',
      'title' => 'Power',
      'category' => 'Paddle Layouts',
      'regions' => array(
        '1_a' => 'A',
        '2_b' => 'B',
        '2_c' => 'C',
        '2_d' => 'D',
        '3_e' => 'E',
        '4_f' => 'F',
        '4_g' => 'G',
        '5_h' => 'H',
        '6_i' => 'I',
        '6_j' => 'J',
        '6_k' => 'k',
        '7_l' => 'L',
        '8_m' => 'M',
        '8_n' => 'N',
        '9_o' => 'O',
      ),
    );
}
