<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Element\Layout\PaddleEducation.
 */

namespace Kanooh\Paddle\Pages\Element\Layout;

/**
 * Education layout.
 */
class PaddleEducation extends Layout
{

    const REGIONA = 'full_a';
    const REGIONB = 'nested_9_b';
    const REGIONC = 'nested_3_c';
    const REGIOND = 'full_d';

    /**
     * {@inheritdoc}
     */
    protected $info = array(
      'id' => 'paddle_education',
      'title' => 'Education',
      'category' => 'Paddle Layouts',
      'regions' => array(
        'full_a' => 'A',
        'nested_9_b' => 'B',
        'nested_3_c' => 'C',
        'full_d' => 'D',
      ),
    );
}
