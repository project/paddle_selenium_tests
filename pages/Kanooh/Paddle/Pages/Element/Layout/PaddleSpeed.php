<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Element\Layout\PaddleSpeed.
 */

namespace Kanooh\Paddle\Pages\Element\Layout;

/**
 * Education layout.
 */
class PaddleSpeed extends Layout
{

    const REGIONA = '1_a';
    const REGIONB = '1_b';
    const REGIONC = '2_c';
    const REGIOND = '2_d';
    const REGIONE = '2_e';
    const REGIONF = '3_f';
    const REGIONG = '3_g';
    const REGIONH = '3_h';
    const REGIONI = '4_i';
    const REGIONJ = '4_j';

    /**
     * {@inheritdoc}
     */
    protected $info = array(
      'id' => 'paddle_speed',
      'title' => 'Speed',
      'category' => 'Paddle Layouts',
      'regions' => array(
        '1_a' => 'A',
        '1_b' => 'B',
        '2_c' => 'C',
        '2_d' => 'D',
        '2_e' => 'E',
        '3_f' => 'F',
        '3_g' => 'G',
        '3_h' => 'H',
        '4_i' => 'I',
        '4_j' => 'J',
      ),
    );
}
