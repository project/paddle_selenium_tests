<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Element\Researcher\ExtraLinksTableRow.
 */

namespace Kanooh\Paddle\Pages\Element\Researcher;

use Kanooh\Paddle\Pages\Element\Form\Text;
use Kanooh\Paddle\Pages\Element\Table\Row;
use Kanooh\WebDriver\WebDriverTestCase;

/**
 * Class ExtraLinksTableRow
 *
 * @property Text $title
 * @property Text $url
 */
class ExtraLinksTableRow extends Row
{

    /**
     * @var \PHPUnit_Extensions_Selenium2TestCase_Element
     */
    protected $element;

    /**
     * {@inheritdoc}
     */
    public function __construct(WebDriverTestCase $webdriver, $element)
    {
        parent::__construct($webdriver);
        $this->element = $element;
    }

    /**
     * {@inheritdoc}
     */
    public function __get($name)
    {
        switch ($name) {
            case 'title':
                return new Text($this->webdriver, $this->element->byXPath('.//div[contains(@class, "link-field-title")]//div//input[@type="text"]'));
                break;
            case 'url':
                return new Text($this->webdriver, $this->element->byXPath('.//div[contains(@class, "link-field-url")]//div//input[@type="text"]'));
                break;
        }

        throw new \Exception("The property with the name $name is not defined.");
    }
}
