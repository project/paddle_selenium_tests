<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Element\Display\WegwijsDisplay.
 */

namespace Kanooh\Paddle\Pages\Element\Display;

/**
 * A display with configurable layout, used for wegwijs.
 */
class WegwijsDisplay extends PaddlePanelsDisplay
{

    /**
     * {@inheritdoc}
     */
    protected $supportedLayouts = array(
      'paddle_2_col_9_3_a' => '\Kanooh\Paddle\Pages\Element\Layout\Paddle2Col9to3VariantALayout',
    );
}
