<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Element\Display\ProjectFicheDisplay.
 */

namespace Kanooh\Paddle\Pages\Element\Display;

/**
 * A display with configurable layout, used for newsletters.
 */
class ProjecFicheDisplay extends PaddlePanelsDisplay
{
    /**
     * {@inheritdoc}
     */
    protected $supportedLayouts = array(
      'paddle_3_col_c' => '\Kanooh\Paddle\Pages\Element\Layout\Paddle3ColVariantCLayout',
    );
}
