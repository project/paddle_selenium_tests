<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Element\Display\CoreStatisticsDisplay.
 */

namespace Kanooh\Paddle\Pages\Element\Display;

/**
 * A display with configurable layout, used for core statistics page.
 */
class CoreStatisticsPageDisplay extends PaddlePanelsDisplay
{
    /**
     * {@inheritdoc}
     */
    protected $supportedLayouts = array(
      'paddle_2_col_9_3' => '\Kanooh\Paddle\Pages\Element\Layout\Paddle2Col9to3Layout',
    );
}
