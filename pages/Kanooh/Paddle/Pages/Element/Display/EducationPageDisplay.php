<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Element\Display\EducationPageDisplay.
 */

namespace Kanooh\Paddle\Pages\Element\Display;

/**
 * A display with configurable layout, used for newsletters.
 */
class EducationPageDisplay extends PaddlePanelsDisplay
{

    /**
     * {@inheritdoc}
     */
    protected $supportedLayouts = array(
      'paddle_education' => '\Kanooh\Paddle\Pages\Element\Layout\PaddleEducation',
    );
}
