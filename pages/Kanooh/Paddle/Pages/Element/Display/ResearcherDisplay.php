<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Element\Display\ResearcherDisplay.
 */

namespace Kanooh\Paddle\Pages\Element\Display;

use Kanooh\Paddle\Pages\Element\Region\Region;

/**
 * A display with configurable layout, used for researcher.
 */
class ResearcherDisplay extends PaddlePanelsDisplay
{

    /**
     * {@inheritdoc}
     */
    protected $supportedLayouts = array(
        'paddle_education' => '\Kanooh\Paddle\Pages\Element\Layout\PaddleEducation',
    );

    /**
     * {@inheritdo.
     *
     * @return Region
     *   return full_d region that is present in the current display.
     */
    public function getRandomRegion()
    {
        return $this->regions['full_d'];
    }
}
