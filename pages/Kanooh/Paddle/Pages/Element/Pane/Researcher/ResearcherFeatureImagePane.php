<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Element\Pane\Researcher\ResearcherFeatureImagePane.
 */

namespace Kanooh\Paddle\Pages\Element\Pane\Researcher;

use Kanooh\Paddle\Pages\Element\Pane\Pane;

/**
 * Class for the Researcher feature image.
 *
 * @property \PHPUnit_Extensions_Selenium2TestCase_Element $image
 */
class ResearcherFeatureImagePane extends Pane
{
    /**
     * {@inheritdoc}
     */
    public function __get($name)
    {
        switch ($name) {
            case 'image':
                return $this->getWebdriverElement()->byCssSelector('img');
        }

        throw new \Exception("Property with name $name not defined");
    }
}
