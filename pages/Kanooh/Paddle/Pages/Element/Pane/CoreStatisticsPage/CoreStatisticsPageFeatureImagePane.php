<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Element\Pane\CoreStatisticsPage\CoreStatisticsPageFeatureImagePane.
 */

namespace Kanooh\Paddle\Pages\Element\Pane\CoreStatisticsPage;

use Kanooh\Paddle\Pages\Element\Pane\Pane;

/**
 * Class for the Core Statistics Page Feature image content type.
 *
 * @property \PHPUnit_Extensions_Selenium2TestCase_Element $image
 */
class CoreStatisticsPageFeatureImagePane extends Pane
{
    /**
     * {@inheritdoc}
     */
    public function __get($name)
    {
        switch ($name) {
            case 'image':
                return $this->getWebdriverElement()->byCssSelector('img');
        }

        throw new \Exception("Property with name $name not defined");
    }
}
