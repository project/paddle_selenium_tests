<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Element\Pane\ProjectFiche\EducationPageFeatureImagePane.
 */

namespace Kanooh\Paddle\Pages\Element\Pane\EducationPage;

use Kanooh\Paddle\Pages\Element\Pane\Pane;

/**
 * Class for the Education Page Feature image content type.
 *
 * @property \PHPUnit_Extensions_Selenium2TestCase_Element $image
 */
class EducationPageFeatureImagePane extends Pane
{
    /**
     * {@inheritdoc}
     */
    public function __get($name)
    {
        switch ($name) {
            case 'image':
                return $this->getWebdriverElement()->byCssSelector('img');
        }

        throw new \Exception("Property with name $name not defined");
    }
}
