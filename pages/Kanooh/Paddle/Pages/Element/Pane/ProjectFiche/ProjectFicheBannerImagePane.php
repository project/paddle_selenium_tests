<?php

/**
 * @file
 * Contains \Kanooh\Paddle\Pages\Element\Pane\ProjectFiche\ProjectFicheBannerImagePane.
 */

namespace Kanooh\Paddle\Pages\Element\Pane\ProjectFiche;

use Kanooh\Paddle\Pages\Element\Pane\Pane;

/**
 * Class for the Project Fiche banner image content type.
 *
 * @property \PHPUnit_Extensions_Selenium2TestCase_Element $image
 */
class ProjectFicheBannerImagePane extends Pane
{
    /**
     * {@inheritdoc}
     */
    public function __get($name)
    {
        switch ($name) {
            case 'image':
                return $this->getWebdriverElement()->byCssSelector('img');
        }

        throw new \Exception("Property with name $name not defined");
    }
}
