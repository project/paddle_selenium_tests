<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\Event\ContentType\EventPage\Common\SocialMediaTest.
 */

namespace Kanooh\Paddle\App\Event\ContentType\EventPage\Common;

use Kanooh\Paddle\Apps\Event;
use Kanooh\Paddle\App\SocialMedia\ContentType\Base\SocialMediaTestBase;

/**
 * SocialMediaTest class.
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class SocialMediaTest extends SocialMediaTestBase
{
    /**
     * {@inheritdoc}
     */
    public function setUpPage()
    {
        parent::setUpPage();

        $this->appService->enableApp(new Event);
    }

    /**
     * {@inheritdoc}
     */
    public function setupNode($title = null)
    {
        return $this->contentCreationService->createEventPage($title);
    }
}
