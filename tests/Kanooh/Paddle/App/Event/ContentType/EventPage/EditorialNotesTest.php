<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\Event\ContentType\EventPage\Common\EditorialNotesTest.
 */

namespace Kanooh\Paddle\App\Event\ContentType\EventPage\Common;

use Kanooh\Paddle\Apps\Event;
use Kanooh\Paddle\Core\ContentType\Base\EditorialNotesTestBase;

/**
 * EditorialNotesTest class.
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class EditorialNotesTest extends EditorialNotesTestBase
{
    /**
     * {@inheritdoc}
     */
    public function setUpPage()
    {
        parent::setUpPage();

        $this->appService->enableApp(new Event);
    }

    /**
     * {@inheritdoc}
     */
    public function setupNode($title = null)
    {
        return $this->contentCreationService->createEventPage($title);
    }
}
