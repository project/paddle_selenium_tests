<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\Event\ContentType\EventPage\Common\ListingPaneTest.
 */

namespace Kanooh\Paddle\App\Event\ContentType\EventPage\Common;

use Kanooh\Paddle\Apps\Event;
use Kanooh\Paddle\Core\ContentType\Base\ListingPaneTestBase;
use Kanooh\Paddle\Utilities\AppService;

/**
 * Class ListingPaneTest
 * @package Kanooh\Paddle\App\Event\ContentType\EventPage\Common
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class ListingPaneTest extends ListingPaneTestBase
{
    /**
     * @var AppService
     */
    protected $appService;


    /**
     * {@inheritdoc}
     */
    public function setUpPage()
    {
        parent::setUpPage();

        $this->appService = new AppService($this, $this->userSessionService);
        $this->appService->enableApp(new Event);
    }

    /**
     * {@inheritdoc}
     */
    public function setupNode($title = null)
    {
        return $this->contentCreationService->createEventPage($title);
    }
}
