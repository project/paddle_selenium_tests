<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\Event\ContentType\EventPage\Common\TaxonomyTest.
 */

namespace Kanooh\Paddle\App\Event\ContentType\EventPage\Common;

use Kanooh\Paddle\Apps\Event;
use Kanooh\Paddle\Core\ContentType\Base\TaxonomyTestBase;
use Kanooh\Paddle\Utilities\AppService;

/**
 * Class TaxonomyTest
 * @package Kanooh\Paddle\App\Event\ContentType\EventPage\Common
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class TaxonomyTest extends TaxonomyTestBase
{
    /**
     * @var AppService
     */
    protected $appService;

    /**
     * {@inheritdoc}
     */

    public function setupPage()
    {
        parent::setUpPage();

        $service = new AppService($this, $this->userSessionService);
        $service->enableApp(new Event);
    }

    /**
     * {@inheritdoc}
     */
    public function setupNode($title = null)
    {
        return $this->contentCreationService->createEventPage($title);
    }
}
