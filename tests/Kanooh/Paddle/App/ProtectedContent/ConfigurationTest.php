<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\ProtectedContent\ConfigurationTest.
 */

namespace Kanooh\Paddle\App\ProtectedContent;

use Kanooh\Paddle\Apps\ProtectedContent;
use Kanooh\Paddle\Pages\Admin\Apps\PaddleApps\PaddleProtectedContent\ConfigurePage\ConfigurePage;
use Kanooh\Paddle\Pages\Admin\Apps\PaddleApps\PaddleProtectedContent\ForgotPasswordPage;
use Kanooh\Paddle\Pages\Admin\Apps\PaddleApps\PaddleProtectedContent\LoginPage;
use Kanooh\Paddle\Utilities\AppService;
use Kanooh\Paddle\Utilities\UserSessionService;
use Kanooh\WebDriver\WebDriverTestCase;
use Kanooh\TestDataProvider\AlphanumericTestDataProvider;
use Kanooh\Paddle\Utilities\AssetCreationService;

/**
 * Performs configuration tests on the protected content paddlet.
 *
 * @package Kanooh\Paddle\App\ProtectedContent
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class ConfigurationTest extends WebDriverTestCase
{

    /**
     * @var AlphanumericTestDataProvider
     */
    protected $alphanumericTestDataProvider;

    /**
     * @var AppService
     */
    protected $appService;

    /**
     * @var ConfigurePage
     */
    protected $configurePage;

    /**
     * @var LoginPage
     */
    protected $loginPage;

    /**
     * @var ForgotPasswordPage
     */
    protected $forgotPasswordPage;

    /**
     * @var AssetCreationService
     */
    protected $assetCreationService;

    /**
     * @var UserSessionService
     */
    protected $userSessionService;

    /**
     * {@inheritdoc}
     */
    public function setUpPage()
    {
        parent::setUpPage();

        // Enable the app if it is not yet enabled.
        $this->alphanumericTestDataProvider = new AlphanumericTestDataProvider();
        $this->configurePage = new ConfigurePage($this);
        $this->loginPage = new LoginPage($this);
        $this->forgotPasswordPage = new ForgotPasswordPage($this);
        $this->userSessionService = new UserSessionService($this);
        $this->appService = new AppService($this, $this->userSessionService);
        $this->appService->enableApp(new ProtectedContent());
        $this->assetCreationService = new AssetCreationService($this);

        // Log in as Chief Editor.
        $this->userSessionService->login('ChiefEditor');
    }

    /**
     * Tests the saving of the paddlet's default settings and the configuration.
     */
    public function testConfiguration()
    {
        // Add an image to be used as a background image for login page.
        $atom_1 = $this->assetCreationService->createImage();

        // Check the configurations page and clear all fields.
        $this->configurePage->go();
        $this->configurePage->form->backgroundImage->clear();
        $this->configurePage->form->loginTitle->clear();
        $this->configurePage->form->contactEmail->clear();

        // Get the random values for the configuration form fields.
        $loginTitle = $this->alphanumericTestDataProvider->getValidValue();
        $contactEmail = 'example@example.com';

        // Set a background image.
        $this->configurePage->form->backgroundImage->chooseFile($atom_1['path']);
        $this->configurePage->form->backgroundImage->uploadButton->click();
        $this->configurePage->form->backgroundImage->waitUntilFileUploaded();
        $file_link = parse_url($this->configurePage->form->backgroundImage->getFileLink()->attribute('href'))['path'];

        // Fill other fields on configuration form and save.
        $this->configurePage->form->loginTitle->fill($loginTitle);
        $this->configurePage->form->contactEmail->fill($contactEmail);
        $this->configurePage->contextualToolbar->buttonSave->click();
        $this->configurePage->checkArrival();

        // Wait until the success message appears.
        $this->waitUntilTextIsPresent('Your configurations have been saved.');
        $this->assertTextPresent('Your configurations have been saved.');

        // Logout the user and clear all cache.
        $this->userSessionService->logout();
        drupal_flush_all_caches();

        // Go to the login page.
        $this->loginPage->go();
        $this->loginPage->checkArrival();

        // Check the background image.
        $background = $this->byCssSelector('body')->attribute('style');
        $this->assertContains($file_link, $background);

        // Check the login title.
        $login_title = $this->byXPath('//div[@class="user-login-form"]//h2')->text();
        $this->assertEquals($loginTitle, $login_title);

        // Check the contact email.
        $href = $this->byXPath('//div[@class="user-login-form"]//div[@class="bttn-wrapper kanooh-mail"]//a[@id="contact-button"]')->attribute('href');
        $this->assertContains($contactEmail, $href);

        // Go to the forgot password page.
        $this->forgotPasswordPage->go();
        $this->forgotPasswordPage->checkArrival();

        // Check the contact email on forgot password page.
        $href = $this->byXPath('//div[@class="user-login-form"]//div[@class="bttn-wrapper kanooh-mail"]//a[@id="contact-button"]')->attribute('href');
        $this->assertContains($contactEmail, $href);
    }
}
