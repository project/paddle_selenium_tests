<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\ProjectFiche\ConfigurationTest.
 */

namespace Kanooh\Paddle\App\ProjectFiche;

use Kanooh\Paddle\Apps\ProjectFiche;
use Kanooh\Paddle\Pages\Admin\Apps\PaddleApps\PaddleProjectFiche\ConfigurePage\ConfigurePage;
use Kanooh\Paddle\Utilities\AppService;
use Kanooh\Paddle\Utilities\ContentCreationService;
use Kanooh\Paddle\Utilities\UserSessionService;
use Kanooh\WebDriver\WebDriverTestCase;
use Kanooh\TestDataProvider\AlphanumericTestDataProvider;
use Kanooh\Paddle\Pages\Node\EditPage\ProjectFiche\ProjectFicheEditPage;

/**
 * Performs configuration tests on the ReCaptcha paddlet.
 *
 * @package Kanooh\Paddle\App\ReCaptcha
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class ConfigurationTest extends WebDriverTestCase
{

    /**
     * @var AlphanumericTestDataProvider
     */
    protected $alphanumericTestDataProvider;

    /**
     * @var AppService
     */
    protected $appService;

    /**
     * @var ConfigurePage
     */
    protected $configurePage;

    /**
     * @var ProjectFicheEditPage
     */
    protected $editPage;

    /**
     * @var ContentCreationService
     */
    protected $contentCreationService;

    /**
     * @var UserSessionService
     */
    protected $userSessionService;

    /**
     * {@inheritdoc}
     */
    public function setUpPage()
    {
        parent::setUpPage();

        // Enable the app if it is not yet enabled.
        $this->alphanumericTestDataProvider = new AlphanumericTestDataProvider();
        $this->configurePage = new ConfigurePage($this);
        $this->editPage = new ProjectFicheEditPage($this);
        $this->userSessionService = new UserSessionService($this);
        $this->appService = new AppService($this, $this->userSessionService);
        $this->appService->enableApp(new ProjectFiche());
        $this->contentCreationService = new ContentCreationService($this, $this->userSessionService);

        // Log in as Site Manager.
        $this->userSessionService->login('ChiefEditor');
    }

    /**
     * Tests the saving of the paddlet's default settings and the configuration.
     */
    public function testConfiguration()
    {
        // Check the configurations page and clear all fields
        $this->configurePage->go();
        $this->configurePage->form->bannerImage->clear();
        $this->configurePage->form->currentStage->clear();
        $this->configurePage->form->date->clear();
        $this->configurePage->form->downloads->clear();
        $this->configurePage->form->financeChannels->clear();
        $this->configurePage->form->partners->clear();
        $this->configurePage->form->researcher->clear();
        $this->configurePage->form->website->clear();

        $bannerImage = $this->alphanumericTestDataProvider->getValidValue();
        $currentStage = $this->alphanumericTestDataProvider->getValidValue();
        $date = $this->alphanumericTestDataProvider->getValidValue();
        $downloads = $this->alphanumericTestDataProvider->getValidValue();
        $financeChannels = $this->alphanumericTestDataProvider->getValidValue();
        $partners = $this->alphanumericTestDataProvider->getValidValue();
        $researcher = $this->alphanumericTestDataProvider->getValidValue();
        $website = $this->alphanumericTestDataProvider->getValidValue();

        // Fill all the fields in the configuration form and save
        $this->configurePage->form->bannerImage->fill($bannerImage);
        $this->configurePage->form->currentStage->fill($currentStage);
        $this->configurePage->form->date->fill($date);
        $this->configurePage->form->downloads->fill($downloads);
        $this->configurePage->form->financeChannels->fill($financeChannels);
        $this->configurePage->form->partners->fill($partners);
        $this->configurePage->form->researcher->fill($researcher);
        $this->configurePage->form->website->fill($website);

        $this->configurePage->contextualToolbar->buttonSave->click();
        $this->waitUntilTextIsPresent('Your configurations have been saved.');

        // Create a project fiche.
        $nid = $this->contentCreationService->createProjectFichePage();

        // Go to the edit page and check the labels
        $this->editPage->go($nid);

        $date_label = $this->byXPath('//div[@id="field-paddle-pf-date-add-more-wrapper"]//fieldset//legend//span')->text();
        $this->assertEquals($date_label, $date, '', 0.0, 10, false, true);

        $partners_label = $this->byXPath('//div[@id="field-paddle-pf-partners-add-more-wrapper"]//div/div//label')->text();
        $this->assertEquals($partners_label, $partners, '', 0.0, 10, false, true);

        $currentStage_label = $this->byXPath('//div[@id="field-paddle-pf-current-stage-add-more-wrapper"]//div//label')->text();
        $this->assertEquals($currentStage_label, $currentStage, '', 0.0, 10, false, true);

        $researcher_label = $this->byXPath('//div[@id="field-paddle-pf-proj-researcher-add-more-wrapper"]//div//table[2]//label')->text();
        $this->assertEquals($researcher_label, $researcher, '', 0.0, 10, false, true);

        $bannerImage_label = $this->byXPath('//div[@id="edit-field-paddle-pf-banner-image-und-0-sid-container"]//div//label')->text();
        $this->assertEquals($bannerImage_label, $bannerImage, '', 0.0, 10, false, true);

        $downloads_label = $this->byXPath('//table[@id="field-paddle-pf-downloads-values"]//thead//tr//th//label')->text();
        $this->assertEquals($downloads_label, $downloads, '', 0.0, 10, false, true);

        $website_label = $this->byXPath('//div[@id="field-paddle-pf-project-website-add-more-wrapper"]//div//label')->text();
        $this->assertEquals($website_label, $website, '', 0.0, 10, false, true);

        $financeChannels_label = $this->byXPath('//div[@id="field-paddle-pf-finance-channels-add-more-wrapper"]//div//label')->text();
        $this->assertEquals($financeChannels_label, $financeChannels, '', 0.0, 10, false, true);
    }
}
