<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\ProjectFiche\ProjectFicheTest.
 */

namespace Kanooh\Paddle\App\ProjectFiche;

use Kanooh\Paddle\Apps\ProjectFiche;
use Drupal\Component\Utility\Random;
use Kanooh\Paddle\Pages\Admin\ContentManager\Node\LayoutPage\ProjectFicheLayoutPage;
use Kanooh\Paddle\Pages\Admin\ContentManager\Node\LayoutPage\LayoutPage;
use Kanooh\Paddle\Pages\Admin\ContentManager\Node\ViewPage\ViewPage as AdministrativeNodeViewPage;
use Kanooh\Paddle\Pages\Node\ViewPage\ProjectFiche\ProjectFicheViewPage;
use Kanooh\Paddle\Pages\Node\EditPage\ProjectFiche\ProjectFicheEditPage;
use Kanooh\Paddle\Utilities\AppService;
use Kanooh\Paddle\Utilities\AssetCreationService;
use Kanooh\Paddle\Utilities\ContentCreationService;
use Kanooh\Paddle\Utilities\ScaldService;
use Kanooh\Paddle\Utilities\UserSessionService;
use Kanooh\TestDataProvider\AlphanumericTestDataProvider;
use Kanooh\WebDriver\WebDriverTestCase;
use Kanooh\Paddle\Pages\Element\AutoComplete\AutoComplete;
use Kanooh\Paddle\Pages\Element\ProjectFiche\DownloadsTableRow;
use Jeremeamia\SuperClosure\SerializableClosure;

/**
 * Class ProjectFicheTest
 *
 * @package Kanooh\Paddle\App\ProjectFiche
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class ProjectFicheTest extends WebDriverTestCase
{

    /**
     * @var AdministrativeNodeViewPage
     */
    protected $administrativeNodeViewPage;

    /**
     * @var AlphanumericTestDataProvider
     */
    protected $alphanumericTestDataProvider;

    /**
     * @var AppService
     */
    protected $appService;

    /**
     * @var AssetCreationService
     */
    protected $assetCreationService;

    /**
     * @var ContentCreationService
     */
    protected $contentCreationService;

    /**
     * @var ProjectFicheEditPage
     */
    protected $editPage;

    /**
     * @var ProjectFicheViewPage
     */
    protected $frontendPage;

    /**
     * @var LayoutPage
     */
    protected $layoutPage;

    /**
     * @var ScaldService
     */
    protected $scaldService;

    /**
     * The random data generation class.
     *
     * @var Random $random
     */
    protected $random;

    /**
     * @var UserSessionService
     */
    protected $userSessionService;

    /**
     * {@inheritdoc}
     */
    public function setUpPage()
    {
        parent::setUpPage();

        // Create some instances to use later on.
        $this->administrativeNodeViewPage = new AdministrativeNodeViewPage($this);
        $this->alphanumericTestDataProvider = new AlphanumericTestDataProvider();
        $this->assetCreationService = new AssetCreationService($this);
        $this->editPage = new ProjectFicheEditPage($this);
        $this->layoutPage = new ProjectFicheLayoutPage($this);
        $this->scaldService = new ScaldService($this);
        $this->frontendPage = new ProjectFicheViewPage($this);
        $this->random = new Random();


        // Go to the login page and log in as Chief Editor.
        $this->userSessionService = new UserSessionService($this);
        $this->contentCreationService = new ContentCreationService($this, $this->userSessionService);
        $this->userSessionService->login('ChiefEditor');

        // Enable the app if it is not yet enabled.
        $this->appService = new AppService($this, $this->userSessionService);
        $this->appService->enableApp(new ProjectFiche);
    }

    /**
     * Tests the node edit of the project fiche page.
     */
    public function testNodeEdit()
    {
        // create 2 contact person
        $first_name_1 = $this->random->name(6);
        $last_name_1 = $this->random->name(6);
        $contact_nid_1 = $this->contentCreationService->createContactPerson($first_name_1, $last_name_1);
        $this->administrativeNodeViewPage->go($contact_nid_1);
        $this->administrativeNodeViewPage->contextualToolbar->buttonPublish->click();

        $first_name_2 = $this->random->name(6);
        $last_name_2 = $this->random->name(6);
        $contact_nid_2 = $this->contentCreationService->createContactPerson($first_name_2, $last_name_2);
        $this->administrativeNodeViewPage->go($contact_nid_2);
        $this->administrativeNodeViewPage->contextualToolbar->buttonPublish->click();

        // Create a project fiche.
        $nid = $this->contentCreationService->createProjectFichePage();

        // Create a image atom to test with.
        $atom = $this->assetCreationService->createImage();

        // Create some files for testing.
        $file_1 = $this->assetCreationService->createFile();
        $file_2 = $this->assetCreationService->createFile();

        // Go to the edit page and fill out all custom fields.
        $this->editPage->go($nid);

        $date = date('d/m/Y');
        $end_date = date("d/m/Y", time() + 86400); // tomorrow's date
        $current_stage = $this->alphanumericTestDataProvider->getValidValue();
        $partners = $this->alphanumericTestDataProvider->getValidValue();
        $project_web = 'http://' . $this->alphanumericTestDataProvider->getValidValue() . '.com';
        $finance_channels = $this->alphanumericTestDataProvider->getValidValue();

        // Fill the fields
        $this->editPage->projectFicheEditForm->projectWebsite->fill($project_web);
        $this->editPage->projectFicheEditForm->date->fill($date);
        $this->editPage->projectFicheEditForm->showEndDate->check();
        $this->editPage->projectFicheEditForm->hideDay->check();
        $this->editPage->projectFicheEditForm->currentStage->fill($current_stage);
        $this->editPage->projectFicheEditForm->endDate->fill($end_date);
        $this->editPage->projectFicheEditForm->partners->setBodyText($partners);
        $this->editPage->projectFicheEditForm->financeChannels->setBodyText($finance_channels);

        $this->addTwoResearchers($first_name_1, $first_name_2);

        // Set some downloads.
        $rows = $this->editPage->projectFicheEditForm->downloads->rows;
        /** @var DownloadsTableRow $row */
        $row = reset($rows);
        // Move to the element above the downloads table.
        // This is to make sure the contextual toolbar won't be in the way.
        //$this->moveto($this->editPage->projectFicheEditForm->datePublished->getWebdriverElement());
        $row->atom->selectButton->click();
        $this->scaldService->insertAtom($file_1['id']);

        $count_rows = $this->editPage->projectFicheEditForm->downloads->getNumberOfRows();
        $this->editPage->projectFicheEditForm->downloads->addAnotherItem->click();

        // We need to wait until the new row with the fields appears.
        $table = $this->editPage->projectFicheEditForm->downloads;
        $callable = new SerializableClosure(
            function () use ($count_rows, $table) {
                if ($table->getNumberOfRows() == ($count_rows + 1)) {
                    return true;
                }
            }
        );
        $this->waitUntil($callable, $this->getTimeout());

        $rows = $this->editPage->projectFicheEditForm->downloads->rows;
        /** @var DownloadsTableRow $row */
        $row = end($rows);
        $this->moveto($row->atom->selectButton);
        $row->atom->selectButton->click();
        $this->scaldService->insertAtom($file_2['id']);

        $this->editPage->projectFicheEditForm->bannerImage->selectAtom($atom['id']);

        $this->editPage->contextualToolbar->buttonSave->click();
        $this->administrativeNodeViewPage->checkArrival();

        // Go back to the edit page and verify that everything has been kept.
        $this->editPage->go($nid);


        $this->assertContains($finance_channels, $this->editPage->projectFicheEditForm->financeChannels->getBodyText());
        $this->assertContains($partners, $this->editPage->projectFicheEditForm->partners->getBodyText());
        $this->assertEquals($current_stage, $this->editPage->projectFicheEditForm->currentStage->getContent());
        $this->assertEquals($project_web, $this->editPage->projectFicheEditForm->projectWebsite->getContent());

        $rows = $this->editPage->projectFicheEditForm->downloads->rows;
        $this->assertEquals($file_1['id'], $rows[0]->atom->valueField->value());
        $this->assertEquals($file_2['id'], $rows[1]->atom->valueField->value());

        $rows = $this->editPage->projectFicheEditForm->projectResearcher->rows;
        $this->assertEquals($first_name_1 . ' ' . $last_name_1 . ' (' . $contact_nid_1 . ')', $rows[0]->name->getContent());
        $this->assertEquals($first_name_2 . ' ' . $last_name_2 . ' (' . $contact_nid_2 . ')', $rows[1]->name->getContent());

        $atoms = $this->editPage->projectFicheEditForm->bannerImage->atoms;
        $this->assertNotNull($atoms[0]->title);

        $this->assertEquals($date, $this->editPage->projectFicheEditForm->date->getContent());
        $this->assertTrue($this->editPage->projectFicheEditForm->showEndDate->isChecked());
        $this->assertTrue($this->editPage->projectFicheEditForm->hideDay->isChecked());
        $this->assertEquals($end_date, $this->editPage->projectFicheEditForm->endDate->getContent());


        $this->editPage->contextualToolbar->buttonSave->click();
        $this->administrativeNodeViewPage->checkArrival();
    }

    /**
     * Adds 2 authors to the edit page.
     *
     * @param string $first_name_1
     *   The name of the first researcher.
     * @param $first_name_2
     *   The name of the second researcher.
     */
    protected function addTwoResearchers($first_name_1, $first_name_2)
    {
        $rows = $this->editPage->projectFicheEditForm->projectResearcher->rows;
        /** @var AuthorsTableRow $row */
        $row = reset($rows);
        $row->name->fill($first_name_1);
        $autocomplete = new AutoComplete($this);
        $autocomplete->waitUntilDisplayed();
        $suggestions = $autocomplete->getSuggestions();
        $autocomplete->pickSuggestionByValue($suggestions[0]);

        $count_rows = $this->editPage->projectFicheEditForm->projectResearcher->getNumberOfRows();
        // Move to the element above to prevent Selenium from trying to click on the contextual menu.
        //$this->moveto($this->editPage->creationDate);
        $this->editPage->projectFicheEditForm->projectResearcher->addAnotherItem->click();

        // We need to wait until the new row with the fields appears.
        $table = $this->editPage->projectFicheEditForm->projectResearcher;
        $callable = new SerializableClosure(
            function () use ($count_rows, $table) {
                if ($table->getNumberOfRows() == ($count_rows + 1)) {
                    return true;
                }
            }
        );
        $this->waitUntil($callable, $this->getTimeout());

        $rows = $this->editPage->projectFicheEditForm->projectResearcher->rows;
        /** @var AuthorsTableRow $row */
        $row = end($rows);
        $row->name->fill($first_name_2);
        $autocomplete = new AutoComplete($this);
        $autocomplete->waitUntilDisplayed();
        $suggestions = $autocomplete->getSuggestions();
        $autocomplete->pickSuggestionByValue($suggestions[0]);
    }

    /**
     * Tests the node page layout of the publication page.
     */
    public function testPageLayout()
    {
        // create 2 contact person
        $first_name_1 = $this->random->name(6);
        $last_name_1 = $this->random->name(6);
        $contact_nid_1 = $this->contentCreationService->createContactPerson($first_name_1, $last_name_1);
        $this->administrativeNodeViewPage->go($contact_nid_1);
        $this->administrativeNodeViewPage->contextualToolbar->buttonPublish->click();

        $first_name_2 = $this->random->name(6);
        $last_name_2 = $this->random->name(6);
        $contact_nid_2 = $this->contentCreationService->createContactPerson($first_name_2, $last_name_2);
        $this->administrativeNodeViewPage->go($contact_nid_2);
        $this->administrativeNodeViewPage->contextualToolbar->buttonPublish->click();

        //Create a project fiche.
        $nid = $this->contentCreationService->createProjectFichePage();


         //Create some files for testing.
        $file_1_title = $this->alphanumericTestDataProvider->getValidValue();
        $file_2_title = $this->alphanumericTestDataProvider->getValidValue();
        $file_1 = $this->assetCreationService->createFile(['title' => $file_1_title]);
        $file_2 = $this->assetCreationService->createFile(['title' => $file_2_title]);

        // Go to the edit page and fill out all custom fields.
        $this->editPage->go($nid);

        $date = date('d/m/Y');
        $end_date = date("d/m/Y", time() + 86400); // tomorrow's date
        $current_stage = $this->alphanumericTestDataProvider->getValidValue();
        $partners = $this->alphanumericTestDataProvider->getValidValue();
        $project_web = 'http://' . $this->alphanumericTestDataProvider->getValidValue() . '.com';
        $finance_channels = $this->alphanumericTestDataProvider->getValidValue();
        $description = $this->alphanumericTestDataProvider->getValidValue();

        // Fill the fields
        $this->editPage->projectFicheEditForm->projectWebsite->fill($project_web);
        $this->editPage->projectFicheEditForm->date->fill($date);
        $this->editPage->projectFicheEditForm->showEndDate->check();
        $this->editPage->projectFicheEditForm->hideDay->check();
        $this->editPage->projectFicheEditForm->currentStage->fill($current_stage);
        $this->editPage->projectFicheEditForm->endDate->fill($end_date);
        $this->editPage->projectFicheEditForm->partners->setBodyText($partners);
        $this->editPage->projectFicheEditForm->financeChannels->setBodyText($finance_channels);
        $this->editPage->projectFicheEditForm->body->setBodyText($description);

        $this->addTwoResearchers($first_name_1, $first_name_2);

        // Set some downloads.
        $rows = $this->editPage->projectFicheEditForm->downloads->rows;
        /** @var DownloadsTableRow $row */
        $row = reset($rows);
        // Move to the element above the downloads table.
        // This is to make sure the contextual toolbar won't be in the way.
        //$this->moveto($this->editPage->projectFicheEditForm->datePublished->getWebdriverElement());
        $row->atom->selectButton->click();
        $this->scaldService->insertAtom($file_1['id']);

        $count_rows = $this->editPage->projectFicheEditForm->downloads->getNumberOfRows();
        $this->editPage->projectFicheEditForm->downloads->addAnotherItem->click();

        // We need to wait until the new row with the fields appears.
        $table = $this->editPage->projectFicheEditForm->downloads;
        $callable = new SerializableClosure(
            function () use ($count_rows, $table) {
                if ($table->getNumberOfRows() == ($count_rows + 1)) {
                    return true;
                }
            }
        );
        $this->waitUntil($callable, $this->getTimeout());

        $rows = $this->editPage->projectFicheEditForm->downloads->rows;
        /** @var DownloadsTableRow $row */
        $row = end($rows);
        $this->moveto($row->atom->selectButton);
        $row->atom->selectButton->click();
        $this->scaldService->insertAtom($file_2['id']);

        $this->editPage->contextualToolbar->buttonSave->click();
        $this->administrativeNodeViewPage->checkArrival();


        // Go to the layout page and verify that the related documents pane is
        // first in the right region.
        $this->layoutPage->go($nid);

        $region = $this->layoutPage->display->region('1_a');
        $panes = $region->getPanes();
        foreach ($panes as $pane) {
            $element = $pane->getWebdriverElement();
            $this->assertContains($description, $element->text());
        }


        $region = $this->layoutPage->display->region('2_a');
        $panes = $region->getPanes();
        foreach ($panes as $pane) {
            $element = $pane->getWebdriverElement();
            $atom_1 = scald_atom_load($file_1['id']);
            $atom_2 = scald_atom_load($file_2['id']);

            $this->assertContains($atom_1->base_entity->filename, $element->text());
            $this->assertContains($atom_2->base_entity->filename, $element->text());
        }

        $region = $this->layoutPage->display->region('3_b');
        $panes = $region->getPanes();
        foreach ($panes as $pane) {
            $element = $pane->getWebdriverElement();
            $this->assertContains(date_format(date_create_from_format('d/m/Y', $date), 'F Y'), $element->text());
            $this->assertContains(date_format(date_create_from_format('d/m/Y', $end_date), 'F Y'), $element->text());
        }

        $region = $this->layoutPage->display->region('4_b');
        $panes = $region->getPanes();
        foreach ($panes as $pane) {
            $element = $pane->getWebdriverElement();
            $this->assertContains($partners, $element->text());
        }

        $region = $this->layoutPage->display->region('5_b');
        $panes = $region->getPanes();
        foreach ($panes as $pane) {
            $element = $pane->getWebdriverElement();
            $this->assertContains($project_web, $element->text());
            $this->assertContains($first_name_1 . ' ' . $last_name_1, $element->text());
            $this->assertContains($first_name_2 . ' ' . $last_name_2, $element->text());
            $this->assertContains($finance_channels, $element->text());
        }
    }

    /**
     * Tests the banner image pane to be shown
     *
     * @group Product
     */
    public function testNodeViewBannerImagePane()
    {
        // Create a product and fill out the featured image field.
        $atom_1 = $this->assetCreationService->createImage();
        $atom_2 = $this->assetCreationService->createImage();
        $nid = $this->contentCreationService->createProjectFichePageViaUI();
        $this->editPage->go($nid);
        $this->editPage->projectFicheEditForm->bannerImage->selectAtom($atom_1['id']);
        $this->editPage->contextualToolbar->buttonSave->click();
        $this->administrativeNodeViewPage->checkArrival();

        // Verify that the lead image pane is shown in the front end.
        $this->frontendPage->go($nid);
        $this->assertEquals('atom-id-' . $atom_1['id'], $this->frontendPage->bannerImagePane->image->attribute('class'));

        // Edit the page and replace the featured image.
        $this->editPage->go($nid);
        $this->editPage->projectFicheEditForm->bannerImage->clear();
        $this->editPage->projectFicheEditForm->bannerImage->selectAtom($atom_2['id']);
        $this->editPage->contextualToolbar->buttonSave->click();
        $this->administrativeNodeViewPage->checkArrival();

        // Verify that the updated lead image pane is shown in the front end.
        $this->frontendPage->go($nid);
        $this->assertEquals('atom-id-' . $atom_2['id'], $this->frontendPage->bannerImagePane->image->attribute('class'));

        // Remove the featured image and verify that the pane has been removed.
        $this->editPage->go($nid);
        $this->editPage->projectFicheEditForm->bannerImage->clear();
        $this->editPage->contextualToolbar->buttonSave->click();
        $this->administrativeNodeViewPage->checkArrival();

        $this->frontendPage->go($nid);
        try {
            $this->frontendPage->bannerImagePane;
            $this->fail('there should be no image pane shown.');
        } catch (\PHPUnit_Extensions_Selenium2TestCase_WebDriverException $e) {
            // Everything is fine.
        }
    }
}
