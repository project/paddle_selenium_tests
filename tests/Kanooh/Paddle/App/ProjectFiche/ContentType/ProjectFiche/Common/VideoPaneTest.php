<?php

/**
 * @file
 * Contains
 *   \Kanooh\Paddle\App\ProjectFiche\ContentType\ProjectFiche\Common\VideoPaneTest.
 */

namespace Kanooh\Paddle\App\ProjectFiche\ContentType\ProjectFiche\Common;

use Kanooh\Paddle\Apps\ProjectFiche;
use Kanooh\Paddle\Core\ContentType\Base\VideoPaneBaseTest;
use Kanooh\Paddle\Utilities\AppService;

/**
 * VideoPaneTest class.
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class VideoPaneTest extends VideoPaneBaseTest
{

    /**
     * {@inheritdoc}
     */
    public function setupPage()
    {
        parent::setUpPage();

        $service = new AppService($this, $this->userSessionService);
        $service->enableApp(new ProjectFiche);
    }

    /**
     * {@inheritdoc}
     */
    public function setupNode($title = null)
    {
        return $this->contentCreationService->createProjectFichePage($title);
    }
}
