<?php

/**
 * @file
 * Contains
 *   \Kanooh\Paddle\App\ProjectFiche\ContentType\ProjectFiche\Common\FeaturedImageTest.
 */

namespace Kanooh\Paddle\App\ProjectFiche\ContentType\ProjectFiche\Common;

use Kanooh\Paddle\Apps\ProjectFiche;
use Kanooh\Paddle\Core\ContentType\Base\FeaturedImageTestBase;

/**
 * FeaturedImageTest class.
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class FeaturedImageTest extends FeaturedImageTestBase
{

    /**
     * {@inheritdoc}
     */
    public function setUpPage()
    {
        parent::setUpPage();

        $this->appService->enableApp(new ProjectFiche);
    }

    /**
     * {@inheritdoc}
     */
    public function setupNode($title = null)
    {
        return $this->contentCreationService->createProjectFichePage($title);
    }
}
