<?php

/**
 * @file
 * Contains
 *   \Kanooh\Paddle\App\ProjectFiche\ContentType\ProjectFiche\Common\PaneSectionsTest.
 */

namespace Kanooh\Paddle\App\ProjectFiche\ContentType\ProjectFiche\Common;

use Kanooh\Paddle\Apps\ProjectFiche;
use Kanooh\Paddle\Core\ContentType\Base\PaneSectionsTestBase;
use Kanooh\Paddle\Utilities\AppService;

/**
 * PaneSectionsTest class.
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class PaneSectionsTest extends PaneSectionsTestBase
{

    /**
     * {@inheritdoc}
     */
    public function setupPage()
    {
        parent::setUpPage();

        $service = new AppService($this, $this->userSessionService);
        $service->enableApp(new ProjectFiche);
    }

    /**
     * {@inheritdoc}
     */
    public function setupNode($title = null)
    {
        return $this->contentCreationService->createProjectFichePage($title);
    }
}
