<?php

/**
 * @file
 * Contains
 *   \Kanooh\Paddle\App\ProjectFiche\ContentType\ProjectFiche\Common\PageInformationTest.
 */

namespace Kanooh\Paddle\App\ProjectFiche\ContentType\ProjectFiche\Common;

use Kanooh\Paddle\Apps\ProjectFiche;
use Kanooh\Paddle\Core\ContentType\Base\PageInformationTestBase;
use Kanooh\Paddle\Utilities\AppService;

/**
 * PageInformationTest class.
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class PageInformationTest extends PageInformationTestBase
{

    /**
     * {@inheritdoc}
     */
    public function setupPage()
    {
        parent::setUpPage();

        $service = new AppService($this, $this->userSessionService);
        $service->enableApp(new ProjectFiche);
    }

    /**
     * {@inheritdoc}
     */
    public function setupNode($title = null)
    {
        return $this->contentCreationService->createProjectFichePage($title);
    }
}
