<?php

/**
 * @file
 * Contains
 *   \Kanooh\Paddle\App\ProjectFiche\ContentType\ProjectFiche\Common\NodeRateTest.
 */

namespace Kanooh\Paddle\App\ProjectFiche\ContentType\ProjectFiche\Common;

use Kanooh\Paddle\App\Rate\ContentType\Base\NodeRateTestBase;
use Kanooh\Paddle\Apps\ProjectFiche;

/**
 * NodeRateTest class.
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class NodeRateTest extends NodeRateTestBase
{

    /**
     * {@inheritdoc}
     */

    public function setupPage()
    {
        parent::setUpPage();

        $this->appService->enableApp(new ProjectFiche);
    }

    /**
     * {@inheritdoc}
     */
    public function setupNode($title = null)
    {
        return $this->contentCreationService->createProjectFichePage($title);
    }

    /**
     * {@inheritdoc}
     */
    public function getContentTypeName()
    {
        return 'project_fiche';
    }
}
