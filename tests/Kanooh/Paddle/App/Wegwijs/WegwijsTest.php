<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\Wegwijs\WegwijsTest.
 */

namespace Kanooh\Paddle\App\Wegwijs;

use Kanooh\Paddle\Apps\Wegwijs;
use Kanooh\Paddle\Pages\Admin\ContentManager\Node\LayoutPage\WegwijsLayoutPage;
use Kanooh\Paddle\Pages\Admin\ContentManager\Node\ViewPage\ViewPage as AdministrativeNodeViewPage;
use Kanooh\Paddle\Pages\Node\ViewPage\Wegwijs\WegwijsViewPage;
use Kanooh\Paddle\Pages\Node\EditPage\Wegwijs\WegwijsEditPage;
use Kanooh\Paddle\Utilities\AppService;
use Kanooh\Paddle\Utilities\ContentCreationService;
use Kanooh\Paddle\Utilities\UserSessionService;
use Kanooh\TestDataProvider\AlphanumericTestDataProvider;
use Kanooh\TestDataProvider\UrlTestDataProvider;
use Kanooh\TestDataProvider\EmailTestDataProvider;
use Kanooh\WebDriver\WebDriverTestCase;
use Kanooh\Paddle\Utilities\TaxonomyService;

/**
 * Class WegwijsTest
 *
 * @package Kanooh\Paddle\App\Wegwijs
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class WegwijsTest extends WebDriverTestCase
{

    /**
     * @var AdministrativeNodeViewPage
     */
    protected $administrativeNodeViewPage;

    /**
     * @var AlphanumericTestDataProvider
     */
    protected $alphanumericTestDataProvider;

    /**
     * @var UrlTestDataProvider
     */
    protected $urlTestDataProvider;

    /**
     * @var EmailTestDataProvider
     */
    protected $emailTestDataProvider;

    /**
     * @var AppService
     */
    protected $appService;

    /**
     * @var ContentCreationService
     */
    protected $contentCreationService;

    /**
     * @var WegwijsEditPage
     */
    protected $editPage;

    /**
     * @var WegwijsViewPage
     */
    protected $frontendPage;

    /**
     * @var WegwijsLayoutPage
     */
    protected $layoutPage;

    /**
     * @var TaxonomyService
     */
    protected $taxonomyService;

    /**
     * @var UserSessionService
     */
    protected $userSessionService;

    /**
     * {@inheritdoc}
     */
    public function setUpPage()
    {
        parent::setUpPage();

        // Create some instances to use later on.
        $this->administrativeNodeViewPage = new AdministrativeNodeViewPage($this);
        $this->alphanumericTestDataProvider = new AlphanumericTestDataProvider();
        $this->urlTestDataProvider = new UrlTestDataProvider();
        $this->emailTestDataProvider = new EmailTestDataProvider();
        $this->editPage = new WegwijsEditPage($this);
        $this->frontendPage = new WegwijsViewPage($this);
        $this->taxonomyService = new TaxonomyService();
        $this->layoutPage = new WegwijsLayoutPage($this);

        // Go to the login page and log in as Chief Editor.
        $this->userSessionService = new UserSessionService($this);
        $this->contentCreationService = new ContentCreationService($this, $this->userSessionService);
        $this->userSessionService->login('ChiefEditor');

        // Enable the app if it is not yet enabled.
        $this->appService = new AppService($this, $this->userSessionService);
        $this->appService->enableApp(new Wegwijs);
    }

    /**
     * Tests the node edit of the wegwijs.
     */
    public function testNodeEdit()
    {
        $title = $this->alphanumericTestDataProvider->getValidValue();
        $ovoNumber = $this->alphanumericTestDataProvider->getValidValue();

        // Create a wegwijs and fill all the fields.
        $nid = $this->contentCreationService->createWegwijs($title, $ovoNumber);

        $this->editPage->go($nid);

        $contact_person = $this->alphanumericTestDataProvider->getValidValue();
        $email = $this->emailTestDataProvider->getValidValue();
        $telephone = $this->alphanumericTestDataProvider->getValidValue();
        $website = $this->urlTestDataProvider->getValidValue();
        $thoroughfare = $this->alphanumericTestDataProvider->getValidValue();
        $premise = $this->alphanumericTestDataProvider->getValidValue();
        $postcode = $this->alphanumericTestDataProvider->getValidValue();
        $city = $this->alphanumericTestDataProvider->getValidValue();
        $body = $this->alphanumericTestDataProvider->getValidValue();
        $rechtsvorm = ucfirst($this->alphanumericTestDataProvider->getValidValue());

        // Set the values on edit form.
        $this->editPage->wegwijsEditForm->contactPerson->fill($contact_person);
        $this->editPage->wegwijsEditForm->emailAddress->fill($email);
        $this->editPage->wegwijsEditForm->telephone->fill($telephone);
        $this->editPage->wegwijsEditForm->website->fill($website);
        $this->editPage->wegwijsEditForm->thoroughfare->fill($thoroughfare);
        $this->editPage->wegwijsEditForm->premise->fill($premise);
        $this->editPage->wegwijsEditForm->postCode->fill($postcode);
        $this->editPage->wegwijsEditForm->city->fill($city);
        $this->editPage->wegwijsEditForm->body->setBodyText($body);
        $this->editPage->wegwijsEditForm->rechtsvorm->value($rechtsvorm);
        $this->editPage->wegwijsEditForm->rechtsvormAddButton->click();
        $this->editPage->waitUntilTagIsDisplayed($rechtsvorm);

        $this->editPage->contextualToolbar->buttonSave->click();
        $this->administrativeNodeViewPage->checkArrival();

        // Go to wegwijs edit page and check if all fields are filled.
        $this->editPage->go($nid);

        $this->assertContains($body, $this->editPage->wegwijsEditForm->body->getBodyText());
        $this->assertEquals($ovoNumber, $this->editPage->wegwijsEditForm->ovonumber->getContent());
        $this->assertEquals($contact_person, $this->editPage->wegwijsEditForm->contactPerson->getContent());
        $this->assertEquals($email, $this->editPage->wegwijsEditForm->emailAddress->getContent());
        $this->assertEquals($telephone, $this->editPage->wegwijsEditForm->telephone->getContent());
        $this->assertEquals($website, $this->editPage->wegwijsEditForm->website->getContent());
        $this->assertEquals($thoroughfare, $this->editPage->wegwijsEditForm->thoroughfare->getContent());
        $this->assertEquals($premise, $this->editPage->wegwijsEditForm->premise->getContent());
        $this->assertEquals($postcode, $this->editPage->wegwijsEditForm->postCode->getContent());
        $this->assertEquals($city, $this->editPage->wegwijsEditForm->city->getContent());
        $this->assertEquals($rechtsvorm, $this->editPage->wegwijsEditForm->rechtsvormSelected->text());
    }

    /**
     * Tests the node page layout of the wegwijs.
     */
    public function testPageLayout()
    {
        $title = $this->alphanumericTestDataProvider->getValidValue();
        $ovoNumber = $this->alphanumericTestDataProvider->getValidValue();

        // Create a wegwijs and fill all the fields.
        $nid = $this->contentCreationService->createWegwijs($title, $ovoNumber);

        $this->editPage->go($nid);

        $contact_person = $this->alphanumericTestDataProvider->getValidValue();
        $email = $this->emailTestDataProvider->getValidValue();
        $telephone = $this->alphanumericTestDataProvider->getValidValue();
        $website = $this->urlTestDataProvider->getValidValue();
        $thoroughfare = $this->alphanumericTestDataProvider->getValidValue();
        $premise = $this->alphanumericTestDataProvider->getValidValue();
        $postcode = $this->alphanumericTestDataProvider->getValidValue();
        $city = $this->alphanumericTestDataProvider->getValidValue();
        $body = $this->alphanumericTestDataProvider->getValidValue();

        // Set the values on edit form.
        $this->editPage->wegwijsEditForm->contactPerson->fill($contact_person);
        $this->editPage->wegwijsEditForm->emailAddress->fill($email);
        $this->editPage->wegwijsEditForm->telephone->fill($telephone);
        $this->editPage->wegwijsEditForm->website->fill($website);
        $this->editPage->wegwijsEditForm->thoroughfare->fill($thoroughfare);
        $this->editPage->wegwijsEditForm->premise->fill($premise);
        $this->editPage->wegwijsEditForm->postCode->fill($postcode);
        $this->editPage->wegwijsEditForm->city->fill($city);
        $this->editPage->wegwijsEditForm->body->setBodyText($body);

        $this->editPage->contextualToolbar->buttonSave->click();
        $this->administrativeNodeViewPage->checkArrival();

        // Go to the layout page and verify that every thing is in its correct region.
        $this->layoutPage->go($nid);

        // Verify that the wegwijs title and ovonumber exists in region A.
        $region = $this->layoutPage->display->region('nested_top');
        $panes = $region->getPanes();
        foreach ($panes as $pane) {
            $element = $pane->getWebdriverElement();
            $this->assertContains($title, $element->text());
            $this->assertContains($ovoNumber, $element->text());
        }

        // Verify that the wegwijs contact info exists in region B.
        $region = $this->layoutPage->display->region('nested_left');
        $panes = $region->getPanes();
        foreach ($panes as $pane) {
            $element = $pane->getWebdriverElement();
            $this->assertContains($contact_person, $element->text());
            $this->assertContains($email, $element->text());
            $this->assertContains($telephone, $element->text());
            $this->assertContains($website, $element->text());
        }

        // Verify that the wegwijs location info exists in region C.
        $region = $this->layoutPage->display->region('nested_right');
        $panes = $region->getPanes();
        foreach ($panes as $pane) {
            $element = $pane->getWebdriverElement();
            $this->assertContains($thoroughfare, $element->text());
            $this->assertContains($premise, $element->text());
            $this->assertContains($postcode, $element->text());
            $this->assertContains($city, $element->text());
        }

        // Verify that the wegwijs description exists in region D.
        $region = $this->layoutPage->display->region('nested_bottom');
        $panes = $region->getPanes();
        foreach ($panes as $pane) {
            $element = $pane->getWebdriverElement();
            $this->assertContains($body, $element->text());
        }
    }
}
