<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\Wegwijs\ContentType\Wegwijs\Common\ResponsibleAuthorTest.
 */

namespace Kanooh\Paddle\App\Wegwijs\ContentType\Wegwijs\Common;

use Kanooh\Paddle\Apps\Wegwijs;
use Kanooh\Paddle\Core\ContentType\Base\ResponsibleAuthorTestBase;
use Kanooh\Paddle\Utilities\AppService;

/**
 * ResponsibleAuthorTest class.
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class ResponsibleAuthorTest extends ResponsibleAuthorTestBase
{

    /**
     * {@inheritdoc}
     */
    public function setupPage()
    {
        parent::setUpPage();

        $service = new AppService($this, $this->userSessionService);
        $service->enableApp(new Wegwijs);
    }

    /**
     * {@inheritdoc}
     */
    public function setupNode($title = null)
    {
        return $this->contentCreationService->createWegwijs($title);
    }
}
