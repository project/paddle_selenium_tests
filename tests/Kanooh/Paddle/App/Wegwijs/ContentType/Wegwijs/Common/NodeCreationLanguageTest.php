<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\Wegwijs\ContentType\Wegwijs\Common\NodeCreationLanguageTest.
 */

namespace Kanooh\Paddle\App\Wegwijs\ContentType\Wegwijs\Common;

use Kanooh\Paddle\App\Multilingual\ContentType\Base\NodeCreationLanguageTestBase;
use Kanooh\Paddle\Apps\Wegwijs;
use Kanooh\Paddle\Pages\Admin\ContentManager\AddPage\CreateWegwijsModal;

/**
 * NodeCreationLanguageTest class.
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class NodeCreationLanguageTest extends NodeCreationLanguageTestBase
{

    /**
     * {@inheritdoc}
     */
    public function setupPage()
    {
        parent::setUpPage();
        $this->appService->enableApp(new Wegwijs);
    }

    /**
     * {@inheritdoc}
     */
    public function getModalClassName()
    {
        return '\Kanooh\Paddle\Pages\Admin\ContentManager\AddPage\CreateWegwijsModal';
    }

    /**
     * {@inheritdoc}
     */
    public function fillInAddModalForm($modal)
    {
        /** @var CreateWegwijsModal $modal */
        $modal->title->fill($this->alphanumericTestDataProvider->getValidValue());
        $modal->ovoNumber->fill($this->alphanumericTestDataProvider->getValidValue());
    }

    /**
     * {@inheritdoc}
     */
    public function getContentTypeName()
    {
        return 'wegwijs';
    }
}
