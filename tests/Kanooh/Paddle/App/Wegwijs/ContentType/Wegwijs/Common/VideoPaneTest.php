<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\Wegwijs\ContentType\Wegwijs\Common\VideoPaneTest.
 */

namespace Kanooh\Paddle\App\Wegwijs\ContentType\Wegwijs\Common;

use Kanooh\Paddle\Apps\Wegwijs;
use Kanooh\Paddle\Core\ContentType\Base\VideoPaneBaseTest;
use Kanooh\Paddle\Utilities\AppService;

/**
 * VideoPaneTest class.
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class VideoPaneTest extends VideoPaneBaseTest
{

    /**
     * {@inheritdoc}
     */
    public function setupPage()
    {
        parent::setUpPage();

        $service = new AppService($this, $this->userSessionService);
        $service->enableApp(new Wegwijs);
    }

    /**
     * {@inheritdoc}
     */
    public function setupNode($title = null)
    {
        return $this->contentCreationService->createWegwijs($title);
    }
}
