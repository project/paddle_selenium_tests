<?php

/**
 * @file
 * Contain \Kanooh\Paddle\App\Wegwijs\ContentType\Wegwijs\Common\SocialMediaTest.
 */

namespace Kanooh\Paddle\App\Wegwijs\ContentType\Wegwijs\Common;

use Kanooh\Paddle\App\SocialMedia\ContentType\Base\SocialMediaTestBase;
use Kanooh\Paddle\Apps\Wegwijs;

/**
 * SocialMediaTest class.
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class SocialMediaTest extends SocialMediaTestBase
{

    /**
     * {@inheritdoc}
     */
    public function setUpPage()
    {
        parent::setUpPage();

        $this->appService->enableApp(new Wegwijs);
    }


    /**
     * {@inheritdoc}
     */
    public function setupNode($title = null)
    {
        return $this->contentCreationService->createWegwijs($title);
    }
}
