<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\Wegwijs\ContentType\Wegwijs\Common\ThemeTest.
 */

namespace Kanooh\Paddle\App\Wegwijs\ContentType\Wegwijs\Common;

use Kanooh\Paddle\Apps\Wegwijs;
use Kanooh\Paddle\Core\ContentType\Base\ThemeTestBase;

/**
 * ThemeTest class.
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class ThemeTest extends ThemeTestBase
{

    /**
     * {@inheritdoc}
     */

    public function setupPage()
    {
        parent::setUpPage();

        $this->appService->enableApp(new Wegwijs);
    }

    /**
     * {@inheritdoc}
     */
    public function setupNode($title = null)
    {
        return $this->contentCreationService->createWegwijsViaUI($title);
    }
}
