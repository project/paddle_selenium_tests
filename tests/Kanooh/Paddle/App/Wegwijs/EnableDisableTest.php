<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\Wegwijs\EnableDisableTest.
 */

namespace Kanooh\Paddle\App\Wegwijs;

use Kanooh\Paddle\App\Base\EnableDisableTestBase;
use Kanooh\Paddle\Apps\Wegwijs;

/**
 * Class EnableDisableTest
 *
 * @package Kanooh\Paddle\App\Wegwijs
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class EnableDisableTest extends EnableDisableTestBase
{

    /**
     * {@inheritdoc}
     */
    public function getApp()
    {
        return new Wegwijs;
    }

    /**
     * Test enabling and disabling the Wegwijs app
     *
     * @group store
     * @group enableDisableApp
     */
    public function testEnableDisable()
    {
        // We are overwriting the base enable disable test since this paddlet is not available in paddle store.
        // We also removed the info test for now.
        // @TODO add the info test back and rollback this change once the wegwijs paddlet is available in the paddle store.
        // Install the app.
        $this->appService->enableApp($this->app);
        drupal_cron_run();
        $this->assertTrue(module_exists('paddle_wegwijs'));
        $this->appService->disableApp($this->app);
        $this->assertFalse(module_exists('paddle_wegwijs'));
    }
}
