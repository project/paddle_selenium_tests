<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\EducationPage\EducationPageTest.
 */

namespace Kanooh\Paddle\App\EducationPage;

use Jeremeamia\SuperClosure\SerializableClosure;
use Kanooh\Paddle\Apps\EducationPage;
use Kanooh\Paddle\Pages\Admin\ContentManager\Node\LayoutPage\LayoutPage;
use Kanooh\Paddle\Pages\Admin\ContentManager\Node\LayoutPage\EducationPageLayoutPage;
use Kanooh\Paddle\Pages\Admin\ContentManager\Node\ViewPage\ViewPage as AdministrativeNodeViewPage;
use Kanooh\Paddle\Pages\Element\EducationPage\WhereFollowEducationTableRow;
use Kanooh\Paddle\Pages\Node\ViewPage\EducationPage\EducationPageViewPage;
use Kanooh\Paddle\Pages\Node\EditPage\EducationPage\EducationPageEditPage;
use Kanooh\Paddle\Utilities\AppService;
use Kanooh\Paddle\Utilities\AssetCreationService;
use Kanooh\Paddle\Utilities\ContentCreationService;
use Kanooh\Paddle\Utilities\ScaldService;
use Kanooh\Paddle\Utilities\UserSessionService;
use Kanooh\TestDataProvider\AlphanumericTestDataProvider;
use Kanooh\WebDriver\WebDriverTestCase;
use Kanooh\Paddle\Pages\Element\AutoComplete\AutoComplete;
use Kanooh\Paddle\Utilities\TaxonomyService;

/**
 * Class EducationPageTest
 *
 * @package Kanooh\Paddle\App\EducationPage
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class EducationPageTest extends WebDriverTestCase
{

    /**
     * @var AdministrativeNodeViewPage
     */
    protected $administrativeNodeViewPage;

    /**
     * @var AlphanumericTestDataProvider
     */
    protected $alphanumericTestDataProvider;

    /**
     * @var AppService
     */
    protected $appService;

    /**
     * @var AssetCreationService
     */
    protected $assetCreationService;

    /**
     * @var ContentCreationService
     */
    protected $contentCreationService;

    /**
     * @var EducationPageEditPage
     */
    protected $editPage;

    /**
     * @var EducationPageViewPage
     */
    protected $frontendPage;

    /**
     * @var LayoutPage
     */
    protected $layoutPage;

    /**
     * @var ScaldService
     */
    protected $scaldService;

    /**
     * @var TaxonomyService
     */
    protected $taxonomyService;

    /**
     * @var UserSessionService
     */
    protected $userSessionService;

    /**
     * {@inheritdoc}
     */
    public function setUpPage()
    {
        parent::setUpPage();

        // Create some instances to use later on.
        $this->administrativeNodeViewPage = new AdministrativeNodeViewPage($this);
        $this->alphanumericTestDataProvider = new AlphanumericTestDataProvider();
        $this->assetCreationService = new AssetCreationService($this);
        $this->editPage = new EducationPageEditPage($this);
        $this->scaldService = new ScaldService($this);
        $this->frontendPage = new EducationPageViewPage($this);
        $this->taxonomyService = new TaxonomyService();
        $this->layoutPage = new EducationPageLayoutPage($this);

        // Go to the login page and log in as Chief Editor.
        $this->userSessionService = new UserSessionService($this);
        $this->contentCreationService = new ContentCreationService($this, $this->userSessionService);
        $this->userSessionService->login('ChiefEditor');

        // Enable the app if it is not yet enabled.
        $this->appService = new AppService($this, $this->userSessionService);
        $this->appService->enableApp(new EducationPage);
    }

    /**
     * Tests the node edit of the Education page.
     */
    public function testNodeEdit()
    {
        // Create two organizational units content
        $organization_unit_1 = $this->alphanumericTestDataProvider->getValidValue();
        $organization_unit_2 = $this->alphanumericTestDataProvider->getValidValue();
        $org_nid_1 = $this->contentCreationService->createOrganizationalUnit($organization_unit_1);
        $this->administrativeNodeViewPage->go($org_nid_1);
        $this->administrativeNodeViewPage->contextualToolbar->buttonPublish->click();

        $org_nid_2 = $this->contentCreationService->createOrganizationalUnit($organization_unit_2);
        $this->administrativeNodeViewPage->go($org_nid_2);
        $this->administrativeNodeViewPage->contextualToolbar->buttonPublish->click();

        // Getting the vid of the study area taxonomy vocabulary and create a study area term
        $vid = db_query("SELECT vid FROM {taxonomy_vocabulary} WHERE machine_name = :machine_name", array(':machine_name' => 'study_area'))->fetchField();
        $study_area = $this->alphanumericTestDataProvider->getValidValue();
        $study_tid = $this->taxonomyService->createTerm($vid, $study_area);

        // Getting the vid of the grade taxonomy vocabulary and create a study area term
        $vid = db_query("SELECT vid FROM {taxonomy_vocabulary} WHERE machine_name = :machine_name", array(':machine_name' => 'grade'))->fetchField();
        $grade = $this->alphanumericTestDataProvider->getValidValue();
        $grade_tid = $this->taxonomyService->createTerm($vid, $grade);

        // Create an education page and fill all the fields.
        $nid = $this->contentCreationService->createEducationPage();

        $this->editPage->go($nid);

        $this->editPage->educationPageEditForm->studyArea->selectTerm($study_tid);
        $this->editPage->educationPageEditForm->grade->selectTerm($grade_tid);

        $hours_per_week = $this->alphanumericTestDataProvider->getValidValue();
        $what_will_learn = $this->alphanumericTestDataProvider->getValidValue();
        $entry_requirements = $this->alphanumericTestDataProvider->getValidValue();
        $what_can_i_become = $this->alphanumericTestDataProvider->getValidValue();
        $more_info = $this->alphanumericTestDataProvider->getValidValue();
        $links = $this->alphanumericTestDataProvider->getValidValue();
        $url = 'http://' . $this->alphanumericTestDataProvider->getValidValue() . '.com';
        $spotify = 'https://open.spotify.com/track/2dpaYNEQHiRxtZbfNsse99';

        // Set organizational unit.
        $this->addTwoOrganizationalUnits($organization_unit_1, $organization_unit_2);

        $this->editPage->educationPageEditForm->studyArea->selectTerm($study_tid);
        $this->editPage->educationPageEditForm->hoursPerWeek->fill($hours_per_week);
        $this->editPage->educationPageEditForm->whatWillYouLearn->setBodyText($what_will_learn);
        $this->editPage->educationPageEditForm->entryRequirements->setBodyText($entry_requirements);
        $this->editPage->educationPageEditForm->whatCanIBecome->setBodyText($what_can_i_become);
        $this->editPage->educationPageEditForm->moreInfo->setBodyText($more_info);
        $this->editPage->educationPageEditForm->links->setBodyText($links);
        $this->editPage->educationPageEditForm->moreInfoLink->fill($url);
        $this->editPage->educationPageEditForm->spotify->fill($spotify);

        $this->editPage->contextualToolbar->buttonSave->click();
        $this->administrativeNodeViewPage->checkArrival();

        // Go to education edit page and check if all fields are filled
        $this->editPage->go($nid);

        $rows = $this->editPage->educationPageEditForm->whereFollowEducationTable->rows;
        $this->assertEquals($organization_unit_1 . ' (' . $org_nid_1 . ')', $rows[0]->name->getContent());
        $this->assertEquals($organization_unit_2 . ' (' . $org_nid_2 . ')', $rows[1]->name->getContent());

        $this->assertContains($links, $this->editPage->educationPageEditForm->links->getBodyText());
        $this->assertContains($more_info, $this->editPage->educationPageEditForm->moreInfo->getBodyText());
        $this->assertContains($what_can_i_become, $this->editPage->educationPageEditForm->whatCanIBecome->getBodyText());
        $this->assertContains($entry_requirements, $this->editPage->educationPageEditForm->entryRequirements->getBodyText());
        $this->assertContains($what_will_learn, $this->editPage->educationPageEditForm->whatWillYouLearn->getBodyText());
        $this->assertEquals($hours_per_week, $this->editPage->educationPageEditForm->hoursPerWeek->getContent());

        $term = $this->editPage->educationPageEditForm->studyArea->getTermById($study_tid);
        $this->assertTrue($term->selected());

        $term = $this->editPage->educationPageEditForm->grade->getTermById($grade_tid);
        $this->assertTrue($term->selected());

        $this->assertEquals($url, $this->editPage->educationPageEditForm->moreInfoLink->getContent());
        $this->assertEquals($spotify, $this->editPage->educationPageEditForm->spotify->getContent());
    }

    /**
     * Adds 2 organizational units to the edit page.
     *
     * @param string $organization_unit_1
     *   The name of the first organizational_unit.
     * @param $organization_unit_2
     *   The name of the second organizational_unit.
     */
    protected function addTwoOrganizationalUnits($organization_unit_1, $organization_unit_2)
    {
        $rows = $this->editPage->educationPageEditForm->whereFollowEducationTable->rows;
        /** @var WhereFollowEducationTableRow $row */
        $row = reset($rows);
        $row->name->fill($organization_unit_1);
        $autocomplete = new AutoComplete($this);
        $autocomplete->waitUntilDisplayed();
        $suggestions = $autocomplete->getSuggestions();
        $autocomplete->pickSuggestionByValue($suggestions[0]);

        $count_rows = $this->editPage->educationPageEditForm->whereFollowEducationTable->getNumberOfRows();
        $this->editPage->educationPageEditForm->whereFollowEducationTable->addAnotherItem->click();

        // We need to wait until the new row with the fields appears.
        $table = $this->editPage->educationPageEditForm->whereFollowEducationTable;
        $callable = new SerializableClosure(
            function () use ($count_rows, $table) {
                if ($table->getNumberOfRows() == ($count_rows + 1)) {
                    return true;
                }
            }
        );
        $this->waitUntil($callable, $this->getTimeout());

        $rows = $this->editPage->educationPageEditForm->whereFollowEducationTable->rows;
        /** @var WhereFollowEducationTableRow $row */
        $row = end($rows);
        $row->name->fill($organization_unit_2);
        $autocomplete = new AutoComplete($this);
        $autocomplete->waitUntilDisplayed();
        $suggestions = $autocomplete->getSuggestions();
        $autocomplete->pickSuggestionByValue($suggestions[0]);
    }

    /**
     * Tests the node page layout of the education page.
     */
    public function testPageLayout()
    {
        $organization_unit_1 = $this->alphanumericTestDataProvider->getValidValue();
        $organization_unit_2 = $this->alphanumericTestDataProvider->getValidValue();
        $org_nid_1 = $this->contentCreationService->createOrganizationalUnit($organization_unit_1);
        $this->administrativeNodeViewPage->go($org_nid_1);
        $this->administrativeNodeViewPage->contextualToolbar->buttonPublish->click();

        $org_nid_2 = $this->contentCreationService->createOrganizationalUnit($organization_unit_2);
        $this->administrativeNodeViewPage->go($org_nid_2);
        $this->administrativeNodeViewPage->contextualToolbar->buttonPublish->click();

        $vid = db_query("SELECT vid FROM {taxonomy_vocabulary} WHERE machine_name = :machine_name", array(':machine_name' => 'study_area'))->fetchField();
        $study_area = $this->alphanumericTestDataProvider->getValidValue();
        $study_tid = $this->taxonomyService->createTerm($vid, $study_area);

        $vid = db_query("SELECT vid FROM {taxonomy_vocabulary} WHERE machine_name = :machine_name", array(':machine_name' => 'grade'))->fetchField();
        $grade = $this->alphanumericTestDataProvider->getValidValue();
        $grade_tid = $this->taxonomyService->createTerm($vid, $grade);

        // Create an education page and fill all the fields.
        $nid = $this->contentCreationService->createEducationPage();

        $this->editPage->go($nid);

        $this->editPage->educationPageEditForm->studyArea->selectTerm($study_tid);
        $this->editPage->educationPageEditForm->grade->selectTerm($grade_tid);

        $hours_per_week = $this->alphanumericTestDataProvider->getValidValue();
        $what_will_learn = $this->alphanumericTestDataProvider->getValidValue();
        $entry_requirements = $this->alphanumericTestDataProvider->getValidValue();
        $what_can_i_become = $this->alphanumericTestDataProvider->getValidValue();
        $more_info = $this->alphanumericTestDataProvider->getValidValue();
        $links = $this->alphanumericTestDataProvider->getValidValue();
        $url = 'http://' . $this->alphanumericTestDataProvider->getValidValue() . '.com';
        $spotify = 'https://open.spotify.com/track/2dpaYNEQHiRxtZbfNsse99';

        // Set organizational unit.
        $this->addTwoOrganizationalUnits($organization_unit_1, $organization_unit_2);

        $this->editPage->educationPageEditForm->studyArea->selectTerm($study_tid);
        $this->editPage->educationPageEditForm->hoursPerWeek->fill($hours_per_week);
        $this->editPage->educationPageEditForm->whatWillYouLearn->setBodyText($what_will_learn);
        $this->editPage->educationPageEditForm->entryRequirements->setBodyText($entry_requirements);
        $this->editPage->educationPageEditForm->whatCanIBecome->setBodyText($what_can_i_become);
        $this->editPage->educationPageEditForm->moreInfo->setBodyText($more_info);
        $this->editPage->educationPageEditForm->links->setBodyText($links);
        $this->editPage->educationPageEditForm->moreInfoLink->fill($url);
        $this->editPage->educationPageEditForm->spotify->fill($spotify);

        $this->editPage->contextualToolbar->buttonSave->click();
        $this->administrativeNodeViewPage->checkArrival();

        // Go to the layout page and verify that every thing is in its correct region
        $this->layoutPage->go($nid);
        $hoursPerWeek_label = $this->byXPath('//div[@id="hours_per_week"]//h2')->text();
        $studyArea_label = $this->byXPath('//div[@id="study_area"]//h2')->text();
        $grade_label = $this->byXPath('//div[@id="grade"]//h2')->text();
        $whatWillYouLearn_label = $this->byXPath('//div[@id="what_will_you_learn"]//h2')->text();
        $entryRequirements_label = $this->byXPath('//div[@id="entry_requirements"]//h2')->text();
        $whatCanIBecome_label = $this->byXPath('//div[@id="what_can_i_become"]//h2')->text();
        $moreInfo_label = $this->byXPath('//div[@id="more_info"]//h2')->text();
        $links_label = $this->byXPath('//div[@id="links"]//h2')->text();
        $whereFollowEducation_label = $this->byXPath('//div[@id="organizational_units"]//h2')->text();

        // Verify that the education info exists in region B
        $region = $this->layoutPage->display->region('nested_9_b');
        $panes = $region->getPanes();
        $i = 1;
        foreach ($panes as $pane) {
            $element = $pane->getWebdriverElement();
            if ($i == 1) {
                $this->assertContains($hours_per_week, $element->text());
            }
            if ($i == 2) {
                $study_term = taxonomy_term_load($study_tid);
                $this->assertContains($study_term->name, $element->text());
            }
            if ($i == 3) {
                $grade_term = taxonomy_term_load($grade_tid);
                $this->assertContains($grade_term->name, $element->text());
            }
            if ($i == 4) {
                $this->assertContains($what_will_learn, $element->text());
                $this->assertContains($url, $element->text());
            }
            if ($i == 5) {
                $this->assertContains($entry_requirements, $element->text());
            }
            if ($i == 6) {
                $this->assertContains($what_can_i_become, $element->text());
            }
            if ($i == 7) {
                $this->assertContains($organization_unit_1, $element->text());
                $this->assertContains($organization_unit_2, $element->text());
            }
            if ($i == 8) {
                $this->assertContains($more_info, $element->text());
            }
            if ($i == 9) {
                $this->assertContains($links, $element->text());

            }
            $i++;
        }

        // Verifiy that the spotify iframe and navigation links exists in region C
        $region = $this->layoutPage->display->region('nested_3_c');
        $panes = $region->getPanes();
        $i = 1;
        foreach ($panes as $pane) {
            $element = $pane->getWebdriverElement();
            if ($i == 1) {
                $src = $element->byXPath('//iframe')->attribute('src');
                $src_array = explode('=', $src);
                $this->assertEquals(urldecode($src_array[1]), $spotify);
            }
            if ($i == 2) {
                $this->assertContains($hoursPerWeek_label, $element->text());
                $this->assertContains($studyArea_label, $element->text());
                $this->assertContains($grade_label, $element->text());
                $this->assertContains($whatWillYouLearn_label, $element->text());
                $this->assertContains($entryRequirements_label, $element->text());
                $this->assertContains($whatCanIBecome_label, $element->text());
                $this->assertContains($whereFollowEducation_label, $element->text());
                $this->assertContains($moreInfo_label, $element->text());
                $this->assertContains($links_label, $element->text());
            }
            $i++;
        }
    }

    /**
     * Tests the feature image pane to be shown
     *
     * @group Product
     */
    public function testNodeViewFeatureImagePane()
    {
        // Create a product and fill out the featured image field.
        $atom_1 = $this->assetCreationService->createImage();
        $atom_2 = $this->assetCreationService->createImage();
        $nid = $this->contentCreationService->createEducationPageViaUI();
        $this->editPage->go($nid);
        $this->editPage->educationPageEditForm->featureImage->selectAtom($atom_1['id']);
        $this->editPage->contextualToolbar->buttonSave->click();
        $this->administrativeNodeViewPage->checkArrival();

        // Verify that the banner image pane is shown in the front end.
        $this->frontendPage->go($nid);
        $this->assertEquals('atom-id-' . $atom_1['id'], $this->frontendPage->featureImagePane->image->attribute('class'));

        // Edit the page and replace the featured image.
        $this->editPage->go($nid);
        $this->editPage->educationPageEditForm->featureImage->clear();
        $this->editPage->educationPageEditForm->featureImage->selectAtom($atom_2['id']);
        $this->editPage->contextualToolbar->buttonSave->click();
        $this->administrativeNodeViewPage->checkArrival();

        // Verify that the updated feature image pane is shown in the front end.
        $this->frontendPage->go($nid);
        $this->assertEquals('atom-id-' . $atom_2['id'], $this->frontendPage->featureImagePane->image->attribute('class'));

        // Remove the featured image and verify that the pane has been removed.
        $this->editPage->go($nid);
        $this->editPage->educationPageEditForm->featureImage->clear();
        $this->editPage->contextualToolbar->buttonSave->click();
        $this->administrativeNodeViewPage->checkArrival();

        $this->frontendPage->go($nid);
        try {
            $this->frontendPage->featureImagePane;
            $this->fail('there should be no image pane shown.');
        } catch (\PHPUnit_Extensions_Selenium2TestCase_WebDriverException $e) {
            // Everything is fine.
        }
    }
}
