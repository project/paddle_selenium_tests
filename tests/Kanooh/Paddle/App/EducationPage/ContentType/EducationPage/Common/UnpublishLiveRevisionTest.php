<?php

/**
 * @file
 * Contains
 *   \Kanooh\Paddle\App\EducationPage\ContentType\EducationPage\Common\UnpublishLiveRevisionTest.
 */

namespace Kanooh\Paddle\App\EducationPage\ContentType\EducationPage\Common;

use Kanooh\Paddle\Apps\EducationPage;
use Kanooh\Paddle\Core\ContentType\Base\UnpublishLiveRevisionTestBase;
use Kanooh\Paddle\Utilities\AppService;

/**
 * UnpublishLiveRevisionTest class.
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class UnpublishLiveRevisionTest extends UnpublishLiveRevisionTestBase
{

    /**
     * {@inheritdoc}
     */
    public function setupPage()
    {
        parent::setUpPage();

        $service = new AppService($this, $this->userSessionService);
        $service->enableApp(new EducationPage);
    }

    /**
     * {@inheritdoc}
     */
    public function setupNode($title = null)
    {
        return $this->contentCreationService->createEducationPage($title);
    }
}
