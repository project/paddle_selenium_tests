<?php

/**
 * @file
 * Contains
 *   \Kanooh\Paddle\App\EducationPage\ContentType\EducationPage\Common\ListingPaneTest.
 */

namespace Kanooh\Paddle\App\EducationPage\ContentType\EducationPage\Common;

use Kanooh\Paddle\Apps\EducationPage;
use Kanooh\Paddle\Core\ContentType\Base\ListingPaneTestBase;

/**
 * ListingPaneTest class.
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class ListingPaneTest extends ListingPaneTestBase
{

    /**
     * {@inheritdoc}
     */
    public function setUpPage()
    {
        parent::setUpPage();

        $this->appService->enableApp(new EducationPage);
    }

    /**
     * {@inheritdoc}
     */
    public function setupNode($title = null)
    {
        return $this->contentCreationService->createEducationPage($title);
    }
}
