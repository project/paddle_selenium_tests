<?php

/**
 * @file
 * Contains
 *   \Kanooh\Paddle\App\EducationPage\ContentType\EducationPage\Common\SchedulingTest.
 */

namespace Kanooh\Paddle\App\EducationPage\ContentType\EducationPage\Common;

use Kanooh\Paddle\Apps\EducationPage;
use Kanooh\Paddle\Core\ContentType\Base\SchedulingTestBase;
use Kanooh\Paddle\Utilities\AppService;

/**
 * SchedulingTest class.
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class SchedulingTest extends SchedulingTestBase
{

    /**
     * {@inheritdoc}
     */
    public function setupPage()
    {
        parent::setUpPage();

        $service = new AppService($this, $this->userSessionService);
        $service->enableApp(new EducationPage);
    }

    /**
     * {@inheritdoc}
     */
    public function setupNode($title = null)
    {
        return $this->contentCreationService->createEducationPage($title);
    }
}
