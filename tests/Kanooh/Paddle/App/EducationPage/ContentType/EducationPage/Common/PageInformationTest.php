<?php

/**
 * @file
 * Contains
 *   \Kanooh\Paddle\App\EducationPage\ContentType\EducationPage\Common\PageInformationTest.
 */

namespace Kanooh\Paddle\App\EducationPage\ContentType\EducationPage\Common;

use Kanooh\Paddle\Apps\EducationPage;
use Kanooh\Paddle\Core\ContentType\Base\PageInformationTestBase;
use Kanooh\Paddle\Utilities\AppService;

/**
 * PageInformationTest class.
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class PageInformationTest extends PageInformationTestBase
{

    /**
     * {@inheritdoc}
     */
    public function setupPage()
    {
        parent::setUpPage();

        $service = new AppService($this, $this->userSessionService);
        $service->enableApp(new EducationPage);
    }

    /**
     * {@inheritdoc}
     */
    public function setupNode($title = null)
    {
        return $this->contentCreationService->createEducationPage($title);
    }
}
