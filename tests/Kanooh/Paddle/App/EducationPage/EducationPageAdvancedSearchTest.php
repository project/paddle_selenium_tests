<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\EducationPage\EducationPageAdvancedSearchTest.
 */

namespace Kanooh\Paddle\App\EducationPage;

use Kanooh\Paddle\Apps\EducationPage;
use Kanooh\Paddle\Pages\Admin\ContentManager\Node\ViewPage\ViewPage as AdministrativeNodeViewPage;
use Kanooh\Paddle\Pages\Node\EditPage\AdvancedSearch\AdvancedSearchPage;
use Kanooh\Paddle\Pages\Node\EditPage\EducationPage\EducationPageEditPage;
use Kanooh\Paddle\Pages\Node\ViewPage\AdvancedSearch\AdvancedSearchViewPage;
use Kanooh\Paddle\Pages\Node\ViewPage\EducationPage\EducationPageViewPage;
use Kanooh\Paddle\Utilities\AppService;
use Kanooh\Paddle\Utilities\ContentCreationService;
use Kanooh\Paddle\Utilities\DrupalApi\DrupalSearchApiApi;
use Kanooh\Paddle\Utilities\TaxonomyService;
use Kanooh\Paddle\Utilities\UserSessionService;
use Kanooh\TestDataProvider\AlphanumericTestDataProvider;
use Kanooh\WebDriver\WebDriverTestCase;

/**
 * Class EducationPageAdvancedSearchTest
 * @package Kanooh\Paddle\App\EducationPage
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class EducationPageAdvancedSearchTest extends WebDriverTestCase
{
    /**
     * @var AdministrativeNodeViewPage
     */
    protected $administrativeNodeViewPage;

    /**
     * @var AdvancedSearchPage
     */
    protected $advancedSearchEditPage;

    /**
     * @var AdvancedSearchViewPage
     */
    protected $advancedSearchFrontViewPage;

    /**
     * @var AlphanumericTestDataProvider
     */
    protected $alphanumericTestDataProvider;

    /**
     * @var AppService
     */
    protected $appService;

    /**
     * @var ContentCreationService
     */
    protected $contentCreationService;

    /**
     * @var DrupalSearchApiApi
     */
    protected $drupalSearchApiApi;

    /**
     * @var EducationPageEditPage
     */
    protected $educationEditPage;

    /**
     * @var EducationPageViewPage
     */
    protected $educationFrontPage;

    /**
     * @var TaxonomyService
     */
    protected $taxonomyService;

    /**
     * @var UserSessionService
     */
    protected $userSessionService;

    /**
     * {@inheritdoc}
     */
    public function setUpPage()
    {
        parent::setUpPage();

        // Create some instances to use later on.
        $this->administrativeNodeViewPage = new AdministrativeNodeViewPage($this);
        $this->advancedSearchEditPage = new AdvancedSearchPage($this);
        $this->advancedSearchFrontViewPage = new AdvancedSearchViewPage($this);
        $this->alphanumericTestDataProvider = new AlphanumericTestDataProvider();
        $this->drupalSearchApiApi = new DrupalSearchApiApi($this);
        $this->educationEditPage = new EducationPageEditPage($this);
        $this->educationFrontPage = new EducationPageViewPage($this);
        $this->taxonomyService = new TaxonomyService();

        // Go to the login page and log in as Chief Editor.
        $this->userSessionService = new UserSessionService($this);
        $this->contentCreationService = new ContentCreationService($this, $this->userSessionService);
        $this->userSessionService->login('ChiefEditor');

        // Enable the app if it is not yet enabled.
        $this->appService = new AppService($this, $this->userSessionService);
        $this->appService->enableApp(new EducationPage);
    }

    /**
     * Tests the study area on advanced search page
     *
     * @group facets
     */
    public function testAdvancedSearchExtraFields()
    {
        // Create an education page.
        $edu_title = $this->alphanumericTestDataProvider->getValidValue();
        $pub_nid = $this->contentCreationService->createEducationPage($edu_title);

        // Create an advanced search page.
        $nid = $this->contentCreationService->createAdvancedSearchPage();
        variable_set('education_page_advanced_search_page', $nid);

        // Getting the vid of the study area taxonomy vocabulary and create a study area term
        $vid = db_query("SELECT vid FROM {taxonomy_vocabulary} WHERE machine_name = :machine_name", array(':machine_name' => 'study_area'))->fetchField();
        $study_area = $this->alphanumericTestDataProvider->getValidValue();
        $study_tid = $this->taxonomyService->createTerm($vid, $study_area);

        // Getting the vid of the study area taxonomy vocabulary and create a grade term
        $vid = db_query("SELECT vid FROM {taxonomy_vocabulary} WHERE machine_name = :machine_name", array(':machine_name' => 'grade'))->fetchField();
        $grade = $this->alphanumericTestDataProvider->getValidValue();
        $grade_tid = $this->taxonomyService->createTerm($vid, $grade);

        $body = $this->alphanumericTestDataProvider->getValidValue();

        // Go to the education page edit page and select a study area.
        $this->educationEditPage->go($pub_nid);

        $this->educationEditPage->educationPageEditForm->studyArea->selectTerm($study_tid);
        $this->educationEditPage->educationPageEditForm->grade->selectTerm($grade_tid);
        $this->educationEditPage->body->setBodyText($body);

        $this->educationEditPage->contextualToolbar->buttonSave->click();

        // Publish the education page.
        $this->administrativeNodeViewPage->checkArrival();
        $this->administrativeNodeViewPage->contextualToolbar->buttonPublish->click();
        $this->administrativeNodeViewPage->checkArrival();

        // Reindex the node index.
        // Index all the nodes and commit the search index.
        $this->drupalSearchApiApi->indexItems('node_index');
        $this->drupalSearchApiApi->commitIndex('node_index');

        // Go to the advanced search edit page and check the new fields.
        $this->advancedSearchEditPage->go($nid);
        $this->assertFalse($this->advancedSearchEditPage->advancedSearchForm->filterStudyAreaCheckbox->isChecked());
        $this->assertFalse($this->advancedSearchEditPage->advancedSearchForm->filterGradeCheckbox->isChecked());
        $this->advancedSearchEditPage->advancedSearchForm->filterStudyAreaCheckbox->check();
        $this->advancedSearchEditPage->advancedSearchForm->filterGradeCheckbox->check();
        $this->advancedSearchEditPage->advancedSearchForm->filterEducationPageCheckbox->check();

        $this->advancedSearchEditPage->contextualToolbar->buttonSave->click();
        $this->administrativeNodeViewPage->checkArrival();

        // Go to the front end of the advanced search page and verify 2 new
        // panes with the correct content are shown.
        $this->advancedSearchFrontViewPage->go($nid);
        $this->assertNotEmpty($this->advancedSearchFrontViewPage->studyAreaFilterFacet->getInactiveLinkByValue($study_tid));
        $this->assertNotEmpty($this->advancedSearchFrontViewPage->gradeFilterFacet->getInactiveLinkByValue($grade_tid));

        // Apply the study area filter.
        $xpath = '//div[contains(@class, "pane-study-area-filter")]//a[contains(@href, "f%5B0%5D=pas_education_page_study_area%3A' . $study_tid . '")]';
        $element = $this->byXPath($xpath);
        $element->click();
        $this->advancedSearchFrontViewPage->checkArrival();
        $this->assertNotEmpty($this->advancedSearchFrontViewPage->studyAreaFilterFacet->getActiveLinkByValue($study_tid));

        // Uncheck the study area filter to apply the grade filter.
        $xpath = '//div[contains(@class, "pane-study-area-filter")]//input[contains(@class, "facetapi-checkbox")]';
        $element = $this->byXPath($xpath);
        $element->click();
        $this->advancedSearchFrontViewPage->checkArrival();

        // Apply the grade filter.
        $xpath = '//div[contains(@class, "pane-grade-filter")]//a[contains(@href, "f%5B0%5D=pas_education_page_grade%3A' . $grade_tid . '")]';
        $element = $this->byXPath($xpath);
        $element->click();
        $this->advancedSearchFrontViewPage->checkArrival();
        $this->assertNotEmpty($this->advancedSearchFrontViewPage->gradeFilterFacet->getActiveLinkByValue($grade_tid));

        // Uncheck the checkboxes and verify that the pane is gone.
        $this->advancedSearchEditPage->go($nid);
        $this->assertTrue($this->advancedSearchEditPage->advancedSearchForm->filterEducationPageCheckbox->isChecked());
        $this->assertTrue($this->advancedSearchEditPage->advancedSearchForm->filterStudyAreaCheckbox->isChecked());
        $this->assertTrue($this->advancedSearchEditPage->advancedSearchForm->filterGradeCheckbox->isChecked());
        $this->advancedSearchEditPage->advancedSearchForm->filterEducationPageCheckbox->uncheck();
        $this->advancedSearchEditPage->advancedSearchForm->filterStudyAreaCheckbox->uncheck();
        $this->advancedSearchEditPage->advancedSearchForm->filterGradeCheckbox->uncheck();

        $this->advancedSearchEditPage->contextualToolbar->buttonSave->click();
        $this->administrativeNodeViewPage->checkArrival();

        $this->advancedSearchFrontViewPage->go($nid);

        try {
            $this->advancedSearchFrontViewPage->studyAreaFilterFacet;
            $this->advancedSearchFrontViewPage->gradeFilterFacet;
            $this->fail('The study area facet should not be shown.');
            $this->fail('The grade facet should not be shown.');
        } catch (\Exception $e) {
            // Do nothing.
        }
    }
}
