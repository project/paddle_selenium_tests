<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\EducationPage\ConfigurationTest.
 */

namespace Kanooh\Paddle\App\EducationPage;

use Kanooh\Paddle\Apps\EducationPage;
use Kanooh\Paddle\Pages\Admin\Apps\PaddleApps\PaddleEducationPage\ConfigurePage\ConfigurePage;
use Kanooh\Paddle\Utilities\AppService;
use Kanooh\Paddle\Utilities\ContentCreationService;
use Kanooh\Paddle\Utilities\UserSessionService;
use Kanooh\WebDriver\WebDriverTestCase;
use Kanooh\TestDataProvider\AlphanumericTestDataProvider;
use Kanooh\Paddle\Pages\Node\EditPage\EducationPage\EducationPageEditPage;
use Kanooh\Paddle\Apps\Multilingual;
use Kanooh\Paddle\Utilities\MultilingualService;
use Jeremeamia\SuperClosure\SerializableClosure;
use Kanooh\Paddle\Pages\Element\AutoComplete\AutoComplete;
use Kanooh\Paddle\Utilities\TaxonomyService;
use Kanooh\Paddle\Pages\Element\EducationPage\WhereFollowEducationTableRow;
use Kanooh\Paddle\Pages\Node\ViewPage\EducationPage\EducationPageViewPage;
use Kanooh\Paddle\Pages\Admin\ContentManager\Node\ViewPage\ViewPage as AdministrativeNodeViewPage;

/**
 * Performs configuration tests on the ReCaptcha paddlet.
 *
 * @package Kanooh\Paddle\App\ReCaptcha
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class ConfigurationTest extends WebDriverTestCase
{

    /**
     * @var AlphanumericTestDataProvider
     */
    protected $alphanumericTestDataProvider;

    /**
     * @var AdministrativeNodeViewPage
     */
    protected $administrativeNodeViewPage;

    /**
     * @var AppService
     */
    protected $appService;

    /**
     * @var ConfigurePage
     */
    protected $configurePage;

    /**
     * @var EducationPageEditPage
     */
    protected $editPage;

    /**
     * @var EducationPageViewPage
     */
    protected $frontendPage;


    /**
     * @var ContentCreationService
     */
    protected $contentCreationService;

    /**
     * @var UserSessionService
     */
    protected $userSessionService;

    /**
     * @var TaxonomyService
     */
    protected $taxonomyService;

    /**
     * {@inheritdoc}
     */
    public function setUpPage()
    {
        parent::setUpPage();

        // Enable the app if it is not yet enabled.
        $this->alphanumericTestDataProvider = new AlphanumericTestDataProvider();
        $this->administrativeNodeViewPage = new AdministrativeNodeViewPage($this);
        $this->configurePage = new ConfigurePage($this);
        $this->editPage = new EducationPageEditPage($this);
        $this->userSessionService = new UserSessionService($this);
        $this->appService = new AppService($this, $this->userSessionService);
        $this->taxonomyService = new TaxonomyService();
        $this->frontendPage = new EducationPageViewPage($this);
        // Disable the Multilingual app if needed.
        $this->appService->disableApp(new Multilingual);
        if (module_exists('i18n_variable')) {
            module_disable(array('i18n_variable'));
        }
        $this->appService->enableApp(new EducationPage());
        $this->contentCreationService = new ContentCreationService($this, $this->userSessionService);

        // Log in as Site Manager.
        $this->userSessionService->login('ChiefEditor');
    }

    /**
     * Tests the saving of the paddlet's default settings and the configuration.
     */
    public function testConfiguration()
    {
        // Check the configurations page and clear all fields.
        $this->configurePage->go();
        $this->configurePage->form->hoursPerWeek->clear();
        $this->configurePage->form->studyArea->clear();
        $this->configurePage->form->grade->clear();
        $this->configurePage->form->whatWillYouLearn->clear();
        $this->configurePage->form->entryRequirements->clear();
        $this->configurePage->form->whatCanIBecome->clear();
        $this->configurePage->form->whereFollowEducation->clear();
        $this->configurePage->form->moreInfo->clear();
        $this->configurePage->form->links->clear();

        // Get the random values for the configuration form fields.
        $hoursPerWeek = $this->alphanumericTestDataProvider->getValidValue();
        $studyArea = $this->alphanumericTestDataProvider->getValidValue();
        $grade = $this->alphanumericTestDataProvider->getValidValue();
        $whatWillYouLearn = $this->alphanumericTestDataProvider->getValidValue();
        $entryRequirements = $this->alphanumericTestDataProvider->getValidValue();
        $whatCanIBecome = $this->alphanumericTestDataProvider->getValidValue();
        $whereFollowEducation = $this->alphanumericTestDataProvider->getValidValue();
        $moreInfo = $this->alphanumericTestDataProvider->getValidValue();
        $links = $this->alphanumericTestDataProvider->getValidValue();

        // Fill all the fields in the configuration form and save.
        $this->configurePage->form->hoursPerWeek->fill($hoursPerWeek);
        $this->configurePage->form->studyArea->fill($studyArea);
        $this->configurePage->form->grade->fill($grade);
        $this->configurePage->form->whatWillYouLearn->fill($whatWillYouLearn);
        $this->configurePage->form->entryRequirements->fill($entryRequirements);
        $this->configurePage->form->whatCanIBecome->fill($whatCanIBecome);
        $this->configurePage->form->whereFollowEducation->fill($whereFollowEducation);
        $this->configurePage->form->moreInfo->fill($moreInfo);
        $this->configurePage->form->links->fill($links);

        $this->configurePage->contextualToolbar->buttonSave->click();
        $this->configurePage->checkArrival();

        $this->waitUntilTextIsPresent('Your configurations have been saved.');
        $this->assertTextPresent('Your configurations have been saved.');
        global $conf;
        $conf = variable_initialize();

        // Create an education page.
        $nid = $this->contentCreationService->createEducationPage();

        // Go to the edit page and check the labels
        $this->editPage->go($nid);
        global $conf;
        $conf = variable_initialize();

        $hoursPerWeek_label = $this->byXPath('//div[@id="field-paddle-ep-hours-per-week-add-more-wrapper"]//div//label')->text();
        $this->assertEquals($hoursPerWeek_label, $hoursPerWeek, '', 0.0, 10, false, true);

        $studyArea_label = $this->byXPath('//div[@id="edit-field-paddle-ep-study-area"]//div//label')->text();
        $this->assertEquals($studyArea_label, $studyArea, '', 0.0, 10, false, true);

        $grade_label = $this->byXPath('//div[@id="edit-field-paddle-ep-grade"]//div//label')->text();
        $this->assertEquals($grade_label, $grade, '', 0.0, 10, false, true);

        $whatWillYouLearn_label = $this->byXPath('//div[@id="field-paddle-ep-what-will-learn-add-more-wrapper"]//div//div//label')->text();
        $this->assertEquals($whatWillYouLearn_label, $whatWillYouLearn, '', 0.0, 10, false, true);

        $entryRequirements_label = $this->byXPath('//div[@id="field-paddle-ep-entry-requirment-add-more-wrapper"]//div//div//label')->text();
        $this->assertEquals($entryRequirements_label, $entryRequirements, '', 0.0, 10, false, true);

        $whatCanIBecome_label = $this->byXPath('//div[@id="field-paddle-ep-what-can-i-becom-add-more-wrapper"]//div/div/label')->text();
        $this->assertEquals($whatCanIBecome_label, $whatCanIBecome, '', 0.0, 10, false, true);

        $whereFollowEducation_label = $this->byXPath('//table[@id="field-paddle-ep-where-follow-edu-values"]//thead//tr//th//label')->text();
        $this->assertEquals($whereFollowEducation_label, $whereFollowEducation, '', 0.0, 10, false, true);

        $moreInfo_label = $this->byXPath('//div[@id="field-paddle-ep-more-info-add-more-wrapper"]//div//div//label')->text();
        $this->assertEquals($moreInfo_label, $moreInfo, '', 0.0, 10, false, true);

        $links_label = $this->byXPath('//div[@id="field-paddle-ep-links-add-more-wrapper"]//div//div//label')->text();
        $this->assertEquals($links_label, $links, '', 0.0, 10, false, true);

        $this->editPage->contextualToolbar->buttonSave->click();
        $this->administrativeNodeViewPage->checkArrival();

        // Go the configuration form again.
        $this->configurePage->go();
        $this->configurePage->checkArrival();

        // Enable the paddle_i18n module.
        $this->appService->enableApp(new Multilingual);
        MultilingualService::setPaddleTestDefaults($this);
        $this->configurePage->go();
        $this->configurePage->checkArrival();

        $current_language = '';
        $default_language = '';
        $multilingual_enabled = paddle_core_is_multilingual();

        if ($multilingual_enabled) {
            global $language_content;
            $current_language = $language_content->language;
            $default_language = language_default('language');
        }

        $fields = array(
          'hoursPerWeek' => 'field_paddle_ep_hours_per_week',
          'studyArea' => 'field_paddle_ep_study_area',
          'grade' => 'field_paddle_ep_grade',
          'whatWillYouLearn' => 'field_paddle_ep_what_will_learn',
          'entryRequirements' => 'field_paddle_ep_entry_requirment',
          'whatCanIBecome' => 'field_paddle_ep_what_can_i_becom',
          'whereFollowEducation' => 'field_paddle_ep_where_follow_edu',
          'moreInfo' => 'field_paddle_ep_more_info',
          'links' => 'field_paddle_ep_links',
        );

        // Check if the values still exist in the configuration form after enabling paddle_i18n
        foreach ($fields as $key => $field) {
            $value = $multilingual_enabled ? i18n_variable_get('label_' . $field, $current_language, i18n_variable_get('label_' . $field, $default_language, '')) : variable_get('label_' . $field, '');
            $this->assertEquals($value, $this->configurePage->form->$key->getContent());
        }

        // Click on another language and fill the values for that language
        $en_ele = $this->byXPath('//ul[@class="language-switcher-locale-url"]//li[@class="en"]//a');
        $en_ele->click();

        // Clear the fields to fill it with new values for this language.
        $this->configurePage->form->hoursPerWeek->clear();
        $this->configurePage->form->studyArea->clear();
        $this->configurePage->form->grade->clear();
        $this->configurePage->form->whatWillYouLearn->clear();
        $this->configurePage->form->entryRequirements->clear();
        $this->configurePage->form->whatCanIBecome->clear();
        $this->configurePage->form->whereFollowEducation->clear();
        $this->configurePage->form->moreInfo->clear();
        $this->configurePage->form->links->clear();

        $hoursPerWeek = $this->alphanumericTestDataProvider->getValidValue();
        $studyArea = $this->alphanumericTestDataProvider->getValidValue();
        $grade = $this->alphanumericTestDataProvider->getValidValue();
        $whatWillYouLearn = $this->alphanumericTestDataProvider->getValidValue();
        $entryRequirements = $this->alphanumericTestDataProvider->getValidValue();
        $whatCanIBecome = $this->alphanumericTestDataProvider->getValidValue();
        $whereFollowEducation = $this->alphanumericTestDataProvider->getValidValue();
        $moreInfo = $this->alphanumericTestDataProvider->getValidValue();
        $links = $this->alphanumericTestDataProvider->getValidValue();

        // Fill all the fields in the configuration form and save
        $this->configurePage->form->hoursPerWeek->fill($hoursPerWeek);
        $this->configurePage->form->studyArea->fill($studyArea);
        $this->configurePage->form->grade->fill($grade);
        $this->configurePage->form->whatWillYouLearn->fill($whatWillYouLearn);
        $this->configurePage->form->entryRequirements->fill($entryRequirements);
        $this->configurePage->form->whatCanIBecome->fill($whatCanIBecome);
        $this->configurePage->form->whereFollowEducation->fill($whereFollowEducation);
        $this->configurePage->form->moreInfo->fill($moreInfo);
        $this->configurePage->form->links->fill($links);
        $current_language = $this->configurePage->form->languageSwitcher->getActiveLanguage();

        $this->configurePage->contextualToolbar->buttonSave->click();
        $this->waitUntilTextIsPresent('Your configurations have been saved.');

        // Check if correct labels are present in backend.
        $this->editPage->go($nid);
        $hoursPerWeek_label = $this->byXPath('//div[@id="field-paddle-ep-hours-per-week-add-more-wrapper"]//div//label')->text();
        $this->assertEquals($hoursPerWeek_label, $hoursPerWeek, '', 0.0, 10, false, true);

        $studyArea_label = $this->byXPath('//div[@id="edit-field-paddle-ep-study-area"]//div//label')->text();
        $this->assertEquals($studyArea_label, $studyArea, '', 0.0, 10, false, true);

        $grade_label = $this->byXPath('//div[@id="edit-field-paddle-ep-grade"]//div//label')->text();
        $this->assertEquals($grade_label, $grade, '', 0.0, 10, false, true);

        $whatWillYouLearn_label = $this->byXPath('//div[@id="field-paddle-ep-what-will-learn-add-more-wrapper"]//div//div//label')->text();
        $this->assertEquals($whatWillYouLearn_label, $whatWillYouLearn, '', 0.0, 10, false, true);

        $entryRequirements_label = $this->byXPath('//div[@id="field-paddle-ep-entry-requirment-add-more-wrapper"]//div//div//label')->text();
        $this->assertEquals($entryRequirements_label, $entryRequirements, '', 0.0, 10, false, true);

        $whatCanIBecome_label = $this->byXPath('//div[@id="field-paddle-ep-what-can-i-becom-add-more-wrapper"]//div/div/label')->text();
        $this->assertEquals($whatCanIBecome_label, $whatCanIBecome, '', 0.0, 10, false, true);

        $whereFollowEducation_label = $this->byXPath('//table[@id="field-paddle-ep-where-follow-edu-values"]//thead//tr//th//label')->text();
        $this->assertEquals($whereFollowEducation_label, $whereFollowEducation, '', 0.0, 10, false, true);

        $moreInfo_label = $this->byXPath('//div[@id="field-paddle-ep-more-info-add-more-wrapper"]//div//div//label')->text();
        $this->assertEquals($moreInfo_label, $moreInfo, '', 0.0, 10, false, true);

        $links_label = $this->byXPath('//div[@id="field-paddle-ep-links-add-more-wrapper"]//div//div//label')->text();
        $this->assertEquals($links_label, $links, '', 0.0, 10, false, true);

        // Create two organizational units content
        $organization_unit_1 = $this->alphanumericTestDataProvider->getValidValue();
        $organization_unit_2 = $this->alphanumericTestDataProvider->getValidValue();
        $org_nid_1 = $this->contentCreationService->createOrganizationalUnit($organization_unit_1);
        $this->administrativeNodeViewPage->go($org_nid_1);
        $this->administrativeNodeViewPage->contextualToolbar->buttonPublish->click();

        $org_nid_2 = $this->contentCreationService->createOrganizationalUnit($organization_unit_2);
        $this->administrativeNodeViewPage->go($org_nid_2);
        $this->administrativeNodeViewPage->contextualToolbar->buttonPublish->click();

        // Getting the vid of the study area taxonomy vocabulary and create a study area term
        $vid = db_query("SELECT vid FROM {taxonomy_vocabulary} WHERE machine_name = :machine_name", array(':machine_name' => 'study_area'))->fetchField();
        $study_area = $this->alphanumericTestDataProvider->getValidValue();
        $study_tid = $this->taxonomyService->createTerm($vid, $study_area);

        // Getting the vid of the grade taxonomy vocabulary and create a study area term
        $vid = db_query("SELECT vid FROM {taxonomy_vocabulary} WHERE machine_name = :machine_name", array(':machine_name' => 'grade'))->fetchField();
        $grade_term = $this->alphanumericTestDataProvider->getValidValue();
        $grade_tid = $this->taxonomyService->createTerm($vid, $grade_term);

        $this->editPage->go($nid);

        $this->editPage->educationPageEditForm->studyArea->selectTerm($study_tid);
        $this->editPage->educationPageEditForm->grade->selectTerm($grade_tid);

        $hours_per_week = $this->alphanumericTestDataProvider->getValidValue();
        $what_will_learn = $this->alphanumericTestDataProvider->getValidValue();
        $entry_requirements = $this->alphanumericTestDataProvider->getValidValue();
        $what_can_i_become = $this->alphanumericTestDataProvider->getValidValue();
        $more_info = $this->alphanumericTestDataProvider->getValidValue();
        $links_value = $this->alphanumericTestDataProvider->getValidValue();
        $url = 'http://' . $this->alphanumericTestDataProvider->getValidValue() . '.com';
        $spotify = 'https://open.spotify.com/track/2dpaYNEQHiRxtZbfNsse99';

        // Set organizational unit.
        $this->addTwoOrganizationalUnits($organization_unit_1, $organization_unit_2);

        $this->editPage->educationPageEditForm->hoursPerWeek->fill($hours_per_week);
        $this->editPage->educationPageEditForm->whatWillYouLearn->setBodyText($what_will_learn);
        $this->editPage->educationPageEditForm->entryRequirements->setBodyText($entry_requirements);
        $this->editPage->educationPageEditForm->whatCanIBecome->setBodyText($what_can_i_become);
        $this->editPage->educationPageEditForm->moreInfo->setBodyText($more_info);
        $this->editPage->educationPageEditForm->links->setBodyText($links_value);
        $this->editPage->educationPageEditForm->moreInfoLink->fill($url);
        $this->editPage->educationPageEditForm->spotify->fill($spotify);

        $this->editPage->contextualToolbar->buttonSave->click();
        $this->administrativeNodeViewPage->checkArrival();

        $this->administrativeNodeViewPage->contextualToolbar->buttonPublish->click();

        // Check if correct labels are present on frontend.
        $this->frontendPage->go($nid);
        $fields = array(
          'hours_per_week' => 'field_paddle_ep_hours_per_week',
          'study_area' => 'field_paddle_ep_study_area',
          'grade' => 'field_paddle_ep_grade',
          'what_will_you_learn' => 'field_paddle_ep_what_will_learn',
          'entry_requirements' => 'field_paddle_ep_entry_requirment',
          'what_can_i_become' => 'field_paddle_ep_what_can_i_becom',
          'organizational_units' => 'field_paddle_ep_where_follow_edu',
          'more_info' => 'field_paddle_ep_more_info',
          'links' => 'field_paddle_ep_links',
        );

        foreach ($fields as $key => $field) {
            $label = $this->byXPath('//div[@id="' . $key .'"]//h2')->text();
            $value = $multilingual_enabled ? i18n_variable_get('label_' . $field, $current_language, i18n_variable_get('label_' . $field, $default_language, '')) : variable_get('label_' . $field, '');
            $this->assertEquals($label, $value, '', 0.0, 10, false, true);
        }
    }

    /**
     * Adds 2 organizational units to the edit page.
     *
     * @param string $organization_unit_1
     *   The name of the first organizational_unit.
     * @param $organization_unit_2
     *   The name of the second organizational_unit.
     */
    protected function addTwoOrganizationalUnits($organization_unit_1, $organization_unit_2)
    {
        $rows = $this->editPage->educationPageEditForm->whereFollowEducationTable->rows;
        /** @var WhereFollowEducationTableRow $row */
        $row = reset($rows);
        $row->name->fill($organization_unit_1);
        $autocomplete = new AutoComplete($this);
        $autocomplete->waitUntilDisplayed();
        $suggestions = $autocomplete->getSuggestions();
        $autocomplete->pickSuggestionByValue($suggestions[0]);

        $count_rows = $this->editPage->educationPageEditForm->whereFollowEducationTable->getNumberOfRows();
        $this->editPage->educationPageEditForm->whereFollowEducationTable->addAnotherItem->click();

        // We need to wait until the new row with the fields appears.
        $table = $this->editPage->educationPageEditForm->whereFollowEducationTable;
        $callable = new SerializableClosure(
            function () use ($count_rows, $table) {
                if ($table->getNumberOfRows() == ($count_rows + 1)) {
                    return true;
                }
            }
        );
        $this->waitUntil($callable, $this->getTimeout());

        $rows = $this->editPage->educationPageEditForm->whereFollowEducationTable->rows;
        /** @var WhereFollowEducationTableRow $row */
        $row = end($rows);
        $row->name->fill($organization_unit_2);
        $autocomplete = new AutoComplete($this);
        $autocomplete->waitUntilDisplayed();
        $suggestions = $autocomplete->getSuggestions();
        $autocomplete->pickSuggestionByValue($suggestions[0]);
    }
}
