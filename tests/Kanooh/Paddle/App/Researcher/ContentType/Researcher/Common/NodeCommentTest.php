<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\Researcher\ContentType\Researcher\Common\NodeCommentTest.
 */

namespace Kanooh\Paddle\App\Researcher\ContentType\Researcher\Common;

use Kanooh\Paddle\App\Comment\ContentType\Base\NodeCommentTestBase;
use Kanooh\Paddle\Apps\Researcher;

/**
 * NodeCommentTest class.
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class NodeCommentTest extends NodeCommentTestBase
{

    /**
     * {@inheritdoc}
     */
    public function setupPage()
    {
        parent::setUpPage();

        $this->appService->enableApp(new Researcher);
    }

    /**
     * {@inheritdoc}
     */
    public function setupNode($first_name = null, $last_name = null)
    {
        return $this->contentCreationService->createResearcher($first_name, $last_name);
    }

    /**
     * {@inheritdoc}
     */
    public function getContentTypeName()
    {
        return 'researcher';
    }
}
