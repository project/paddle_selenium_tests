<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\Researcher\ContentType\Researcher\Common\SocialMediaTest.
 */

namespace Kanooh\Paddle\App\Researcher\ContentType\Researcher\Common;

use Kanooh\Paddle\App\SocialMedia\ContentType\Base\SocialMediaTestBase;
use Kanooh\Paddle\Apps\Researcher;

/**
 * SocialMediaTest class.
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class SocialMediaTest extends SocialMediaTestBase
{

    /**
     * {@inheritdoc}
     */
    public function setUpPage()
    {
        parent::setUpPage();

        $this->appService->enableApp(new Researcher);
    }


    /**
     * {@inheritdoc}
     */
    public function setupNode($first_name = null, $last_name = null)
    {
        return $this->contentCreationService->createResearcher($first_name, $last_name);
    }
}
