<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\Researcher\ContentType\Researcher\Common\MenuStructurePaneTest.
 */

namespace Kanooh\Paddle\App\Researcher\ContentType\Researcher\Common;

use Kanooh\Paddle\Apps\Researcher;
use Kanooh\Paddle\Core\ContentType\Base\MenuStructurePaneTestBase;
use Kanooh\Paddle\Pages\Admin\ContentManager\Node\LayoutPage\ResearcherLayoutPage;
use Kanooh\Paddle\Utilities\AppService;

/**
 * MenuStructurePaneTest class.
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class MenuStructurePaneTest extends MenuStructurePaneTestBase
{

    /**
     * {@inheritdoc}
     */
    public function setupPage()
    {
        parent::setUpPage();

        $service = new AppService($this, $this->userSessionService);
        $service->enableApp(new Researcher);
    }

    /**
     * {@inheritdoc}
     */
    public function setupNode($first_name = null, $last_name = null)
    {
        return $this->contentCreationService->createResearcher($first_name, $last_name);
    }

    /**
     * {@inheritdoc}
     */
    public function getLayoutPage()
    {
        return new ResearcherLayoutPage($this);
    }
}
