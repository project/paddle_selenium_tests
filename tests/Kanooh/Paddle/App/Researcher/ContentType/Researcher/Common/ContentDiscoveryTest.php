<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\Researcher\ContentType\Researcher\Common\ContentDiscoveryTest.
 */

namespace Kanooh\Paddle\App\Researcher\ContentType\Researcher\Common;

use Kanooh\Paddle\Apps\Researcher;
use Kanooh\Paddle\Core\ContentType\Base\ContentDiscoveryTestBase;
use Kanooh\Paddle\Utilities\AppService;

/**
 * ContentDiscoveryTest class.
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class ContentDiscoveryTest extends ContentDiscoveryTestBase
{

    /**
     * @var AppService
     */
    protected $appService;

    /**
     * {@inheritdoc}
     */
    public function setUpPage()
    {
        parent::setUpPage();

        $this->appService = new AppService($this, $this->userSessionService);
        $this->appService->enableApp(new Researcher);
    }

    /**
     * {@inheritdoc}
     */
    public function setupNode($first_name = null, $last_name = null)
    {
        return $this->contentCreationService->createResearcher($first_name, $last_name);
    }
}
