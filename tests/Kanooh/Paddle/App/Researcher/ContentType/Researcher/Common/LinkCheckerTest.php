<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\Researcher\ContentType\Researcher\Common\LinkCheckerTest.
 */

namespace Kanooh\Paddle\App\Researcher\ContentType\Researcher\Common;

use Kanooh\Paddle\Apps\Researcher;
use Kanooh\Paddle\Core\ContentType\Base\LinkCheckerTestBase;
use Kanooh\Paddle\Utilities\AppService;
use Kanooh\Paddle\Pages\Admin\ContentManager\Node\LayoutPage\ResearcherLayoutPage;

/**
 * LinkCheckerTest class.
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class LinkCheckerTest extends LinkCheckerTestBase
{

    /**
     * @var AppService
     */
    protected $appService;

    /**
     * @var ResearcherLayoutPage
     */
    protected $layoutPage;

    /**
     * {@inheritdoc}
     */
    public function setUpPage()
    {
        parent::setUpPage();

        $this->appService = new AppService($this, $this->userSessionService);
        $this->appService->enableApp(new Researcher);
        $this->layoutPage = new ResearcherLayoutPage($this);
    }

    /**
     * {@inheritdoc}
     */
    public function setupNode($first_name = null, $last_name = null)
    {
        return $this->contentCreationService->createResearcher($first_name, $last_name);
    }

    /**
     * {@inheritdoc}
     */
    public function testReferenceWarningWhenBulkArchiving()
    {
        // Create a few nodes to be referenced.
        $referenced_nodes = array();
        for ($i = 0; $i < 3; $i++) {
            $first_name = $this->alphanumericTestDataProvider->getValidValue();
            $last_name = $this->alphanumericTestDataProvider->getValidValue();
            $title = $first_name . ' ' . $last_name;
            $referenced_nodes[$title] = $this->setupNode($first_name, $last_name);
        }
        // Create a non-referenced node which will hold the references.
        $referencing_nid = $this->setupNode();

        // Add references to the referenced nodes.
        $this->editPage->go($referencing_nid);
        $this->editPage->body->waitUntilReady();
        $this->editPage->body->buttonSource->click();
        $text = '';
        foreach ($referenced_nodes as $title => $nid) {
            $text .= l($title, "node/$nid", array('alias' => true));
        }
        $this->editPage->body->setBodyText($text);
        $this->editPage->contextualToolbar->buttonSave->click();
        $this->administrativeNodeViewPage->checkArrival();

        // Try to bulk archive all of the nodes.
        $this->contentManagerPage->go();
        foreach ($referenced_nodes as $title => $nid) {
            $row = $this->contentManagerPage->contentTable->getNodeRowByNid($nid);
            $row->bulkActionCheckbox->check();
        }
        $this->contentManagerPage->bulkActions->selectAction->selectOptionByLabel('Set moderation state');
        $this->contentManagerPage->bulkActions->selectState->selectOptionByLabel('Archived');
        $this->contentManagerPage->bulkActions->executeButton->click();
        $this->contentManagerPage->checkArrival();

        // Check that there is a warning message is it contains the correct links.
        $warning_message = 'The following pages are used by other pages. See the usage overview below. '
          . 'Are you sure you want to break the links on those pages?';
        $this->assertTextPresent($warning_message);
        $this->assertCount(3, $this->contentManagerPage->referencedNodesWarningLinks);
        foreach ($this->contentManagerPage->referencedNodesWarningLinks as $link) {
            watchdog('nodes', '<pre>' . print_r($referenced_nodes, true) . '</pre>');
            watchdog('linktext', $link->text());
            $nid = $referenced_nodes[$link->text()];
            $this->assertNotNull($nid);
            $url = url("node/$nid/references", array('absolute' => true));
            $this->assertEquals($url, $link->attribute('href'));
        }

        // Archive all the referenced nodes.
        $this->contentManagerPage->bulkActions->buttonConfirm->click();
        $this->contentManagerPage->checkArrival();

        // Try archiving the non-referenced node and make sure there is no
        // warning message.
        $row = $this->contentManagerPage->contentTable->getNodeRowByNid($referencing_nid);
        $row->bulkActionCheckbox->check();
        $this->contentManagerPage->bulkActions->selectAction->selectOptionByLabel('Set moderation state');
        $this->contentManagerPage->bulkActions->selectState->selectOptionByLabel('Archived');
        $this->contentManagerPage->bulkActions->executeButton->click();
        $this->contentManagerPage->checkArrival();
        $this->assertTextNotPresent($warning_message);
        $this->assertEmpty($this->contentManagerPage->referencedNodesWarningLinks);
    }
}
