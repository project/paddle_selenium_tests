<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\Researcher\ContentType\Researcher\Common\NodeCreationLanguageTest.
 */

namespace Kanooh\Paddle\App\Researcher\ContentType\Researcher\Common;

use Kanooh\Paddle\Apps\Researcher;
use Kanooh\Paddle\App\Multilingual\ContentType\Base\NodeCreationLanguageTestBase;
use Kanooh\Paddle\Pages\Admin\ContentManager\AddPage\CreateResearcherModal;

/**
 * Class NodeDeleteButtonTest
 * @package Kanooh\Paddle\App\Researcher\ContentType\Researcher\Common
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class NodeCreationLanguageTest extends NodeCreationLanguageTestBase
{
    /**
     * {@inheritdoc}
     */
    public function setUpPage()
    {
        parent::setUpPage();

        $this->appService->enableApp(new Researcher);
    }

    /**
     * {@inheritdoc}
     */
    public function getModalClassName()
    {
        return '\Kanooh\Paddle\Pages\Admin\ContentManager\AddPage\CreateResearcherModal';
    }

    /**
     * {@inheritdoc}
     */
    public function getContentTypeName()
    {
        return 'researcher';
    }

    /**
     * {@inheritdoc}
     */
    public function fillInAddModalForm($modal)
    {
        /** @var CreateResearcherModal $modal */
        $modal->firstName->fill($this->alphanumericTestDataProvider->getValidValue());
        $modal->lastName->fill($this->alphanumericTestDataProvider->getValidValue());
    }
}
