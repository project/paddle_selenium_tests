<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\Researcher\ContentType\Researcher\Common\NodeDetailTest.
 */

namespace Kanooh\Paddle\App\Researcher\ContentType\Researcher\Common;

use Kanooh\Paddle\Core\ContentType\Base\NodeDetailTestBase;
use Kanooh\Paddle\Utilities\ContentCreationService;

/**
 * NodeDetailTest class.
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class NodeDetailTest extends NodeDetailTestBase
{

    /**
     * {@inheritdoc}
     */
    public function setupNode($first_name = null, $last_name = null)
    {
        return $this->contentCreationService->createResearcher($first_name, $last_name);
    }
}
