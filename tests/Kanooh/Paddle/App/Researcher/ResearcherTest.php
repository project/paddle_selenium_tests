<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\Researcher\ResearcherTest.
 */

namespace Kanooh\Paddle\App\Researcher;

use Jeremeamia\SuperClosure\SerializableClosure;
use Kanooh\Paddle\Apps\Researcher;
use Kanooh\Paddle\Pages\Admin\ContentManager\Node\LayoutPage\ResearcherLayoutPage;
use Kanooh\Paddle\Pages\Admin\ContentManager\Node\ViewPage\ViewPage as AdministrativeNodeViewPage;
use Kanooh\Paddle\Pages\Node\ViewPage\Researcher\ResearcherViewPage;
use Kanooh\Paddle\Pages\Node\EditPage\Researcher\ResearcherEditPage;
use Kanooh\Paddle\Utilities\AppService;
use Kanooh\Paddle\Utilities\AssetCreationService;
use Kanooh\Paddle\Utilities\ContentCreationService;
use Kanooh\Paddle\Utilities\ScaldService;
use Kanooh\Paddle\Utilities\UserSessionService;
use Kanooh\TestDataProvider\AlphanumericTestDataProvider;
use Kanooh\TestDataProvider\UrlTestDataProvider;
use Kanooh\TestDataProvider\EmailTestDataProvider;
use Kanooh\WebDriver\WebDriverTestCase;
use Kanooh\Paddle\Utilities\TaxonomyService;
use Kanooh\Paddle\Pages\Element\Researcher\ExtraLinksTableRow;

/**
 * Class ResearcherPageTest
 *
 * @package Kanooh\Paddle\App\Researcher
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class ResearcherTest extends WebDriverTestCase
{

    /**
     * @var AdministrativeNodeViewPage
     */
    protected $administrativeNodeViewPage;

    /**
     * @var AlphanumericTestDataProvider
     */
    protected $alphanumericTestDataProvider;

    /**
     * @var UrlTestDataProvider
     */
    protected $urlTestDataProvider;

    /**
     * @var EmailTestDataProvider
     */
    protected $emailTestDataProvider;

    /**
     * @var AppService
     */
    protected $appService;

    /**
     * @var AssetCreationService
     */
    protected $assetCreationService;

    /**
     * @var ContentCreationService
     */
    protected $contentCreationService;

    /**
     * @var ResearcherEditPage
     */
    protected $editPage;

    /**
     * @var ResearcherViewPage
     */
    protected $frontendPage;

    /**
     * @var ResearcherLayoutPage
     */
    protected $layoutPage;

    /**
     * @var ScaldService
     */
    protected $scaldService;

    /**
     * @var TaxonomyService
     */
    protected $taxonomyService;

    /**
     * @var UserSessionService
     */
    protected $userSessionService;

    /**
     * {@inheritdoc}
     */
    public function setUpPage()
    {
        parent::setUpPage();

        // Create some instances to use later on.
        $this->administrativeNodeViewPage = new AdministrativeNodeViewPage($this);
        $this->alphanumericTestDataProvider = new AlphanumericTestDataProvider();
        $this->urlTestDataProvider = new UrlTestDataProvider();
        $this->emailTestDataProvider = new EmailTestDataProvider();
        $this->assetCreationService = new AssetCreationService($this);
        $this->editPage = new ResearcherEditPage($this);
        $this->scaldService = new ScaldService($this);
        $this->frontendPage = new ResearcherViewPage($this);
        $this->taxonomyService = new TaxonomyService();
        $this->layoutPage = new ResearcherLayoutPage($this);

        // Go to the login page and log in as Chief Editor.
        $this->userSessionService = new UserSessionService($this);
        $this->contentCreationService = new ContentCreationService($this, $this->userSessionService);
        $this->userSessionService->login('ChiefEditor');

        // Enable the app if it is not yet enabled.
        $this->appService = new AppService($this, $this->userSessionService);
        $this->appService->enableApp(new Researcher);
    }

    /**
     * Tests the node edit of the Researcher.
     */
    public function testNodeEdit()
    {
        $first_name = $this->alphanumericTestDataProvider->getValidValue();
        $last_name = $this->alphanumericTestDataProvider->getValidValue();

        // Getting the vid of the staff type taxonomy vocabulary and create a staff type term.
        $vid = db_query("SELECT vid FROM {taxonomy_vocabulary} WHERE machine_name = :machine_name", array(':machine_name' => 'staff_type'))->fetchField();
        $staff_type = $this->alphanumericTestDataProvider->getValidValue();
        $staff_type_tid = $this->taxonomyService->createTerm($vid, $staff_type);

        $position = $this->alphanumericTestDataProvider->getValidValue();
        $email = $this->emailTestDataProvider->getValidValue();
        $telephone = '123456789';
        $linkedin = $this->urlTestDataProvider->getValidValue();
        $cris_link = $this->urlTestDataProvider->getValidValue();
        $projects_link = $this->urlTestDataProvider->getValidValue();
        $publications_link = $this->urlTestDataProvider->getValidValue();

        $body = $this->alphanumericTestDataProvider->getValidValue();
        $campus = $this->alphanumericTestDataProvider->getValidValue();
        $office = $this->alphanumericTestDataProvider->getValidValue();
        $address = $this->alphanumericTestDataProvider->getValidValue();
        $postcode = $this->alphanumericTestDataProvider->getValidValue();
        $city = $this->alphanumericTestDataProvider->getValidValue();

        // Create a image atom to test with.
        $atom_1 = $this->assetCreationService->createImage();
        $atom_2 = $this->assetCreationService->createImage();

        // Create a researcher and fill all the fields.
        $nid = $this->contentCreationService->createResearcher($first_name, $last_name);

        $this->editPage->go($nid);

        $this->editPage->researcherEditForm->position->fill($position);
        $this->editPage->researcherEditForm->email->fill($email);
        $this->editPage->researcherEditForm->telephone->fill($telephone);

        $this->editPage->researcherEditForm->linkedin->fill($linkedin);
        $this->editPage->researcherEditForm->crisLink->fill($cris_link);
        $this->editPage->researcherEditForm->projectsLink->fill($projects_link);
        $this->editPage->researcherEditForm->publicationsLink->fill($publications_link);
        $this->editPage->researcherEditForm->campus->fill($campus);
        $this->editPage->researcherEditForm->address->fill($address);
        $this->editPage->researcherEditForm->office->fill($office);
        $this->editPage->researcherEditForm->postCode->fill($postcode);
        $this->editPage->researcherEditForm->city->fill($city);

        $this->editPage->researcherEditForm->photo1->selectAtom($atom_1['id']);
        $this->editPage->researcherEditForm->photo2->selectAtom($atom_2['id']);

        // Select the staff type term on researcher edit form.
        $this->editPage->researcherEditForm->staffType->selectTerm($staff_type_tid);

        $this->editPage->researcherEditForm->body->setBodyText($body);


        // Fill the extra links field.
        $row_1_url = $this->urlTestDataProvider->getValidValue();
        $row_2_url = $this->urlTestDataProvider->getValidValue();
        $row_1_title = $this->alphanumericTestDataProvider->getValidValue();
        $row_2_title = $this->alphanumericTestDataProvider->getValidValue();

        $rows = $this->editPage->researcherEditForm->extraLinks->rows;
        /** @var ExtraLinksTableRow $row */
        $row = reset($rows);
        $row->title->fill($row_1_title);
        $row->url->fill($row_1_url);

        $count_rows = $this->editPage->researcherEditForm->extraLinks->getNumberOfRows();
        $this->editPage->researcherEditForm->extraLinks->addAnotherItem->click();

        // We need to wait until the new row with the fields appears.
        $table = $this->editPage->researcherEditForm->extraLinks;
        $callable = new SerializableClosure(
            function () use ($count_rows, $table) {
                if ($table->getNumberOfRows() == ($count_rows + 1)) {
                    return true;
                }
            }
        );
        $this->waitUntil($callable, $this->getTimeout());

        $rows = $this->editPage->researcherEditForm->extraLinks->rows;
        /** @var ExtraLinksTableRow $row */
        $row = end($rows);
        $row->title->fill($row_2_title);
        $row->url->fill($row_2_url);

        // Save the node.
        $this->editPage->contextualToolbar->buttonSave->click();
        $this->administrativeNodeViewPage->checkArrival();

        // Go to researcher edit page and check if all fields are filled
        $this->editPage->go($nid);

        $rows = $this->editPage->researcherEditForm->extraLinks->rows;
        $this->assertEquals($row_1_title, $rows[0]->title->getContent());
        $this->assertEquals($row_1_url, $rows[0]->url->getContent());
        $this->assertEquals($row_2_title, $rows[1]->title->getContent());
        $this->assertEquals($row_2_url, $rows[1]->url->getContent());

        $this->assertContains($body, $this->editPage->researcherEditForm->body->getBodyText());
        $this->assertEquals($first_name, $this->editPage->researcherEditForm->firstName->getContent());
        $this->assertEquals($last_name, $this->editPage->researcherEditForm->lastName->getContent());
        $this->assertEquals($position, $this->editPage->researcherEditForm->position->getContent());
        $this->assertEquals($email, $this->editPage->researcherEditForm->email->getContent());
        $this->assertEquals($telephone, $this->editPage->researcherEditForm->telephone->getContent());
        $this->assertEquals($linkedin, $this->editPage->researcherEditForm->linkedin->getContent());
        $this->assertEquals($cris_link, $this->editPage->researcherEditForm->crisLink->getContent());
        $this->assertEquals($projects_link, $this->editPage->researcherEditForm->projectsLink->getContent());
        $this->assertEquals($publications_link, $this->editPage->researcherEditForm->publicationsLink->getContent());
        $this->assertEquals($campus, $this->editPage->researcherEditForm->campus->getContent());
        $this->assertEquals($office, $this->editPage->researcherEditForm->office->getContent());
        $this->assertEquals($address, $this->editPage->researcherEditForm->address->getContent());
        $this->assertEquals($postcode, $this->editPage->researcherEditForm->postCode->getContent());
        $this->assertEquals($city, $this->editPage->researcherEditForm->city->getContent());

        $term = $this->editPage->researcherEditForm->staffType->getTermById($staff_type_tid);
        $this->assertTrue($term->selected());

        $atoms = $this->editPage->researcherEditForm->photo1->atoms;
        $this->assertNotNull($atoms[0]->title);

        $atoms = $this->editPage->researcherEditForm->photo2->atoms;
        $this->assertNotNull($atoms[0]->title);
    }

    /**
     * Tests the node page layout of the researcher.
     */
    public function testPageLayout()
    {
        $first_name = $this->alphanumericTestDataProvider->getValidValue();
        $last_name = $this->alphanumericTestDataProvider->getValidValue();

        // Getting the vid of the staff type taxonomy vocabulary and create a staff type term.
        $vid = db_query("SELECT vid FROM {taxonomy_vocabulary} WHERE machine_name = :machine_name", array(':machine_name' => 'staff_type'))->fetchField();
        $staff_type = $this->alphanumericTestDataProvider->getValidValue();
        $staff_type_tid = $this->taxonomyService->createTerm($vid, $staff_type);

        $position = $this->alphanumericTestDataProvider->getValidValue();
        $email = $this->emailTestDataProvider->getValidValue();
        $telephone = '123456789';
        $linkedin = $this->urlTestDataProvider->getValidValue();
        $cris_link = $this->urlTestDataProvider->getValidValue();
        $projects_link = $this->urlTestDataProvider->getValidValue();
        $publications_link = $this->urlTestDataProvider->getValidValue();

        $body = $this->alphanumericTestDataProvider->getValidValue();
        $campus = $this->alphanumericTestDataProvider->getValidValue();
        $office = $this->alphanumericTestDataProvider->getValidValue();
        $address = $this->alphanumericTestDataProvider->getValidValue();
        $postcode = $this->alphanumericTestDataProvider->getValidValue();
        $city = $this->alphanumericTestDataProvider->getValidValue();

        // Create a image atom to test with.
        $atom_1 = $this->assetCreationService->createImage();

        // Create a researcher and fill all the fields.
        $nid = $this->contentCreationService->createResearcher($first_name, $last_name);

        $this->editPage->go($nid);

        $this->editPage->researcherEditForm->position->fill($position);
        $this->editPage->researcherEditForm->email->fill($email);
        $this->editPage->researcherEditForm->telephone->fill($telephone);

        $this->editPage->researcherEditForm->linkedin->fill($linkedin);
        $this->editPage->researcherEditForm->crisLink->fill($cris_link);
        $this->editPage->researcherEditForm->projectsLink->fill($projects_link);
        $this->editPage->researcherEditForm->publicationsLink->fill($publications_link);
        $this->editPage->researcherEditForm->campus->fill($campus);
        $this->editPage->researcherEditForm->address->fill($address);
        $this->editPage->researcherEditForm->office->fill($office);
        $this->editPage->researcherEditForm->postCode->fill($postcode);
        $this->editPage->researcherEditForm->city->fill($city);

        $this->editPage->researcherEditForm->photo1->selectAtom($atom_1['id']);

        // Select the staff type term on researcher edit form.
        $this->editPage->researcherEditForm->staffType->selectTerm($staff_type_tid);

        $this->editPage->researcherEditForm->body->setBodyText($body);


        // Fill the extra links field.
        $row_1_url = $this->urlTestDataProvider->getValidValue();
        $row_1_title = $this->alphanumericTestDataProvider->getValidValue();

        $rows = $this->editPage->researcherEditForm->extraLinks->rows;
        /** @var ExtraLinksTableRow $row */
        $row = reset($rows);
        $row->title->fill($row_1_title);
        $row->url->fill($row_1_url);

        // Save the node.
        $this->editPage->contextualToolbar->buttonSave->click();
        $this->administrativeNodeViewPage->checkArrival();

        // Go to the layout page and verify that every thing is in its correct region.
        $this->layoutPage->go($nid);

        // Verify the researcher info exist in region A.
        $region = $this->layoutPage->display->region('full_a');
        $panes = $region->getPanes();
        foreach ($panes as $pane) {
            $element = $pane->getWebdriverElement();
            $element_photo1 = $element->byXPath('//div[@class="photo"]//img')->attribute('class');
            $this->assertEquals('atom-id-' . $atom_1['id'], $element_photo1);

            $staff_type_term = taxonomy_term_load($staff_type_tid);
            $this->assertContains($staff_type_term->name, $element->text());

            $this->assertContains($first_name, $element->text());
            $this->assertContains($last_name, $element->text());
            $this->assertContains($position, $element->text());
            $this->assertContains($email, $element->text());
            $this->assertContains($telephone, $element->text());

            $linkedin_url = $element->byXPath('//div[@class="linkedin-link"]//a')->attribute('href');
            $this->assertContains(strtolower($linkedin), $linkedin_url);
            $cris_url = $element->byXPath('//div[@class="cris-link"]//a')->attribute('href');
            $this->assertContains(strtolower($cris_link), $cris_url);
            $projects_url = $element->byXPath('//div[@class="projects-link"]//a')->attribute('href');
            $this->assertContains(strtolower($projects_link), $projects_url);
            $publications_url = $element->byXPath('//div[@class="publication-link"]//a')->attribute('href');
            $this->assertContains(strtolower($publications_link), $publications_url);
            $extra_link = $element->byXPath('//div[@class="extra-links"]//a')->attribute('href');
            $this->assertContains(strtolower($row_1_url), $extra_link);
        }

        // Verify that the biography exists in region B.
        $region = $this->layoutPage->display->region('nested_9_b');
        $panes = $region->getPanes();
        foreach ($panes as $pane) {
            $element = $pane->getWebdriverElement();
            $this->assertContains($body, $element->text());
        }

        // Verify that the location field exists in region C.
        $region = $this->layoutPage->display->region('nested_3_c');
        $panes = $region->getPanes();
        foreach ($panes as $pane) {
            $element = $pane->getWebdriverElement();
            $this->assertContains($campus, $element->text());
            $this->assertContains($office, $element->text());
            $this->assertContains($address, $element->text());
            $this->assertContains($postcode, $element->text());
            $this->assertContains($city, $element->text());
        }
    }
}
