<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\PasswordPolicy\InfoTest.
 */

namespace Kanooh\Paddle\App\PasswordPolicy;

use Kanooh\Paddle\App\Base\InfoTestBase;
use Kanooh\Paddle\Apps\PasswordPolicy;

/**
 * Class InfoTest
 * @package Kanooh\Paddle\App\PasswordPolicy
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class InfoTest extends InfoTestBase
{
    /**
     * {@inheritdoc}
     */
    public function getApp()
    {
        return new PasswordPolicy;
    }
}
