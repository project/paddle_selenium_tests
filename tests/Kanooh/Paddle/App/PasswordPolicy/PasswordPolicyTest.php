<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\PasswordPolicy\PasswordPolicyTest.
 */

namespace Kanooh\Paddle\App\PasswordPolicy;

use Kanooh\Paddle\Apps\PasswordPolicy;
use Kanooh\Paddle\Pages\Admin\Apps\PaddleApps\PaddlePasswordPolicy\ConfigurePage\ConfigurePage;
use Kanooh\Paddle\Utilities\AppService;
use Kanooh\Paddle\Utilities\UserSessionService;
use Kanooh\Paddle\Pages\Admin\User\UserProfileEditPage;
use Kanooh\TestDataProvider\AlphanumericTestDataProvider;
use Kanooh\WebDriver\WebDriverTestCase;

/**
 * Class ConfigurationTest
 * @package Kanooh\Paddle\App\PasswordPolicy
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class PasswordPolicyTest extends WebDriverTestCase
{
    /**
     * @var AlphanumericTestDataProvider
     */
    protected $alphanumericTestDataProvider;

    /**
     * @var AppService
     */
    protected $appService;

    /**
     * @var ConfigurePage
     */
    protected $configurePage;

    /**
     * @var UserSessionService
     */
    protected $userSessionService;

    /**
     * @var UserProfileEditPage
     */
    protected $userProfileEditPage;

    /**
     * {@inheritdoc}
     */
    public function setUpPage()
    {
        parent::setUpPage();

        // Instantiate some classes to use in the test.
        $this->alphanumericTestDataProvider = new AlphanumericTestDataProvider();
        $this->configurePage = new ConfigurePage($this);
        $this->userSessionService = new UserSessionService($this);
        $this->userProfileEditPage = new UserProfileEditPage($this);
        // Log in as chief editor.
        $this->userSessionService->login('ChiefEditor');

        // Enable the app if it is not yet enabled.
        $this->appService = new AppService($this, $this->userSessionService);
        $this->appService->enableApp(new PasswordPolicy);
    }

    /**
     * {@inheritdoc}
     */
    public function tearDown()
    {
        // Log out.
        $this->userSessionService = new UserSessionService($this);
        $this->userSessionService->logout();

        parent::tearDown();
    }

    /**
     * Tests the Add functionality.
     */
    public function testPasswordPolicy()
    {
        $real_name = $this->alphanumericTestDataProvider->getValidValue();
        $original_password = "demo";
        $bad_password = "demodemo";
        $new_password = "Demodemo1";

        // Enable the password policy.
        $this->configurePage->go();
        $this->configurePage->form->useNormalPolicy->check();
        $this->configurePage->contextualToolbar->buttonSave->click();
        $this->waitUntilTextIsPresent('The configuration has been saved.');
        // Go to the user profile.
        $this->userProfileEditPage->go($this->userSessionService->getCurrentUserId());
        // Set a real name for the user.
        $this->userProfileEditPage->form->realName->clear();

        $this->userProfileEditPage->form->realName->fill($real_name);
        $this->userProfileEditPage->form->currentPassword->fill($original_password);
        // Check if it will fail with a simple password.
        $this->userProfileEditPage->form->newPassword->fill($bad_password);
        $this->userProfileEditPage->form->confirmNewPassword->fill($bad_password);
        $this->userProfileEditPage->contextualToolbar->buttonSave->click();
        $this->assertTextPresent('Your password has not met the following requirement(s)');
        // Now with password meeting at least 2 requirements.
        // Set a real name for the user.
        $this->userProfileEditPage->form->realName->clear();
        $this->userProfileEditPage->form->realName->fill($real_name);
        $this->userProfileEditPage->form->currentPassword->fill($original_password);
        $this->userProfileEditPage->form->newPassword->fill($new_password);
        $this->userProfileEditPage->form->confirmNewPassword->fill($new_password);
        $this->userProfileEditPage->contextualToolbar->buttonSave->click();

        $this->waitUntilTextIsPresent('The changes have been saved.');

        // disable password policy, we don't want to break the other tests.
        $this->configurePage->go();
        $this->configurePage->form->useNormalPolicy->uncheck();
        $this->configurePage->contextualToolbar->buttonSave->click();
        // Go to the user profile and restore password.
        $this->userProfileEditPage->go($this->userSessionService->getCurrentUserId());
        $this->userProfileEditPage->form->currentPassword->fill($new_password);
        $this->userProfileEditPage->form->newPassword->fill($original_password);
        $this->userProfileEditPage->form->confirmNewPassword->fill($original_password);
        $this->userProfileEditPage->contextualToolbar->buttonSave->click();

        $this->waitUntilTextIsPresent('The changes have been saved.');
    }
}
