<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\PasswordPolicy\EnableDisableTest.
 */

namespace Kanooh\Paddle\App\PasswordPolicy;

use Kanooh\Paddle\App\Base\EnableDisableTestBase;
use Kanooh\Paddle\Apps\PasswordPolicy;

/**
 * Class EnableDisableTest
 * @package Kanooh\Paddle\App\PasswordPolicy
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class EnableDisableTest extends EnableDisableTestBase
{
    /**
     * {@inheritdoc}
     */
    public function getApp()
    {
        return new PasswordPolicy();
    }
}
