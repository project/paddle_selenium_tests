<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\CoreStatisticsPage\ContentType\CoreStatisticsPage\Common\NodeCloneTest.
 */

namespace Kanooh\Paddle\App\CoreStatisticsPage\ContentType\CoreStatisticsPage\Common;

use Kanooh\Paddle\Apps\CoreStatisticsPage;
use Kanooh\Paddle\Core\ContentType\Base\NodeCloneTestBase;

/**
 * Class NodeCloneTest.
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class NodeCloneTest extends NodeCloneTestBase
{
    /**
     * {@inheritdoc}
     */
    public function setUpPage()
    {
        parent::setUpPage();

        $this->appService->enableApp(new CoreStatisticsPage);
    }

    /**
     * {@inheritdoc}
     */
    public function setupNode($title = null)
    {
        return $this->contentCreationService->createCoreStatisticsPage($title);
    }
}
