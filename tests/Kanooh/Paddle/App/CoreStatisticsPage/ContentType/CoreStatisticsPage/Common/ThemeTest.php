<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\CoreStatisticsPage\ContentType\CoreStatisticsPage\Common\ThemeTest.
 */

namespace Kanooh\Paddle\App\CoreStatisticsPage\ContentType\CoreStatisticsPage\Common;

use Kanooh\Paddle\Apps\CoreStatisticsPage;
use Kanooh\Paddle\Core\ContentType\Base\ThemeTestBase;

/**
 * ThemeTest class.
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class ThemeTest extends ThemeTestBase
{
    /**
     * {@inheritdoc}
     */

    public function setupPage()
    {
        parent::setUpPage();

        $this->appService->enableApp(new CoreStatisticsPage);
    }

    /**
     * {@inheritdoc}
     */
    public function setupNode($title = null)
    {
        return $this->contentCreationService->createCoreStatisticsPageViaUI($title);
    }
}
