<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\CoreStatisticsPage\ContentType\CoreStatisticsPage\Common\ReadOnlyRoleTest.
 */

namespace Kanooh\Paddle\App\CoreStatisticsPage\ContentType\CoreStatisticsPage\Common;

use Kanooh\Paddle\Apps\CoreStatisticsPage;
use Kanooh\Paddle\Core\ContentType\Base\ReadOnlyRoleTestBase;
use Kanooh\Paddle\Utilities\AppService;

/**
 * ReadOnlyRoleTest class.
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class ReadOnlyRoleTest extends ReadOnlyRoleTestBase
{
    /**
     * {@inheritdoc}
     */
    public function setupPage()
    {
        parent::setUpPage();

        $service = new AppService($this, $this->userSessionService);
        $service->enableApp(new CoreStatisticsPage);
    }

    /**
     * {@inheritdoc}
     */
    public function setupNode($title = null)
    {
        return $this->contentCreationService->createCoreStatisticsPage($title);
    }
}
