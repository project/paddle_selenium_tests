<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\CoreStatisticsPage\ContentType\CoreStatisticsPage\Common\NodeMenuItemListTest.
 */

namespace Kanooh\Paddle\App\CoreStatisticsPage\ContentType\CoreStatisticsPage\Common;

use Kanooh\Paddle\Apps\CoreStatisticsPage;
use Kanooh\Paddle\Core\ContentType\Base\NodeMenuItemListTestBase;
use Kanooh\Paddle\Utilities\AppService;

/**
 * NodeMenuItemListTest class.
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class NodeMenuItemListTest extends NodeMenuItemListTestBase
{
    /**
     * @var AppService
     */
    protected $appService;

    /**
     * {@inheritdoc}
     */
    public function setUpPage()
    {
        parent::setUpPage();

        $this->appService = new AppService($this, $this->userSessionService);
        $this->appService->enableApp(new CoreStatisticsPage);
    }

    /**
     * {@inheritdoc}
     */
    public function setupNode($title = null)
    {
        return $this->contentCreationService->createCoreStatisticsPage($title);
    }

    /**
     * {@inheritdoc}
     */
    public function getContentTypeName()
    {
        return 'core_statistics_page';
    }
}
