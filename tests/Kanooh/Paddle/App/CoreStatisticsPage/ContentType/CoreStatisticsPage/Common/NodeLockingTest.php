<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\CoreStatisticsPage\ContentType\CoreStatisticsPage\Common\NodeLockingTest.
 */

namespace Kanooh\Paddle\App\CoreStatisticsPage\ContentType\CoreStatisticsPage\Common;

use Kanooh\Paddle\Apps\CoreStatisticsPage;
use Kanooh\Paddle\Core\ContentType\Base\NodeLockingTestBase;
use Kanooh\Paddle\Utilities\AppService;

/**
 * NodeLockingTest class.
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class NodeLockingTest extends NodeLockingTestBase
{
    /**
     * {@inheritdoc}
     */
    public function setupPage()
    {
        parent::setUpPage();

        $service = new AppService($this, $this->userSessionService);
        $service->enableApp(new CoreStatisticsPage);
    }

    /**
     * {@inheritdoc}
     */
    public function setupNode($title = null)
    {
        return $this->contentCreationService->createCoreStatisticsPage($title);
    }
}
