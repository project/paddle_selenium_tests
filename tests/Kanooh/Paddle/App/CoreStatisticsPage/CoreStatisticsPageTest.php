<?php

/**
 * @file
 * Contains \Kanooh\Paddle\App\CoreStatisticsPage\CoreStatisticsPageTest.
 */

namespace Kanooh\Paddle\App\CoreStatisticsPage;

use Kanooh\Paddle\Apps\CoreStatisticsPage;
use Jeremeamia\SuperClosure\SerializableClosure;
use Kanooh\Paddle\Pages\Admin\ContentManager\Node\LayoutPage\LayoutPage;
use Kanooh\Paddle\Pages\Admin\ContentManager\Node\ViewPage\ViewPage as AdministrativeNodeViewPage;
use Kanooh\Paddle\Pages\Node\EditPage\CoreStatisticsPage\CoreStatisticsPageEditPage;
use Kanooh\Paddle\Pages\Node\ViewPage\CoreStatisticsPage\CoreStatisticsPageViewPage;
use Kanooh\Paddle\Pages\Node\EditPage\CoreStatisticsPage\ParagraphsTableRow;
use Kanooh\Paddle\Utilities\AppService;
use Kanooh\Paddle\Utilities\AssetCreationService;
use Kanooh\Paddle\Utilities\ContentCreationService;
use Kanooh\Paddle\Utilities\ScaldService;
use Kanooh\Paddle\Utilities\UserSessionService;
use Kanooh\TestDataProvider\AlphanumericTestDataProvider;
use Kanooh\WebDriver\WebDriverTestCase;
use Kanooh\Paddle\Pages\Admin\ContentManager\Node\LayoutPage\CoreStatisticsPageLayoutPage;

/**
 * Class CoreStatisticsPageTest
 *
 * @package Kanooh\Paddle\App\CoreStatisticsPage
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class CoreStatisticsPageTest extends WebDriverTestCase
{

    /**
     * @var AdministrativeNodeViewPage
     */
    protected $administrativeNodeViewPage;

    /**
     * @var AlphanumericTestDataProvider
     */
    protected $alphanumericTestDataProvider;

    /**
     * @var AppService
     */
    protected $appService;

    /**
     * @var AssetCreationService
     */
    protected $assetCreationService;

    /**
     * @var ContentCreationService
     */
    protected $contentCreationService;

    /**
     * @var CoreStatisticsPageEditPage
     */
    protected $editPage;

    /**
     * @var CoreStatisticsPageViewPage
     */
    protected $frontendPage;

    /**
     * @var LayoutPage
     */
    protected $layoutPage;

    /**
     * @var ScaldService
     */
    protected $scaldService;

    /**
     * @var UserSessionService
     */
    protected $userSessionService;

    /**
     * {@inheritdoc}
     */
    public function setUpPage()
    {
        parent::setUpPage();

        // Create some instances to use later on.
        $this->administrativeNodeViewPage = new AdministrativeNodeViewPage($this);
        $this->alphanumericTestDataProvider = new AlphanumericTestDataProvider();
        $this->assetCreationService = new AssetCreationService($this);
        $this->editPage = new CoreStatisticsPageEditPage($this);
        $this->layoutPage = new CoreStatisticsPageLayoutPage($this);
        $this->scaldService = new ScaldService($this);
        $this->frontendPage = new CoreStatisticsPageViewPage($this);

        // Go to the login page and log in as Chief Editor.
        $this->userSessionService = new UserSessionService($this);
        $this->contentCreationService = new ContentCreationService($this, $this->userSessionService);
        $this->userSessionService->login('ChiefEditor');

        // Enable the app if it is not yet enabled.
        $this->appService = new AppService($this, $this->userSessionService);
        $this->appService->enableApp(new CoreStatisticsPage);
    }

    /**
     * Tests the node edit of the core statistics page.
     */
    public function testNodeEdit()
    {
        // Create a core statistics page and fill all the fields.
        $nid = $this->contentCreationService->createCoreStatisticsPage();
        $title_1 = $this->alphanumericTestDataProvider->getValidValue();
        $body_1 = $this->alphanumericTestDataProvider->getValidValue();
        $url_1 = 'http://' . $this->alphanumericTestDataProvider->getValidValue() . '.com';

        $title_2 = $this->alphanumericTestDataProvider->getValidValue();
        $body_2 = $this->alphanumericTestDataProvider->getValidValue();
        $url_2 = 'http://' . $this->alphanumericTestDataProvider->getValidValue() . '.com';

        $sources = $this->alphanumericTestDataProvider->getValidValue();
        $relevant_links = $this->alphanumericTestDataProvider->getValidValue();
        $definitions = $this->alphanumericTestDataProvider->getValidValue();
        $metadata = 'http://' . $this->alphanumericTestDataProvider->getValidValue() . '.com';

        $publication_date = date('d/m/Y');
        $next_update = date('d/m/Y', time() + 86400);

        $this->editPage->go($nid);

        $row = $this->editPage->coreStatisticsPageEditForm->paragraphs->rows[0];
        //$row = reset($rows);
        $row->title->fill($title_1);
        $row->body->setBodyText($body_1);
        $row->dataWrapper->fill($url_1);

        $count_rows = $this->editPage->coreStatisticsPageEditForm->paragraphs->getNumberOfRows();
        // Move to the element above to prevent Selenium from trying to click on the contextual menu.
        //$this->moveto($this->editPage->creationDate);
        $this->editPage->coreStatisticsPageEditForm->paragraphs->addAnotherItem->click();

        // We need to wait until the new row with the fields appears.
        $table = $this->editPage->coreStatisticsPageEditForm->paragraphs;
        $callable = new SerializableClosure(
            function () use ($count_rows, $table) {
                if ($table->getNumberOfRows() == ($count_rows + 1)) {
                    return true;
                }
            }
        );
        $this->waitUntil($callable, $this->getTimeout());

        $row = $this->editPage->coreStatisticsPageEditForm->paragraphs->rows[1];
        //$row = reset($rows);
        $row->title->fill($title_2);
        $row->body->setBodyText($body_2);
        $row->dataWrapper->fill($url_2);

        $this->editPage->coreStatisticsPageEditForm->sources->setBodyText($sources);
        $this->editPage->coreStatisticsPageEditForm->relevantLinks->setBodyText($relevant_links);
        $this->editPage->coreStatisticsPageEditForm->definitions->setBodyText($definitions);

        $this->editPage->coreStatisticsPageEditForm->publicationDate->fill($publication_date);
        $this->editPage->coreStatisticsPageEditForm->nextUpdate->fill($next_update);
        $this->editPage->coreStatisticsPageEditForm->metaData->fill($metadata);

        $this->editPage->contextualToolbar->buttonSave->click();
        $this->administrativeNodeViewPage->checkArrival();

        $this->editPage->go($nid);

        $rows = $this->editPage->coreStatisticsPageEditForm->paragraphs->rows;
        $this->assertEquals($title_1, $rows[0]->title->getContent());
        $this->assertContains($body_1, $rows[0]->body->getBodyText());
        $this->assertEquals($url_1, $rows[0]->dataWrapper->getContent());

        $this->assertEquals($title_2, $rows[1]->title->getContent());
        $this->assertContains($body_2, $rows[1]->body->getBodyText());
        $this->assertEquals($url_2, $rows[1]->dataWrapper->getContent());

        $this->assertContains($sources, $this->editPage->coreStatisticsPageEditForm->sources->getBodyText());
        $this->assertContains($relevant_links, $this->editPage->coreStatisticsPageEditForm->relevantLinks->getBodyText());
        $this->assertContains($definitions, $this->editPage->coreStatisticsPageEditForm->definitions->getBodyText());

        $this->assertEquals($publication_date, $this->editPage->coreStatisticsPageEditForm->publicationDate->getContent());
        $this->assertEquals($next_update, $this->editPage->coreStatisticsPageEditForm->nextUpdate->getContent());
        $this->assertEquals($metadata, $this->editPage->coreStatisticsPageEditForm->metaData->getContent());
    }

    /**
     * Tests the node page layout of the core statistics page.
     */
    public function testPageLayout()
    {
        // Create a core statistics page and fill all the fields.
        $nid = $this->contentCreationService->createCoreStatisticsPage();
        $title_1 = $this->alphanumericTestDataProvider->getValidValue();
        $body_1 = $this->alphanumericTestDataProvider->getValidValue();
        $url_1 = 'http://' . $this->alphanumericTestDataProvider->getValidValue() . '.com';

        $title_2 = $this->alphanumericTestDataProvider->getValidValue();
        $body_2 = $this->alphanumericTestDataProvider->getValidValue();
        $url_2 = 'http://' . $this->alphanumericTestDataProvider->getValidValue() . '.com';

        $sources = $this->alphanumericTestDataProvider->getValidValue();
        $relevant_links = $this->alphanumericTestDataProvider->getValidValue();
        $definitions = $this->alphanumericTestDataProvider->getValidValue();
        $metadata = 'http://' . $this->alphanumericTestDataProvider->getValidValue() . '.com';

        $publication_date = date('d/m/Y');
        $next_update = date('d/m/Y', time() + 86400);

        $this->editPage->go($nid);

        $row = $this->editPage->coreStatisticsPageEditForm->paragraphs->rows[0];
        //$row = reset($rows);
        $row->title->fill($title_1);
        $row->body->setBodyText($body_1);
        $row->dataWrapper->fill($url_1);

        $count_rows = $this->editPage->coreStatisticsPageEditForm->paragraphs->getNumberOfRows();
        // Move to the element above to prevent Selenium from trying to click on the contextual menu.
        //$this->moveto($this->editPage->creationDate);
        $this->editPage->coreStatisticsPageEditForm->paragraphs->addAnotherItem->click();

        // We need to wait until the new row with the fields appears.
        $table = $this->editPage->coreStatisticsPageEditForm->paragraphs;
        $callable = new SerializableClosure(
            function () use ($count_rows, $table) {
                if ($table->getNumberOfRows() == ($count_rows + 1)) {
                    return true;
                }
            }
        );
        $this->waitUntil($callable, $this->getTimeout());

        $row = $this->editPage->coreStatisticsPageEditForm->paragraphs->rows[1];
        //$row = reset($rows);
        $row->title->fill($title_2);
        $row->body->setBodyText($body_2);
        $row->dataWrapper->fill($url_2);

        $this->editPage->coreStatisticsPageEditForm->sources->setBodyText($sources);
        $this->editPage->coreStatisticsPageEditForm->relevantLinks->setBodyText($relevant_links);
        $this->editPage->coreStatisticsPageEditForm->definitions->setBodyText($definitions);

        $this->editPage->coreStatisticsPageEditForm->publicationDate->fill($publication_date);
        $this->editPage->coreStatisticsPageEditForm->nextUpdate->fill($next_update);
        $this->editPage->coreStatisticsPageEditForm->displayJustMonthNextUpdate->check();
        $this->editPage->coreStatisticsPageEditForm->metaData->fill($metadata);

        $this->editPage->contextualToolbar->buttonSave->click();
        $this->administrativeNodeViewPage->checkArrival();

        // Go to the layout page and verify that every thing is in its correct region
        $this->layoutPage->go($nid);

        // Verify that the education info exists in region B
        $region = $this->layoutPage->display->region('left');
        $panes = $region->getPanes();
        $i = 1;
        foreach ($panes as $pane) {
            $element = $pane->getWebdriverElement();

            if ($i == 1) {
                $this->assertContains($title_1, $element->text());
                $this->assertContains($title_2, $element->text());
            }

            if ($i == 2) {
                $iframe_src_1 = $element->byXPath('//iframe')->attribute('src');
                $this->assertContains($title_1, $element->text());
                $this->assertContains($title_2, $element->text());
                $this->assertContains($body_1, $element->text());
                $this->assertContains($body_2, $element->text());
                $this->assertEquals(strtolower($url_1), rtrim($iframe_src_1, '/'));
                $iframe_src_2 = $element->byXPath('//div[@class="pane-core-statistics-paragraphs"]//li[@class="last"]//iframe')->attribute('src');
                $this->assertEquals(strtolower($url_2), rtrim($iframe_src_2, '/'));
            }

            if ($i == 3) {
                $this->assertContains($sources, $element->text());
            }

            if ($i == 4) {
                $this->assertContains($relevant_links, $element->text());
            }

            if ($i == 5) {
                $this->assertContains($definitions, $element->text());
            }

            $i++;
        }

        // Verify that the education info exists in region B
        $region = $this->layoutPage->display->region('right');
        $panes = $region->getPanes();
        $i = 1;
        foreach ($panes as $pane) {
            $element = $pane->getWebdriverElement();
            if ($i == 1) {
                $this->assertContains(date_format(date_create_from_format('d/m/Y', $publication_date), 'j F Y'), $element->text());
            }

            if ($i == 2) {
                $this->assertContains(date_format(date_create_from_format('d/m/Y', $next_update), 'F Y'), $element->text());
            }

            if ($i == 3) {
                $metadata = $element->byXPath('//div[@class="pane-core-statistics-metadata"]//a')->attribute('href');
                $this->assertEquals($metadata, $metadata);
            }

            $i++;
        }
    }

    /**
     * Tests the feature image pane to be shown
     *
     * @group Product
     */
    public function testNodeViewFeatureImagePane()
    {
        // Create a product and fill out the featured image field.
        $atom_1 = $this->assetCreationService->createImage();
        $atom_2 = $this->assetCreationService->createImage();
        $nid = $this->contentCreationService->createCoreStatisticsPageViaUI();
        $this->editPage->go($nid);
        $this->editPage->coreStatisticsPageEditForm->featureImage->selectAtom($atom_1['id']);
        $this->editPage->contextualToolbar->buttonSave->click();
        $this->administrativeNodeViewPage->checkArrival();

        // Verify that the banner image pane is shown in the front end.
        $this->frontendPage->go($nid);
        $this->assertEquals('atom-id-' . $atom_1['id'], $this->frontendPage->featureImagePane->image->attribute('class'));

        // Edit the page and replace the featured image.
        $this->editPage->go($nid);
        $this->editPage->coreStatisticsPageEditForm->featureImage->clear();
        $this->editPage->coreStatisticsPageEditForm->featureImage->selectAtom($atom_2['id']);
        $this->editPage->contextualToolbar->buttonSave->click();
        $this->administrativeNodeViewPage->checkArrival();

        // Verify that the updated feature image pane is shown in the front end.
        $this->frontendPage->go($nid);
        $this->assertEquals('atom-id-' . $atom_2['id'], $this->frontendPage->featureImagePane->image->attribute('class'));

        // Remove the featured image and verify that the pane has been removed.
        $this->editPage->go($nid);
        $this->editPage->coreStatisticsPageEditForm->featureImage->clear();
        $this->editPage->contextualToolbar->buttonSave->click();
        $this->administrativeNodeViewPage->checkArrival();

        $this->frontendPage->go($nid);
        try {
            $this->frontendPage->featureImagePane;
            $this->fail('there should be no image pane shown.');
        } catch (\PHPUnit_Extensions_Selenium2TestCase_WebDriverException $e) {
            // Everything is fine.
        }
    }
}
